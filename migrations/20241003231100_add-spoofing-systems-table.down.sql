
DROP TRIGGER IF EXISTS spoof_systems_prune_trigger ON spoof_systems;
DROP FUNCTION IF EXISTS  spoof_systems_prune;
DROP TABLE IF EXISTS spoof_systems;