-- Step 1: Remove the victim_ship column from elite_kills_overlay
ALTER TABLE elite_kills_overlay
DROP COLUMN victim_ship;

CREATE OR REPLACE VIEW elite_kills_patched AS
SELECT 
    ek.kill_id,
    ek.interaction_index,
    ek.killer,
    ek.killer_squadron,
    ek.killer_rank,
    ek.killer_ship,
    ek.victim,
    ek.victim_squadron,
    ek.victim_rank,
    ek.victim_ship,
    ek.kill_timestamp,
    ek.hashed_token_creator,
    ek.hashed_token_updater,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.system_name 
        ELSE ek.system_name 
    END AS system_name
FROM 
    elite_kills ek
LEFT JOIN 
    elite_kills_overlay eco ON ek.kill_id = eco.kill_id;