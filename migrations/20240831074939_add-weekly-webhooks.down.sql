ALTER TABLE webhooks
DROP COLUMN IF EXISTS push_elite_weekly,
DROP COLUMN IF EXISTS push_elite_weekly_deep_dive,
DROP CONSTRAINT IF EXISTS webhooks_pkey;
