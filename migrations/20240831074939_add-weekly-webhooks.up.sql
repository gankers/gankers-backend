ALTER TABLE webhooks
ADD COLUMN push_elite_weekly BOOLEAN DEFAULT FALSE NOT NULL,
ADD COLUMN push_elite_weekly_deep_dive BOOLEAN DEFAULT FALSE NOT NULL,
ADD CONSTRAINT webhooks_pkey PRIMARY KEY (webhook_url);
