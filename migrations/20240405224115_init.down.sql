DROP TRIGGER IF EXISTS trigger_revoke_all_tokens_on_ban ON accounts;
DROP TRIGGER IF EXISTS trigger_revoke_all_admin_perms_on_demote ON accounts;

DROP TABLE IF EXISTS tokens;
DROP TABLE IF EXISTS accounts;
DROP TABLE IF EXISTS elite_kills;
DROP TABLE IF EXISTS elite_commanders;
DROP TABLE IF EXISTS webhooks;

DROP TABLE IF EXISTS elite_kills_replay;

DROP FUNCTION IF EXISTS trigger_fn_revoke_all_admin_perms_on_demote();
DROP FUNCTION IF EXISTS trigger_fn_revoke_all_tokens_on_ban();


DROP INDEX IF EXISTS idx_elite_commanders_search;
