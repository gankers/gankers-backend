-- Remove powerplay information for elite kills
ALTER TABLE elite_kills
DROP COLUMN killer_power,
DROP COLUMN victim_power;

-- Remove the last_power column from elite_commanders
ALTER TABLE elite_commanders
DROP COLUMN last_power;

CREATE OR REPLACE VIEW elite_kills_patched AS
SELECT 
    ek.kill_id,
    ek.interaction_index,
    ek.killer,
    ek.killer_squadron,
    ek.killer_rank,
    ek.killer_ship,
    ek.victim,
    ek.victim_squadron,
    ek.victim_rank,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.victim_ship
        ELSE ek.victim_ship
    END AS victim_ship,
    ek.kill_timestamp,
    ek.hashed_token_creator,
    ek.hashed_token_updater,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.system_name 
        ELSE ek.system_name 
    END AS system_name
FROM 
    elite_kills ek
LEFT JOIN 
    elite_kills_overlay eco ON ek.kill_id = eco.kill_id;