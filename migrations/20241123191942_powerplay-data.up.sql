-- Add powerplay information for elite kills
ALTER TABLE elite_kills
ADD COLUMN killer_power VARCHAR,
ADD COLUMN victim_power VARCHAR;

ALTER TABLE elite_commanders
ADD COLUMN last_power VARCHAR;

CREATE OR REPLACE VIEW elite_kills_patched AS
SELECT 
    ek.kill_id,
    ek.interaction_index,
    ek.killer,
    ek.killer_squadron,
    ek.killer_rank,
    ek.killer_ship,
    ek.victim,
    ek.victim_squadron,
    ek.victim_rank,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.victim_ship
        ELSE ek.victim_ship
    END AS victim_ship,
    ek.kill_timestamp,
    ek.hashed_token_creator,
    ek.hashed_token_updater,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.system_name 
        ELSE ek.system_name 
    END AS system_name,
    ek.killer_power,
    ek.victim_power
FROM 
    elite_kills ek
LEFT JOIN 
    elite_kills_overlay eco ON ek.kill_id = eco.kill_id;