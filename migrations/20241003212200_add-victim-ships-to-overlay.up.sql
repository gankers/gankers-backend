-- Step 1: Alter the table to add victim_ship column
ALTER TABLE elite_kills_overlay
ADD COLUMN victim_ship VARCHAR(64);

-- Step 2: Copy over the victim_ship data from elite_kills
UPDATE elite_kills_overlay eko
SET victim_ship = ek.victim_ship
FROM elite_kills ek
WHERE eko.kill_id = ek.kill_id;

CREATE OR REPLACE VIEW elite_kills_patched AS
SELECT 
    ek.kill_id,
    ek.interaction_index,
    ek.killer,
    ek.killer_squadron,
    ek.killer_rank,
    ek.killer_ship,
    ek.victim,
    ek.victim_squadron,
    ek.victim_rank,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.victim_ship
        ELSE ek.victim_ship
    END AS victim_ship,
    ek.kill_timestamp,
    ek.hashed_token_creator,
    ek.hashed_token_updater,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.system_name 
        ELSE ek.system_name 
    END AS system_name
FROM 
    elite_kills ek
LEFT JOIN 
    elite_kills_overlay eco ON ek.kill_id = eco.kill_id;