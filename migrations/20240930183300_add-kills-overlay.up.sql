-- :)
CREATE TABLE IF NOT EXISTS elite_kills_overlay (
    kill_id INT NOT NULL PRIMARY KEY,
    system_name VARCHAR(64),
    valid_until TIMESTAMPTZ NOT NULL
);

-- This function is invoked whenever elite_kills_overlay is inserted into.
-- It will remove all entries that are no longer relevant because NOW() is > valid_until
CREATE OR REPLACE FUNCTION trigger_fn_remove_stale_entries()
RETURNS TRIGGER AS $$
BEGIN
    -- Delete rows where valid_until is older than the current time
    DELETE FROM elite_kills_overlay 
    WHERE valid_until < NOW();
    
    -- Return the NEW record to allow the INSERT operation to complete
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_delete_old_entries
BEFORE INSERT ON elite_kills_overlay
FOR EACH STATEMENT
EXECUTE FUNCTION trigger_fn_remove_stale_entries();


ALTER TABLE accounts
ADD COLUMN spoof_location_duration_minutes SMALLINT NOT NULL DEFAULT 0;


CREATE TABLE IF NOT EXISTS webhook_patches_queue (
    id INT GENERATED ALWAYS AS IDENTITY,
    patch_webhook_url VARCHAR(256) NOT NULL,
    patch_body TEXT NOT NULL,
    not_before TIMESTAMPTZ NOT NULL
);


CREATE OR REPLACE VIEW elite_kills_patched AS
SELECT 
    ek.kill_id,
    ek.interaction_index,
    ek.killer,
    ek.killer_squadron,
    ek.killer_rank,
    ek.killer_ship,
    ek.victim,
    ek.victim_squadron,
    ek.victim_rank,
    ek.victim_ship,
    ek.kill_timestamp,
    ek.hashed_token_creator,
    ek.hashed_token_updater,
    CASE 
        WHEN eco.valid_until > NOW() THEN eco.system_name 
        ELSE ek.system_name 
    END AS system_name
FROM 
    elite_kills ek
LEFT JOIN 
    elite_kills_overlay eco ON ek.kill_id = eco.kill_id;

CREATE INDEX idx_elite_kills_kill_id ON elite_kills(kill_id);
CREATE INDEX idx_elite_kills_overlay_kill_id ON elite_kills_overlay(kill_id);
CREATE INDEX idx_elite_kills_overlay_valid_until ON elite_kills_overlay(valid_until);
