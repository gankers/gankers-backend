DROP INDEX IF EXISTS idx_elite_kills_kill_id;
DROP INDEX IF EXISTS idx_elite_kills_overlay_kill_id;
DROP INDEX IF EXISTS idx_elite_kills_overlay_valid_until;

DROP TRIGGER IF EXISTS trigger_delete_old_entries ON elite_kills_overlay;
DROP FUNCTION IF EXISTS trigger_fn_remove_stale_entries;

ALTER TABLE accounts
DROP COLUMN spoof_location_duration_minutes;

DROP TABLE IF EXISTS webhook_patches_queue;

DROP VIEW IF EXISTS elite_kills_patched;
DROP TABLE IF EXISTS elite_kills_overlay;

