CREATE TABLE IF NOT EXISTS spoof_systems (
    cmdr_name VARCHAR(64) NOT NULL PRIMARY KEY,
    system_name VARCHAR(64) NOT NULL,
    valid_until TIMESTAMPTZ NOT NULL
);

CREATE OR REPLACE FUNCTION spoof_systems_prune()
RETURNS TRIGGER AS $$
BEGIN
    -- Delete entries where valid_until is in the past
    DELETE FROM spoof_systems
    WHERE valid_until < NOW();
    
    -- Allow the insert to proceed
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER spoof_systems_prune_trigger
BEFORE INSERT ON spoof_systems
FOR EACH ROW
EXECUTE FUNCTION spoof_systems_prune();