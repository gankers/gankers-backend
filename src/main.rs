mod auth;
mod config;
mod db;
mod services;
mod util;

use std::{
    collections::VecDeque,
    sync::{Arc, Mutex, RwLock},
};

use chrono::{DateTime, Utc};
use db::elite_historic_kills::update_data_for_commanders;
use services::{
    get_router,
    types::{HistoricQueueTask, SseBroadcaster},
};
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};
use tokio::net::TcpListener;
use tracing::{debug, error, info};
use util::weekly_summary::get_next_weekly_emission_time;

use crate::{config::CONFIG, services::historic::init_historic_handler};

#[derive(Clone)]
pub(crate) struct AppState {
    db: Pool<Postgres>,
    historic_queue: Arc<Mutex<VecDeque<HistoricQueueTask>>>,
    /// (public) Emits to all listeners if a Live Kill is logged.
    live_kills_channel: Arc<SseBroadcaster>,
    /// (admin only) Emits to all listeners if a Background Job has finished.
    /// To be used by the Bot to send a DM with statistics
    historic_updates_channel: Arc<SseBroadcaster>,
    /// Time when the weekly is next emitted. On startup this is set to the next Thursday 8 AM UTC
    emit_weekly_summary_not_before: Arc<RwLock<DateTime<Utc>>>,
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .init();

    let db = PgPoolOptions::new()
        .max_connections(50)
        .connect(&CONFIG.db_connect_string)
        .await
        .expect("could not connect to db connect string");
    sqlx::migrate!()
        .run(&db)
        .await
        .expect("Failed to get Postgres DB in Sync via Migrations");
    let queue_mutex: Mutex<VecDeque<HistoricQueueTask>> = Mutex::new(VecDeque::with_capacity(64));
    let queue_arc = Arc::new(queue_mutex);

    let app_state = AppState {
        db,
        historic_queue: queue_arc,
        live_kills_channel: SseBroadcaster::new(),
        historic_updates_channel: SseBroadcaster::new(),
        emit_weekly_summary_not_before: Arc::new(RwLock::new(get_next_weekly_emission_time(
            Utc::now(),
        ))),
    };
    let router = get_router(app_state.clone());
    // On Startup, reconcile cmdrs table
    match app_state.db.begin().await {
        Ok(mut tx) => {
            if let Err(err) = update_data_for_commanders(None, &mut tx).await {
                error!("Failed to run full reconcile: {err}")
            } else if let Err(err) = tx.commit().await {
                error!("Failed to run commit transaction for reconcile: {err}")
            } else {
                info!("Reconcile Complete without errors")
            }
        }
        Err(err) => error!("Failed to acquire TX to run CMDR Table reconcile. Db Error: {err}"),
    };

    // Spawn a background task which is used for handling historic events
    let background_task_state = app_state.clone();
    tokio::spawn(async move {
        debug!("Started up Historic Background Handler");
        init_historic_handler(background_task_state).await
    });

    let listener = TcpListener::bind(format!("0.0.0.0:{}", CONFIG.port))
        .await
        .unwrap();
    info!("Listening on :{}", CONFIG.port);
    axum::serve(listener, router).await.unwrap();
}
