use axum::Router;

use crate::AppState;

use self::route_self::get_nested_router_users_self;

mod route_self;

pub(super) fn create_users_router(state: &AppState) -> Router<AppState> {
    Router::new().nest("/self", get_nested_router_users_self(state.clone()))
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> Vec<(String, Vec<schemars::schema::RootSchema>)> {
    vec![route_self::get_schemas()]
}
