use crate::db::elite_cmdrs::{get_all_cmdrs_associated_with_token, DbCmdrAssociation};
use crate::db::tokens::get_all_tokens_for_user;
use crate::util::generic_error::GenericMessage;
use crate::AppState;
use crate::{auth::claim::AuthError, db::tokens::TokenData};
use axum::extract::Request;
use axum::extract::State;
use axum::response::{IntoResponse, Json, Response};
use axum::routing::{delete, get};
use axum::{body::Body, middleware::Next, Router};
use axum::{http::StatusCode, middleware, routing::post};
use schemars::{schema_for, JsonSchema};
use serde::{Deserialize, Serialize};
use tracing::error;

///
/// This Router contains all the routes that user themself can call for all things tokens.
/// Basically getting own account, basically doing CRUD on Tokens for own account
///
pub(super) fn get_nested_router_users_self(state: AppState) -> Router<AppState> {
    Router::new()
        .route("/", get(get_self))
        .route("/tokens/active", get(get_active_token))
        .route("/tokens", get(get_self_tokens))
        .route("/tokens", post(post_new_self_token))
        .route("/tokens/:token_name", delete(delete_self_token_by_name))
        .layer(middleware::from_fn_with_state(
            state.clone(),
            auth_validator,
        ))
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct AccountData {
    discord_id: i64,
    is_banned: bool,
    associated_cmdrs: Vec<DbCmdrAssociation>,
    active_tokens: Vec<TokenData>,
    disabled_tokens: Vec<TokenData>,
}

async fn get_active_token(token_data_opt: Option<TokenData>) -> Response {
    match token_data_opt {
        Some(data) => (StatusCode::OK, Json::from(data)).into_response(),
        None => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json::from(GenericMessage::new(
                "Failed to extract token data. Should never happen.".to_string(),
            )),
        )
            .into_response(),
    }
}

async fn get_self(token_data_opt: Option<TokenData>, State(state): State<AppState>) -> Response {
    let data = match token_data_opt {
        Some(data) => data,
        None => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to extract token data. Should never happen.".to_string(),
                )),
            )
                .into_response()
        }
    };

    let all_tokens = match get_all_tokens_for_user(data.owner_discord_id, &state.db).await {
        Ok(data) => data,
        Err(err) => {
            error!(
                "Failed to get Token Data for given User because of DB Error: {}",
                err
            );
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to get tokens for User".to_string(),
                )),
            )
                .into_response();
        }
    };

    let (active_tokens, disabled_tokens): (Vec<_>, Vec<_>) =
        all_tokens.into_iter().partition(|x| !x.revoked);

    let associated_cmdrs =
        match get_all_cmdrs_associated_with_token(&state.db, data.owner_discord_id).await {
            Ok(data) => data,
            Err(err) => {
                error!(
                    "Failed to get CMDR Data for given User because of DB Error: {}",
                    err
                );
                return (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Json::from(GenericMessage::new(
                        "Failed to get CMDRs for User".to_string(),
                    )),
                )
                    .into_response();
            }
        };

    (
        StatusCode::OK,
        Json(AccountData {
            discord_id: data.owner_discord_id,
            is_banned: false, // TODO!
            associated_cmdrs,
            active_tokens,
            disabled_tokens,
        }),
    )
        .into_response()
}

async fn get_self_tokens() {}

async fn post_new_self_token() {}

async fn delete_self_token_by_name() {}

async fn auth_validator(
    token_data_opt: Option<TokenData>,
    request: Request<Body>,
    next: Next,
) -> Response<Body> {
    if token_data_opt.is_none() {
        return AuthError::MissingCredentials.into_response();
    }
    let token_data = token_data_opt.unwrap();

    if token_data.revoked {
        return AuthError::RevokedToken.into_response();
    }

    if !token_data.permissions.self_api {
        return AuthError::Forbidden.into_response();
    }

    next.run(request).await
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (String::from("users_self"), vec![schema_for!(AccountData)])
}
