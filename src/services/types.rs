use std::sync::Arc;

use tokio::sync::broadcast;
use tracing::warn;

use super::elite::KillEvent;

#[derive(Debug, Clone)]
pub(crate) struct HistoricQueueTask {
    /// Just a random string to tell us which
    pub(crate) identifier: String,
    pub(crate) task: HistoricQueueTaskData,
}

#[derive(Debug, Clone)]
pub(crate) enum HistoricQueueTaskData {
    EliteHistoricQueueTask {
        token_submitter: String,
        payload: Vec<KillEvent>,
    },
    // Add Star Citizen here (if that ever has something like historic uploads, lol)
}

pub(crate) struct SseBroadcaster {
    fanout: broadcast::Sender<String>,
}

impl SseBroadcaster {
    pub fn new() -> Arc<Self> {
        let (tx, _) = broadcast::channel(16);
        Arc::new(SseBroadcaster { fanout: tx })
    }

    pub fn add_client(&self) -> broadcast::Receiver<String> {
        self.fanout.subscribe()
    }

    pub fn broadcast(&self, event: &str) {
        if let Err(err) = self.fanout.send(event.to_string()) {
            warn!("Err trying to broadcast: {}", err)
        }
    }
}
