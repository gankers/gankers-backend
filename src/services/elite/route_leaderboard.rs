use axum::{
    extract::{Query, State},
    http::{HeaderValue, StatusCode},
    response::{
        sse::{Event, KeepAlive},
        IntoResponse, Response, Sse,
    },
    routing::get,
    Json, Router,
};
use chrono::Utc;
use futures::Stream;
use schemars::{schema_for, JsonSchema};
use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use tracing::error;

use crate::{
    db::elite_leaderboard::{
        get_leaderboard_entries, get_leaderboard_entries_in_range, get_recent_killboard_entries,
        get_total_kill_event_count, KillboardEntry, LeaderboardEntry, WeeklyLeaderboardEntry,
    },
    util::{
        generic_error::GenericMessage, weekly_summary::get_current_weekly_period_from_timestamp,
    },
    AppState,
};

pub(super) fn get_leaderboard_router() -> Router<AppState> {
    Router::<AppState>::new()
        .route("/", get(get_leaderboard))
        .route("/weekly", get(get_weekly_leaderboard))
        .route("/recent", get(get_recent_kills))
        .route("/live-sse", get(leaderboard_sse_handler))
        .route("/kill-count", get(get_kill_count))
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
struct LeaderboardResult {
    page: i64,
    result: Vec<LeaderboardEntry>,
}

#[derive(Deserialize, JsonSchema)]
struct Pagination {
    page: usize,
}

#[derive(Deserialize, JsonSchema)]
pub(crate) struct SizeablePagination {
    pub(crate) page: usize,
    pub(crate) size: usize,
}

async fn get_recent_kills(
    pagination: Query<SizeablePagination>,
    State(state): State<AppState>,
) -> Response {
    let page = pagination.0.page as i64;

    if page < 1 {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new("Page cannot be < 1".to_string())),
        )
            .into_response();
    }

    match get_recent_killboard_entries(pagination.size as i64, page, &state.db).await {
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
        Err(err) => {
            error!("DB error while fetching recent kills: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching recent kill".to_string(),
                )),
            )
                .into_response()
        }
    }
}

async fn get_weekly_leaderboard(
    pagination: Query<SizeablePagination>,
    State(state): State<AppState>,
) -> Response {
    let time_range = get_current_weekly_period_from_timestamp(Utc::now());

    match get_leaderboard_entries_in_range(
        pagination.page as i64,
        pagination.size as i64,
        &state.db,
        time_range,
    )
    .await
    {
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
        Err(data) => {
            error!("DB error while fetching weekly result: {}", data);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "DB error while fetching weekly result".to_string(),
                )),
            )
                .into_response()
        }
    }
}

async fn get_leaderboard(State(state): State<AppState>, pagination: Query<Pagination>) -> Response {
    let page = pagination.0.page as i64;

    if page < 1 {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new("Page cannot be < 1".to_string())),
        )
            .into_response();
    }

    let result = match get_leaderboard_entries(page, &state.db).await {
        Ok(data) => data,
        Err(err) => {
            error!("Failed to comm with the Database: {}", err);
            let message = "Interaction with the Database failed. See server logs for more info. If you are a user, send a message to one of the Admins in GGI.".to_string();
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(GenericMessage::new(message.to_string())),
            )
                .into_response();
        }
    };

    (
        StatusCode::OK,
        Json::from(LeaderboardResult { page, result }),
    )
        .into_response()
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("elite_leaderboard"),
        vec![
            schema_for!(LeaderboardResult),
            schema_for!(Pagination),
            schema_for!(SizeablePagination),
            schema_for!(KillboardEntry),
            schema_for!(WeeklyLeaderboardEntry),
            schema_for!(KillCountResponse),
        ],
    )
}

fn create_leaderboard_sse_handler(
    mut rx: tokio::sync::broadcast::Receiver<String>,
) -> Sse<impl Stream<Item = Result<Event, Infallible>>> {
    let stream = async_stream::stream! {
        yield Ok(Event::default().comment("connected"));
        loop {
            let msg = rx.recv().await.unwrap();
            yield Ok(Event::default().data(&msg));
        }
    };

    Sse::new(stream).keep_alive(KeepAlive::default())
}

async fn leaderboard_sse_handler(State(app_state): State<AppState>) -> impl IntoResponse {
    let rx: tokio::sync::broadcast::Receiver<String> = app_state.live_kills_channel.add_client();
    let mut sse = create_leaderboard_sse_handler(rx).into_response();
    sse.headers_mut()
        .append("X-Accel-Buffering", HeaderValue::from_static("no"));
    sse.headers_mut()
        .append("Access-Control-Allow-Origin", HeaderValue::from_static("*"));
    sse
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct KillCountResponse {
    total_count: usize,
}

/// Gets the current amount of kill/death events in the database
async fn get_kill_count(State(app_state): State<AppState>) -> Response {
    match get_total_kill_event_count(&app_state.db).await {
        Ok(count) => (
            StatusCode::OK,
            Json(KillCountResponse { total_count: count }),
        )
            .into_response(),
        Err(err) => {
            error!("Failed to get total count of kill events: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(GenericMessage::new(
                    "An Error occurred when trying to get the total count.".to_string(),
                )),
            )
                .into_response()
        }
    }
}
