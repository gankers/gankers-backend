use crate::auth::claim::RawToken;
use crate::db::accounts::get_spoof_duration_from_token_minutes;
use crate::db::elite_historic_kills::update_data_for_commanders;
use crate::db::elite_kills::{
    determine_poison_ship_name, determine_poison_system, insert_new_live_event,
    insert_spoof_overlay, InsertionResult,
};
use crate::db::webhooks::get_all_elite_kill_webhooks;
use crate::services::historic::historic_elite::append_kill_events_to_job_queue;
use crate::util::elite_kill_webhook::send_killboard_webhook;
use crate::util::generic_error::GenericMessage;
use crate::AppState;
use crate::{auth::claim::AuthError, db::tokens::TokenData};
use axum::extract::{self, Request};
use axum::extract::{DefaultBodyLimit, State};
use axum::response::{IntoResponse, Json, Response};
use axum::{body::Body, middleware::Next, Router};
use axum::{http::StatusCode, middleware, routing::post};
use chrono::TimeDelta;
use schemars::{schema_for, JsonSchema};
use serde::{Deserialize, Serialize};
use tracing::{debug, error, info};

use super::types::InteractionKey;
use super::KillEvent;

pub(super) fn get_submit_router(state: &AppState) -> Router<AppState> {
    Router::<AppState>::new()
        .route("/", post(submit_kill))
        // /historic is capped at 10MByte, which should be… plenty
        .route(
            "/historic",
            post(submit_historic).layer(DefaultBodyLimit::max(10 * 1024 * 1024)),
        )
        .layer(middleware::from_fn_with_state(
            state.clone(),
            auth_validator,
        ))
}

#[derive(Serialize, Deserialize, Debug, JsonSchema)]
struct SubmitResponse {
    message: String,
    id: i32,
    event: InsertionResult,
}

/// This is used to submit a "live" event
/// This will also emit an event to the webhook
async fn submit_kill(
    token: Option<RawToken>,
    State(state): State<AppState>,
    extract::Json(event): extract::Json<KillEvent>,
) -> Response {
    // Get the interaction key, then query the DB against all occurences with this interaction where the occurence is within a time window above and below
    let interaction = InteractionKey::from(&event);

    if let Err(errs) = event.validate() {
        return (StatusCode::BAD_REQUEST, Json::from(errs)).into_response();
    }

    match token {
        Some(t) => {
            let hashed_token = t.hash();

            match insert_new_live_event(&interaction.0, &event, &state.db, &hashed_token).await {
                Err(err) => {
                    error!("Failed to comm with the Database: {}", err);
                    let message = "Interaction with the Database failed. See server logs for more info. If you are a user, send a message to one of the Admins in GGI.".to_string();
                    (
                        StatusCode::INTERNAL_SERVER_ERROR,
                        Json(GenericMessage::new(message.to_string())),
                    )
                        .into_response()
                }
                Ok(data) => {
                    let json_data = Json(SubmitResponse {
                        message: format!("{} entry with ID {}", data.1.to_string(), data.0),
                        id: data.0,
                        event: data.1,
                    });

                    let cmdrs_in_this_event = vec![event.killer.clone(), event.victim.clone()];

                    if InsertionResult::Created == json_data.event {
                        // Check if we should spoof the Location
                        let spoofing_duration =
                            get_spoof_duration_from_token_minutes(&hashed_token, &state.db)
                                .await
                                .unwrap_or(0);
                        let spoof_event = if spoofing_duration > 0 {
                            // We need to spoof
                            match determine_poison_system(
                                &state.db,
                                event.system.clone(),
                                event.killer.clone(),
                            )
                            .await
                            {
                                Some(system) => {
                                    let spoofed_victim_ship =
                                        determine_poison_ship_name(&state.db, system.clone()).await;
                                    let mut poisoned_event = event.clone();
                                    poisoned_event.system = Some(system);
                                    if let Some(ship) = spoofed_victim_ship {
                                        poisoned_event.victim_ship = Some(ship);
                                    }
                                    Some(poisoned_event)
                                }
                                None => None,
                            }
                        } else {
                            None
                        };

                        if let Some(spoof) = &spoof_event {
                            // we create an "overlay" that will cause fetches to return the spoofed data
                            let spoof_until =
                                spoof.kill_timestamp + TimeDelta::minutes(spoofing_duration as i64);
                            if let Err(err) = insert_spoof_overlay(
                                &state.db,
                                spoof_until,
                                spoof.system.clone(),
                                spoof.victim_ship.clone(),
                                data.0,
                            )
                            .await
                            {
                                error!("Failed to commit spoof into elite_kills_overlay. Reason: {} Continuing...", err)
                            }
                        }

                        let event_to_send_immediate = match &spoof_event {
                            Some(val) => val,
                            None => &event,
                        };

                        let event_to_send_as_correction = spoof_event.as_ref().map(|_| &event);

                        match get_all_elite_kill_webhooks(&state.db).await {
                            Ok(data) => {
                                let len = data.len();
                                send_killboard_webhook(
                                    data,
                                    event_to_send_immediate,
                                    event_to_send_as_correction,
                                    event.kill_timestamp
                                        + TimeDelta::minutes(spoofing_duration as i64),
                                    &state.db,
                                )
                                .await;
                                info!("Finished Kill Emit for {} Webhooks.", len)
                            }
                            Err(err) => {
                                error!("Failed to get all Elite Webhooks: {}", err)
                            }
                        };

                        match serde_json::to_string(&event) {
                            Ok(sse_payload) => {
                                state.live_kills_channel.broadcast(&sse_payload);
                                debug!("Emitted Kill Event to SSE Listener Channel")
                            }
                            Err(err) => {
                                error!("Failed to TX Kill Events to SSE Channel: {}", err)
                            }
                        }
                    }

                    if InsertionResult::Ignored != json_data.event {
                        // If here, the DB State has changed. We need to reconcile the CMDR Table
                        match state.db.begin().await {
                            Ok(mut tx) => {
                                if let Err(err) = update_data_for_commanders(
                                    Some(cmdrs_in_this_event.clone()),
                                    &mut tx,
                                )
                                .await
                                {
                                    error!("Failed to reconcile CMDR Table for {} and {}. Db Error: {}", cmdrs_in_this_event[0], cmdrs_in_this_event[1], err);
                                } else if let Err(err) = tx.commit().await {
                                    error!(
                                        "Failed to Commit Reconcile for CMDRS {} and {}: {}",
                                        &cmdrs_in_this_event[0], &cmdrs_in_this_event[1], err
                                    )
                                }
                            }
                            Err(err) => {
                                error!("Failed to acquire TX to run CMDR Table reconcile. Db Error: {err}");
                            }
                        };
                    }

                    (StatusCode::OK, json_data).into_response()
                }
            }
        }
        None => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(GenericMessage::new(
                "Failed to find token even though token was extracted. This should never happen"
                    .to_string(),
            )),
        )
            .into_response(),
    }
}

/// Use this to submit "old" events. This consumes multiple Kill Events at the same time
/// This will **not** emit an event to webhooks.
async fn submit_historic(
    token: Option<RawToken>,
    State(state): State<AppState>,
    extract::Json(events): extract::Json<Vec<KillEvent>>,
) -> Response {
    if events.is_empty() {
        return (
            StatusCode::OK,
            Json(GenericMessage::new(
                "No data provided. Nothing to do".to_string(),
            )),
        )
            .into_response();
    }

    let token_hash = match token {
        Some(val) => val.hash(),
        None => return (StatusCode::INTERNAL_SERVER_ERROR, Json(GenericMessage::new("This should never happen. Bad configuration. Failed to unpack Token. This should have been caught upstream!".to_string()))).into_response(),
    };
    match append_kill_events_to_job_queue(token_hash, events, state) {
        Ok(value) => value,
        Err(value) => value,
    }
}

async fn auth_validator(
    token_data_opt: Option<TokenData>,
    request: Request<Body>,
    next: Next,
) -> Response<Body> {
    if token_data_opt.is_none() {
        return AuthError::MissingCredentials.into_response();
    }
    let token_data = token_data_opt.unwrap();

    if token_data.revoked {
        return AuthError::RevokedToken.into_response();
    }

    if !token_data.permissions.elite_submit {
        return AuthError::Forbidden.into_response();
    }

    next.run(request).await
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("elite_submit"),
        vec![schema_for!(SubmitResponse)],
    )
}



