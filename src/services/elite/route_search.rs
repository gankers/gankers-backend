use axum::response::{IntoResponse, Json, Response};
use axum::{
    extract::{Query, State},
    http::StatusCode,
    routing::get,
    Router,
};

use schemars::{schema_for, JsonSchema};
use serde::Deserialize;
use tracing::error;

use crate::{db::elite_cmdrs::search_cmdrs_fuzzy, util::generic_error::GenericMessage, AppState};

pub(super) fn get_search_router() -> Router<AppState> {
    Router::<AppState>::new().route("/cmdrs", get(get_search_cmdrs_fuzzy))
}

#[derive(Deserialize, JsonSchema)]
struct SearchQuery {
    q: String,
}

async fn get_search_cmdrs_fuzzy(
    State(state): State<AppState>,
    pagination: Query<SearchQuery>,
) -> Response {
    match search_cmdrs_fuzzy(&pagination.0.q, &state.db).await {
        Err(err) => {
            error!("Got DB Error while Searching for CMDR: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("A DB Error occurred".to_string())),
            )
                .into_response()
        }
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
    }
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (String::from("elite_search"), vec![schema_for!(SearchQuery)])
}
