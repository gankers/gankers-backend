use axum::extract::Path;
use axum::response::{IntoResponse, Json, Response};
use axum::{
    extract::{Query, State},
    http::StatusCode,
    routing::get,
    Router,
};

use schemars::{schema_for, JsonSchema};
use serde::{Deserialize, Serialize};
use tracing::error;

use crate::db::elite_leaderboard::{
    get_recent_killboard_entries_for_system, get_system_metadata, KillboardEntry, SystemMetadata,
};
use crate::util::sized_pagination::SizedPaginationQuery;
use crate::{util::generic_error::GenericMessage, AppState};

pub(super) fn get_systems_router() -> Router<AppState> {
    Router::<AppState>::new().route("/:system_name", get(get_system_data))
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct SystemDataResponse {
    meta: SystemMetadata,
    page: usize,
    page_data: Vec<KillboardEntry>,
    total_pages: usize,
}

async fn get_system_data(
    State(state): State<AppState>,
    Path(system): Path<String>,
    Query(pagination): Query<SizedPaginationQuery>,
) -> Response {
    let page = pagination.page;
    let page_size = pagination.page_size;
    if page < 1 {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new("Page cannot be < 1".to_string())),
        )
            .into_response();
    }
    if !(1..=100).contains(&page_size) {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new(
                "Page Size must be > 0 and <= 100".to_string(),
            )),
        )
            .into_response();
    }
    let system_meta = match get_system_metadata(system.clone(), &state.db).await {
        Ok(e) => match e {
            Some(meta) => meta,
            None => {
                return (
                    StatusCode::NOT_FOUND,
                    Json(GenericMessage::new(
                        "No Kills found for provided system".to_string(),
                    )),
                )
                    .into_response()
            }
        },
        Err(err) => {
            error!(
                "DB Error when trying to get System Metadata {}: {}",
                &system, err
            );
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(GenericMessage::new(
                    "DB error when trying to get System Result".to_string(),
                )),
            )
                .into_response();
        }
    };

    let system_data = match get_recent_killboard_entries_for_system(
        // We use the system name from the metadata as it (should) contain the expected system name with correct casing
        system_meta.system_name.clone(),
        page_size as i64,
        page as i64,
        &state.db,
    )
    .await
    {
        Ok(data) => data,
        Err(err) => {
            error!(
                "DB Error when trying to get System Data {}: {}",
                &system, err
            );
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(GenericMessage::new(
                    "DB error when trying to get System Result".to_string(),
                )),
            )
                .into_response();
        }
    };

    let total_count = system_meta.kill_count;
    let mut total_pages = total_count / pagination.page_size as i64;
    if total_count % pagination.page_size as i64 != 0 {
        total_pages += 1
    }

    (
        StatusCode::OK,
        Json::from(SystemDataResponse {
            meta: system_meta,
            page_data: system_data,
            page: page.into(),
            total_pages: total_pages as usize,
        }),
    )
        .into_response()
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("elite_system"),
        vec![schema_for!(SystemDataResponse)],
    )
}
