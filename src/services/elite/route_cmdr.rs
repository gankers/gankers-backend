use axum::extract::Path;
use axum::response::{IntoResponse, Json, Response};
use axum::{
    extract::{Query, State},
    http::StatusCode,
    routing::get,
    Router,
};

use schemars::schema_for;
use tracing::error;

use crate::db::elite_cmdrs::{
    get_kill_history_for_cmdr, get_kills_and_death_count_for_cmdr, CmdrQueryResult,
};
use crate::db::elite_leaderboard::KillboardEntry;
use crate::util::sized_pagination::SizedPaginationQueryWithKillFilter;
use crate::{util::generic_error::GenericMessage, AppState};

pub(super) fn get_cmdr_router() -> Router<AppState> {
    Router::<AppState>::new().route("/:cmdr_name", get(get_cmdr_data))
}

async fn get_cmdr_data(
    State(state): State<AppState>,
    Path(cmdr_name): Path<String>,
    Query(pagination): Query<SizedPaginationQueryWithKillFilter>,
) -> Response {
    let page = pagination.page;

    if page < 1 {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new("Page cannot be < 1".to_string())),
        )
            .into_response();
    }

    let cmdr_meta: crate::db::elite_cmdrs::CmdrMetadata =
        match get_kills_and_death_count_for_cmdr(&cmdr_name, &state.db).await {
            Ok(data) => match data {
                Some(val) => val,
                None => {
                    return (
                        StatusCode::NOT_FOUND,
                        Json(GenericMessage::new(format!(
                            "No Kills for a CMDR with the Name {} found.",
                            &cmdr_name
                        ))),
                    )
                        .into_response()
                }
            },
            Err(err) => {
                error!(
                    "DB Error when trying to get KD Result for CMDR {}: {}",
                    &cmdr_name, err
                );
                return (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Json(GenericMessage::new(
                        "DB error when trying to get KD Result".to_string(),
                    )),
                )
                    .into_response();
            }
        };

    let result: Vec<KillboardEntry> = match get_kill_history_for_cmdr(
        &cmdr_name,
        page as usize,
        pagination.page_size as usize,
        pagination.filter,
        &state.db,
    )
    .await
    {
        Ok(data) => match data {
            Some(data) => data,
            None => {
                return (
                    StatusCode::NOT_FOUND,
                    Json(GenericMessage::new(format!(
                        "No Kills for a CMDR with the Name {} found.",
                        &cmdr_name
                    ))),
                )
                    .into_response()
            }
        },
        Err(err) => {
            error!(
                "DB Error when trying to get Kill History Result for CMDR {}: {}",
                &cmdr_name, err
            );
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(GenericMessage::new(
                    "DB error when trying to get Kill History Result".to_string(),
                )),
            )
                .into_response();
        }
    };

    let total_count = cmdr_meta.deaths + cmdr_meta.kills;
    let mut total_pages = total_count / pagination.page_size as i64;
    if total_count % pagination.page_size as i64 != 0 {
        total_pages += 1
    }

    let result = CmdrQueryResult {
        page: page.into(),
        total_pages: total_pages as usize,
        page_data: result,
        kills: cmdr_meta.kills as usize,
        deaths: cmdr_meta.deaths as usize,
        cmdr_name: cmdr_meta.cmdr_name,
        squadron: cmdr_meta.squadron,
        rank: cmdr_meta.rank.map(|x| x as usize),
        power: cmdr_meta.power,
    };
    (StatusCode::OK, Json::from(result)).into_response()
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("elite_cmdr"),
        vec![schema_for!(CmdrQueryResult)],
    )
}
