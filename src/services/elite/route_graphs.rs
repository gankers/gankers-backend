use axum::{
    extract::{Path, Query, State},
    response::{IntoResponse, Response},
    routing::get,
    Json, Router,
};
use chrono::{DateTime, Utc};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::error;

use crate::{
    db::elite_graphs::{
        get_hourly_rank_count_for_week, get_hourly_ship_count_for_week,
        get_hourly_system_count_for_week, get_kills_and_death_by_week_for_cmdr,
        get_week_summary_metrics, get_weekly_board_progress_all, get_weekly_board_progress_top_ten,
        get_weekly_sum_series,
    },
    util::generic_error::GenericMessage,
    AppState,
};

pub(super) fn get_graphs_router() -> Router<AppState> {
    Router::<AppState>::new()
        .route("/weekly-board", get(get_weekly_board_data))
        .route("/weekly-grouped", get(get_weekly_grouped))
        .route("/week-hourly-grouped", get(get_week_hourly_grouped))
        .route(
            "/weekly-kd-grouped/:cmdr",
            get(get_kills_and_death_by_week_for_cmdr_route),
        )
        .route("/week-hourly-systems", get(get_week_hourly_systems))
        .route("/week-hourly-ranks", get(get_week_hourly_ranks))
        .route("/week-hourly-ships", get(get_week_hourly_ships))
        .route("/week-summary", get(get_week_summary))
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct WeeklyBoardQuery {
    #[serde(default = "default_ts")]
    pub(crate) ts: String,
}

fn default_ts() -> String {
    "".to_string()
}

async fn get_week_hourly_systems(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let response = match get_hourly_system_count_for_week(&state.db, ts).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("failed to get data".to_string())),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(response)).into_response()
}

async fn get_week_summary(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let response = match get_week_summary_metrics(&state.db, ts).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("failed to get data".to_string())),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(response)).into_response()
}

async fn get_week_hourly_ranks(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let response = match get_hourly_rank_count_for_week(&state.db, ts).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("failed to get data".to_string())),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(response)).into_response()
}

async fn get_week_hourly_ships(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let response = match get_hourly_ship_count_for_week(&state.db, ts).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("failed to get data".to_string())),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(response)).into_response()
}

async fn get_kills_and_death_by_week_for_cmdr_route(
    State(state): State<AppState>,
    Path(cmdr): Path<String>,
) -> Response {
    let result = match get_kills_and_death_by_week_for_cmdr(&state.db, cmdr).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("failed to get data".to_string())),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(result)).into_response()
}

fn make_ts(ts_str: &str) -> Result<DateTime<Utc>, Response> {
    match ts_str.len() {
        0 => Ok(Utc::now()),
        _ => match DateTime::parse_from_rfc3339(ts_str) {
            Ok(val) => Ok(val.to_utc()),
            Err(err) => Err((
                StatusCode::BAD_REQUEST,
                Json::from(GenericMessage::new(format!(
                    "ts Query param must define a RFC 3339 Timestamp: {}",
                    err
                ))),
            )
                .into_response()),
        },
    }
}

async fn get_weekly_board_data(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let result = match get_weekly_board_progress_top_ten(&state.db, ts).await {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to get Weekly Board Data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "failed to get weekly board data".to_string(),
                )),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(result)).into_response()
}

async fn get_weekly_grouped(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let result = match get_weekly_sum_series(&state.db, ts).await {
        Ok(data) => data,
        Err(err) => {
            error!("Failed to get Weekly Sum Series Data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "failed to get weekly sum series".to_string(),
                )),
            )
                .into_response();
        }
    };

    (StatusCode::OK, Json::from(result)).into_response()
}

async fn get_week_hourly_grouped(
    State(state): State<AppState>,
    Query(q): Query<WeeklyBoardQuery>,
) -> Response {
    let ts = match make_ts(&q.ts) {
        Ok(x) => x,
        Err(resp) => return resp,
    };

    let result = match get_weekly_board_progress_all(&state.db, ts).await {
        Ok(data) => data,
        Err(err) => {
            error!("Failed to get Weekly Sum Series Data: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "failed to get weekly sum series".to_string(),
                )),
            )
                .into_response();
        }
    };
    (StatusCode::OK, Json::from(result)).into_response()
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    use schemars::schema_for;

    use crate::db::elite_graphs::{
        TimedCmdrKillsEntry, TimedKillsDeathsEntry, TimedKillsEntry, TimedRankKillsDeathsEntry,
        TimedShipKillsDeathsEntry, TimedSystemKillsEntry, WeekSummary, WeekSummaryRankEntry,
        WeekSummaryShipEntry, WeekSummarySystemEntry,
    };
    (
        String::from("elite_graphs"),
        vec![
            schema_for!(TimedKillsEntry),
            schema_for!(TimedKillsDeathsEntry),
            schema_for!(TimedCmdrKillsEntry),
            schema_for!(TimedShipKillsDeathsEntry),
            schema_for!(TimedRankKillsDeathsEntry),
            schema_for!(TimedSystemKillsEntry),
            schema_for!(WeekSummarySystemEntry),
            schema_for!(WeekSummaryShipEntry),
            schema_for!(WeekSummaryRankEntry),
            schema_for!(WeekSummary),
        ],
    )
}
