use super::KillEvent;

#[derive(Clone, Eq, Hash, PartialEq)]
pub(crate) struct InteractionKey(pub(crate) String);

impl Ord for InteractionKey {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl PartialOrd for InteractionKey {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl From<&KillEvent> for InteractionKey {
    fn from(value: &KillEvent) -> Self {
        let interaction_key = if value.killer.as_str() > value.victim.as_str() {
            format!(
                "{}:{}",
                value.killer.to_lowercase(),
                value.victim.to_lowercase()
            )
        } else {
            format!(
                "{}:{}",
                value.victim.to_lowercase(),
                value.killer.to_lowercase()
            )
        };

        InteractionKey(interaction_key)
    }
}

pub(crate) enum InsertionState {
    Create,
    PostCreate(i32),
    Update(i32),
    Ignore,
}

pub(crate) struct KillEventWithDiff {
    pub(crate) event: KillEvent,
    pub(crate) insertion_state: InsertionState,
}

impl KillEventWithDiff {
    pub(crate) fn new_create(event: KillEvent) -> KillEventWithDiff {
        KillEventWithDiff {
            event,
            insertion_state: InsertionState::Create,
        }
    }

    pub(crate) fn new_modify(event: KillEvent, entry_id_to_update: i32) -> KillEventWithDiff {
        KillEventWithDiff {
            event,
            insertion_state: InsertionState::Update(entry_id_to_update),
        }
    }

    pub(crate) fn new_ignore(event: KillEvent) -> KillEventWithDiff {
        KillEventWithDiff {
            event,
            insertion_state: InsertionState::Ignore,
        }
    }
}
