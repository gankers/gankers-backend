use axum::Router;
use chrono::{DateTime, Utc};
use route_cmdr::get_cmdr_router;
use route_graphs::get_graphs_router;
use route_powers::get_powers_router;
use route_system::get_systems_router;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

mod route_cmdr;
mod route_graphs;
mod route_leaderboard;
mod route_powers;
mod route_search;
mod route_submit;
mod route_system;

pub(crate) mod types;
use crate::{util::powerplay::is_string_powerplay_name, AppState};

use self::{
    route_leaderboard::get_leaderboard_router, route_search::get_search_router,
    route_submit::get_submit_router,
};

/// This router contains all the routes that have anything to do with Elite.
/// There is a separate Router for Star Citizen that mirrors the functionality
pub(super) fn create_elite_router(state: &AppState) -> Router<AppState> {
    Router::new()
        .nest("/leaderboard", get_leaderboard_router())
        .nest("/search", get_search_router())
        .nest("/cmdrs", get_cmdr_router())
        .nest("/systems", get_systems_router())
        .nest("/graphs", get_graphs_router())
        .nest("/powers", get_powers_router())
        .nest("/submit", get_submit_router(&state.clone()))
}

#[derive(Debug, Serialize, Deserialize, Clone, JsonSchema)]
pub(crate) struct KillEvent {
    pub(crate) kill_id: Option<i32>,
    pub(crate) system: Option<String>,
    pub(crate) killer: String,
    pub(crate) killer_rank: i16,
    pub(crate) victim: String,
    pub(crate) victim_rank: i16,
    pub(crate) killer_ship: Option<String>,
    pub(crate) victim_ship: Option<String>,
    pub(crate) killer_squadron: Option<String>,
    pub(crate) victim_squadron: Option<String>,
    pub(crate) killer_power: Option<String>,
    pub(crate) victim_power: Option<String>,
    pub(crate) kill_timestamp: DateTime<chrono::Utc>,
}

impl KillEvent {
    pub(crate) fn validate(&self) -> Result<(), Vec<String>> {
        let mut issues = Vec::new();

        if self.kill_timestamp > Utc::now() {
            issues.push(String::from("Kill happened in the future. Rejecting"))
        }

        if let Some(squad) = &self.killer_squadron {
            if squad.len() != 4 {
                issues.push(format!(
                    "Killer Squadron [{}] has a length of {} when 4 was expected.",
                    squad,
                    squad.len()
                ));
            }
        }

        if let Some(squad) = &self.victim_squadron {
            if squad.len() != 4 {
                issues.push(format!(
                    "Victim Squadron [{}] has a length of {} when 4 was expected.",
                    squad,
                    squad.len()
                ));
            }
        }

        if let Some(power) = &self.victim_power {
            if !is_string_powerplay_name(power) {
                issues.push(format!("Power {power} is not a known power."));
            }
        }

        if let Some(power) = &self.killer_power {
            if !is_string_powerplay_name(power) {
                issues.push(format!("Power {power} is not a known power."));
            }
        }

        match issues.is_empty() {
            true => Ok(()),
            false => Err(issues),
        }
    }
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> Vec<(String, Vec<schemars::schema::RootSchema>)> {
    vec![
        route_cmdr::get_schemas(),
        route_leaderboard::get_schemas(),
        route_search::get_schemas(),
        route_submit::get_schemas(),
        route_system::get_schemas(),
        route_graphs::get_schemas(),
        route_powers::get_schemas(),
    ]
}
