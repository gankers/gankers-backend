use axum::{
    extract::{Path, Query, State},
    response::{IntoResponse, Response},
    routing::get,
    Json, Router,
};
use chrono::Utc;
use itertools::Itertools;
use reqwest::StatusCode;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use tracing::error;

use crate::{
    db::elite_powerplay::{
        get_agent_leaderboard_for_power_and_timerange, get_all_powers_summary,
        get_power_detail_info, get_recent_killboard_entries_involving_power, PowerplayInfo,
    },
    util::{
        generic_error::GenericMessage, sized_pagination,
        weekly_summary::get_current_weekly_period_from_timestamp,
    },
};

use self::sized_pagination::{PowerplayAllowUnpledged, PowerplaySizedPaginationWithAllowUnpledged};

use super::AppState;

pub(super) fn get_powers_router() -> Router<AppState> {
    Router::<AppState>::new()
        .route("/weekly-summary", get(get_weekly_summary_all))
        .route("/weekly-summary/:power", get(get_weekly_summary_power))
        .route(
            "/weekly-summary/:power/agents",
            get(get_weekly_agents_board),
        )
        .route("/:power/recent", get(get_recent_power_kills))
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
struct PowerWeeklyAndTotalGroup {
    name: String,
    total: PowerplayInfo,
    week: PowerplayInfo,
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
struct PowerWeeklyAndTotalGroupWithTopAgents {
    name: String,
    total: PowerplayInfo,
    week: PowerplayInfo,
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
struct PowerAgentBoardEntry {
    name: String,
    kills: i64,
    deaths: i64,
}

async fn get_weekly_summary_all(
    Query(query): Query<PowerplayAllowUnpledged>,
    State(state): State<AppState>,
) -> Response {
    let bounds = get_current_weekly_period_from_timestamp(Utc::now());

    let result_week_fut = get_all_powers_summary(&state.db, Some(bounds), query.with_unpledged);
    let result_all_fut = get_all_powers_summary(&state.db, None, query.with_unpledged);

    let (result_week, result_all) = tokio::join!(result_week_fut, result_all_fut);

    let result_week = match result_week {
        Ok(r) => r,
        Err(err) => {
            error!("DB error while fetching weekly powerplay info: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching data".to_string(),
                )),
            )
                .into_response();
        }
    };
    let result_all = match result_all {
        Ok(r) => r,
        Err(err) => {
            error!("DB error while fetching total powerplay info: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching data".to_string(),
                )),
            )
                .into_response();
        }
    };

    let response = result_all
        .into_iter()
        .map(|x| {
            let week = match result_week.iter().find(|y| y.power == x.power) {
                Some(x) => x.clone(),
                None => PowerplayInfo {
                    power: x.power.clone(),
                    kills: 0,
                    deaths: 0,
                    agents: Some(0),
                },
            };

            PowerWeeklyAndTotalGroup {
                name: x.power.clone(),
                total: x,
                week,
            }
        })
        .collect_vec();
    (StatusCode::OK, Json::from(response)).into_response()
}

async fn get_weekly_summary_power(
    Query(query): Query<PowerplayAllowUnpledged>,
    State(state): State<AppState>,
    Path(power): Path<String>,
) -> Response {
    let bounds = get_current_weekly_period_from_timestamp(Utc::now());
    let week_fut = get_power_detail_info(&state.db, Some(bounds), &power, query.with_unpledged);
    let total_fut = get_power_detail_info(&state.db, None, &power, query.with_unpledged);

    let (result_week, result_all) = tokio::join!(week_fut, total_fut);

    let result_week = match result_week {
        Ok(r) => match r {
            Some(x) => x,
            None => PowerplayInfo {
                power: power.clone(),
                kills: 0,
                deaths: 0,
                agents: Some(0),
            },
        },
        Err(err) => {
            error!("DB error while fetching weekly powerplay info: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching data".to_string(),
                )),
            )
                .into_response();
        }
    };
    let result_all = match result_all {
        Ok(r) => match r {
            Some(x) => x,
            None => {
                // NO entries at all -> Power doesnt exists probably!
                return (
                    StatusCode::NOT_FOUND,
                    Json::from(GenericMessage::new(format!("Power {power} not found"))),
                )
                    .into_response();
            }
        },
        Err(err) => {
            error!("DB error while fetching total powerplay info: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching data".to_string(),
                )),
            )
                .into_response();
        }
    };

    (
        StatusCode::OK,
        Json::from(PowerWeeklyAndTotalGroup {
            name: power,
            total: result_all,
            week: result_week,
        }),
    )
        .into_response()
}

async fn get_recent_power_kills(
    pagination: Query<PowerplaySizedPaginationWithAllowUnpledged>,
    Path(power): Path<String>,
    State(state): State<AppState>,
) -> Response {
    let page = pagination.0.page as i64;

    if page < 1 {
        return (
            StatusCode::BAD_REQUEST,
            Json(GenericMessage::new("Page cannot be < 1".to_string())),
        )
            .into_response();
    }

    match get_recent_killboard_entries_involving_power(
        pagination.page_size as i64,
        page,
        &state.db,
        &power,
        pagination.with_unpledged,
    )
    .await
    {
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
        Err(err) => {
            error!("DB error while fetching recent kills: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching recent kill".to_string(),
                )),
            )
                .into_response()
        }
    }
}

async fn get_weekly_agents_board(
    State(state): State<AppState>,
    Path(power): Path<String>,
    Query(sized_pagination): Query<PowerplaySizedPaginationWithAllowUnpledged>,
) -> Response {
    let bounds = get_current_weekly_period_from_timestamp(Utc::now());

    let response = match get_agent_leaderboard_for_power_and_timerange(
        &state.db,
        bounds,
        &power,
        (sized_pagination.page as i64 - 1) * sized_pagination.page_size as i64,
        sized_pagination.page_size as i64,
        sized_pagination.with_unpledged,
    )
    .await
    {
        Err(err) => {
            error!("DB error while fetching total powerplay info: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    " error while fetching data".to_string(),
                )),
            )
                .into_response();
        }
        Ok(data) => data
            .into_iter()
            .map(|x| PowerAgentBoardEntry {
                name: x.0,
                kills: x.1,
                deaths: x.2,
            })
            .collect_vec(),
    };

    (StatusCode::OK, Json::from(response)).into_response()
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    use schemars::schema_for;

    (
        String::from("elite_powers"),
        vec![
            schema_for!(PowerWeeklyAndTotalGroup),
            schema_for!(PowerAgentBoardEntry),
        ],
    )
}
