///
/// This module contains admin-related Events.
/// To register to SSE events here, one must provide an admin token.
/// The token must be provided using the Bearer-Token, and as such does not comply with the SSE Spec
/// Anyways, given that only Services (and no browser app) actually use this Endpoint, this is more than fine.
///
use axum::{
    extract::State,
    http::HeaderValue,
    response::{
        sse::{Event, KeepAlive},
        IntoResponse, Sse,
    },
    routing::get,
    Router,
};
use futures::Stream;
use std::convert::Infallible;

use crate::AppState;

pub(super) fn get_nested_events_router() -> Router<AppState> {
    Router::new().route("/elite-historic-aggregate", get(leaderboard_sse_handler))
}

/// Pushes an Event when a Historic Upload is finished
async fn leaderboard_sse_handler(State(app_state): State<AppState>) -> impl IntoResponse {
    let rx: tokio::sync::broadcast::Receiver<String> =
        app_state.historic_updates_channel.add_client();
    let mut sse = create_leaderboard_sse_handler(rx).into_response();
    sse.headers_mut()
        .append("X-Accel-Buffering", HeaderValue::from_static("no"));
    sse
}

fn create_leaderboard_sse_handler(
    mut rx: tokio::sync::broadcast::Receiver<String>,
) -> Sse<impl Stream<Item = Result<Event, Infallible>>> {
    let stream = async_stream::stream! {
        yield Ok(Event::default().comment("connected"));
        loop {
            let msg = rx.recv().await.unwrap();
            yield Ok(Event::default().data(&msg));
        }
    };

    Sse::new(stream).keep_alive(KeepAlive::default())
}
