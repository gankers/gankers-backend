use std::env;

use crate::{
    auth::claim::AuthError,
    db::tokens::{create_new_token_for_user, Permissions, TokenData},
    util::generic_error::GenericMessage,
    AppState,
};
use axum::{
    body::Body,
    extract::State,
    http::{Request, StatusCode},
    middleware::{self, Next},
    response::{IntoResponse, Response},
    routing::post,
    Json, Router,
};
use route_backup::get_nested_backup_router;
use route_events::get_nested_events_router;
use route_reconcile::get_nested_reconcile_router;
use route_webhooks::get_nested_webhooks_router;
use tracing::{error, warn};

use self::{route_migrate::get_nested_migrate_router, route_users::get_nested_users_router_admin};

mod route_backup;
mod route_events;
mod route_migrate;
mod route_reconcile;
mod route_users;
mod route_webhooks;

pub(super) fn create_admin_router(state: &AppState) -> Router<AppState> {
    let admin_router = Router::new()
        .nest("/users", get_nested_users_router_admin())
        .nest("/migrate", get_nested_migrate_router())
        .nest("/sse", get_nested_events_router())
        .nest("/webhooks", get_nested_webhooks_router())
        .nest("/reconcile", get_nested_reconcile_router())
        .layer(middleware::from_fn_with_state(
            state.clone(),
            auth_validator_admin,
        ))
        .nest("/backup", get_nested_backup_router(state));

    if env::var("ADMIN_INIT").is_ok() {
        for _ in 0..10 {
            warn!("### WARNING ### ADMIN_INIT IS SET");
        }
        warn!("Do a POST against /admin/init to get your Admin Token. Call it multiple times to get multiple Tokens. Tokens are assigned to User 0 (Technical User)");
        admin_router.route("/init", post(admin_init_handler))
    } else {
        admin_router
    }
}

async fn admin_init_handler(State(state): State<AppState>) -> Response {
    warn!("### WARNING ### ADMIN INIT ENDPOINT WAS USED.");
    match create_new_token_for_user(
        &TokenData {
            // 0 -> Technical User
            owner_discord_id: 0,
            revoke_on_new_token: false,
            revoked: false,
            permissions: Permissions {
                admin: true,
                self_api: true,
                elite_submit: true,
            },
            hashed_token: None,
        },
        &state.db,
    )
    .await
    {
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
        Err(err) => {
            error!("DB Error on created admin token: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Something failed while trying to create Admin Token".to_string(),
                )),
            )
                .into_response()
        }
    }
}

pub(self) async fn auth_validator_admin(
    token_data_opt: Option<TokenData>,
    request: Request<Body>,
    next: Next,
) -> Response<Body> {
    return auth_validator(token_data_opt, request, |x| x.admin, next).await;
}

pub(self) async fn auth_validator_any_valid(
    token_data_opt: Option<TokenData>,
    request: Request<Body>,
    next: Next,
) -> Response<Body> {
    return auth_validator(token_data_opt, request, |_| true, next).await;
}

pub(self) async fn auth_validator(
    token_data_opt: Option<TokenData>,
    request: Request<Body>,
    required_permission: impl Fn(Permissions) -> bool,
    next: Next,
) -> Response<Body> {
    if token_data_opt.is_none() {
        return AuthError::MissingCredentials.into_response();
    }
    let token_data = token_data_opt.unwrap();

    if token_data.revoked {
        return AuthError::RevokedToken.into_response();
    }

    if !required_permission(token_data.permissions) {
        return AuthError::Forbidden.into_response();
    }

    next.run(request).await
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> Vec<(String, Vec<schemars::schema::RootSchema>)> {
    vec![route_migrate::get_schemas(), route_users::get_schemas()]
}
