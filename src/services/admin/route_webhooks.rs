use axum::{
    extract::State,
    response::{IntoResponse, Response},
    routing::post,
    Json, Router,
};
use chrono::Utc;
use reqwest::StatusCode;
use tracing::{error, info};

use crate::{util::generic_error::GenericMessage, AppState};

pub(super) fn get_nested_webhooks_router() -> Router<AppState> {
    Router::new().route("/weekly/enqueue", post(enqueue_weekly_immediate))
}

async fn enqueue_weekly_immediate(State(state): State<AppState>) -> Response {
    // Acquire the write lock, change the next update time to nowm
    // which should then get picked up by our Event Loop

    // This is blocking until the write lock is acquired. Locks shouldnt take long, so it's fine
    match state.emit_weekly_summary_not_before.write() {
        Ok(mut w) => {
            let new_time = Utc::now();
            *w = new_time;
            info!("Set the next weekly time to {}", new_time);

            (StatusCode::NO_CONTENT).into_response()
        }
        Err(err) => {
            error!("Failed to acquire lock: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to get Account Info from DB".to_string(),
                )),
            )
                .into_response()
        }
    }
}
