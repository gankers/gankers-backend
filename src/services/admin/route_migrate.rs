use axum::{
    extract::{self, DefaultBodyLimit, State},
    response::Response,
    routing::post,
    Router,
};
use chrono::{DateTime, Utc};
use itertools::Itertools;
use schemars::{schema_for, JsonSchema};
use serde::Deserialize;
use tracing::info;

use crate::{
    db::tokens::TokenData,
    services::{elite::KillEvent, historic::historic_elite::append_kill_events_to_job_queue},
    AppState,
};

pub(super) fn get_nested_migrate_router() -> Router<AppState> {
    Router::new().route(
        "/elite",
        // This endpoint is capped at a payload of 50MByte
        post(upload_elite_kills_old_db).layer(DefaultBodyLimit::max(50 * 1024 * 1024)),
    )
}

#[derive(Deserialize, JsonSchema)]
struct OldDbData {
    timestamp: DateTime<Utc>,
    killer_name: String,
    killer_rank: i16,
    killer_ship: Option<String>,
    victim_name: String,
    victim_rank: i16,
    victim_ship: Option<String>,
    location: Option<String>,
}

impl From<&OldDbData> for KillEvent {
    fn from(value: &OldDbData) -> Self {
        KillEvent {
            kill_id: None,
            system: value.location.clone(),
            killer: value.killer_name.clone(),
            killer_rank: value.killer_rank,
            victim: value.victim_name.clone(),
            victim_rank: value.victim_rank,
            killer_ship: value.killer_ship.clone(),
            victim_ship: value.victim_ship.clone(),
            killer_squadron: None,
            victim_squadron: None,
            kill_timestamp: value.timestamp,
            killer_power: None,
            victim_power: None,
        }
    }
}

async fn upload_elite_kills_old_db(
    State(state): State<AppState>,
    creator_token: Option<TokenData>,
    body: extract::Json<Vec<OldDbData>>,
) -> Response {
    // We piggyback onto the Historic Aggregation Logic for this :)
    let kill_events = body.iter().map(KillEvent::from).collect_vec();

    let token = creator_token.unwrap();

    info!(
        "User {} sent a Migration of Old DB Data!",
        token.owner_discord_id
    );

    match append_kill_events_to_job_queue(token.hashed_token.unwrap(), kill_events, state) {
        Ok(data) => data,
        Err(data) => data,
    }
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("admin_migrate"),
        vec![(schema_for!(OldDbData))],
    )
}
