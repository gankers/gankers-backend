use axum::{
    extract::State,
    middleware,
    response::{IntoResponse, Response},
    routing::get,
    Json, Router,
};
use chrono::{DateTime, Utc};
use itertools::Itertools;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use tracing::error;

use crate::AppState;

use super::{auth_validator_admin, auth_validator_any_valid, GenericMessage};

pub(super) fn get_nested_backup_router(state: &AppState) -> Router<AppState> {
    Router::new()
        .nest(
            "/private",
            Router::new()
                .route("/elite_kills", get(get_admin_elite_commanders_backup))
                .layer(middleware::from_fn_with_state(
                    state.clone(),
                    auth_validator_admin,
                )),
        )
        .nest(
            "/public",
            Router::new()
                .route("/elite_kills", get(get_safe_elite_commanders_backup))
                .layer(middleware::from_fn_with_state(
                    state.clone(),
                    auth_validator_any_valid,
                )),
        )
}

#[derive(Serialize, Deserialize)]
pub(super) struct KillboardEntryDbWithSecrets {
    pub(super) kill_id: Option<i32>,
    pub(super) interaction_index: Option<String>,
    pub(super) killer: Option<String>,
    pub(super) killer_squadron: Option<String>,
    pub(super) killer_rank: Option<i16>,
    pub(super) killer_ship: Option<String>,
    pub(super) victim: Option<String>,
    pub(super) victim_squadron: Option<String>,
    pub(super) victim_rank: Option<i16>,
    pub(super) victim_ship: Option<String>,
    pub(super) kill_timestamp: Option<DateTime<Utc>>,
    pub(super) system_name: Option<String>,
    pub(super) killer_power: Option<String>,
    pub(super) victim_power: Option<String>,
    pub(super) hashed_token_creator: Option<String>,
    pub(super) hashed_token_updater: Option<String>,
}

async fn dump_table(pool: &PgPool) -> Result<Vec<KillboardEntryDbWithSecrets>, sqlx::Error> {
    let result: Vec<KillboardEntryDbWithSecrets> =
        sqlx::query_as!(KillboardEntryDbWithSecrets, "SELECT * FROM elite_kills")
            .fetch_all(pool)
            .await?;

    return Ok(result);
}

async fn get_safe_elite_commanders_backup(State(state): State<AppState>) -> Response {
    let result = match dump_table(&state.db).await {
        Ok(data) => data
            .into_iter()
            .map(|mut x| {
                x.hashed_token_creator = Some("redacted".to_string());
                x.hashed_token_updater = Some("redacted".to_string());
                x
            })
            .collect_vec(),
        Err(err) => {
            error!("Failed to get backup due to DB errro: {err}");
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to get full backup due to DB Error".to_string(),
                )),
            )
                .into_response();
        }
    };
    (StatusCode::OK, Json::from(result)).into_response()
}

async fn get_admin_elite_commanders_backup(State(state): State<AppState>) -> Response {
    let result = match dump_table(&state.db).await {
        Ok(data) => data,
        Err(err) => {
            error!("Failed to get backup due to DB errro: {err}");
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to get full backup due to DB Error".to_string(),
                )),
            )
                .into_response();
        }
    };
    (StatusCode::OK, Json::from(result)).into_response()
}
