use axum::{extract::State, response::IntoResponse, routing::post, Json, Router};
use reqwest::StatusCode;

use crate::{db::elite_historic_kills::update_data_for_commanders, AppState};

use super::GenericMessage;

pub(super) fn get_nested_reconcile_router() -> Router<AppState> {
    Router::new().route("/elite_commanders", post(reconcile_elite_commanders))
}

/// Pushes an Event when a Historic Upload is finished
async fn reconcile_elite_commanders(State(app_state): State<AppState>) -> impl IntoResponse {
    let result = match app_state.db.begin().await {
        Ok(mut tx) => {
            if let Err(err) = update_data_for_commanders(None, &mut tx).await {
                Err(format!("Failed to run full reconcile: {err}"))
            } else if let Err(err) = tx.commit().await {
                Err(format!(
                    "Failed to run commit transaction for reconcile: {err}"
                ))
            } else {
                Ok(())
            }
        }
        Err(err) => Err(format!(
            "Failed to acquire TX to run CMDR Table reconcile. Db Error: {err}"
        )),
    };

    (
        match result {
            Ok(_) => StatusCode::OK,
            Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
        },
        Json::from(GenericMessage::new(match result {
            Ok(_) => "Full Reconcile was a success".to_string(),
            Err(err) => err,
        })),
    )
        .into_response()
}
