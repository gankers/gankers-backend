use axum::{
    extract::{self, Path, State},
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::{delete, get, post},
    Json, Router,
};
use schemars::{schema_for, JsonSchema};
use serde::{Deserialize, Serialize};
use tracing::{error, info};

use crate::{
    db::{
        accounts::{
            create_account_by_discord_id, get_account_info_by_discord_id,
            patch_account_by_discord_id,
        },
        tokens::{create_new_token_for_user, get_all_tokens_for_user, TokenData},
    },
    util::generic_error::GenericMessage,
    AppState,
};

///
/// Similar to self router, except to be called by an admin / by the Bot. Also has some additional routes like creating an account, deleting account, suspending / banning account
///
pub fn get_nested_users_router_admin() -> Router<AppState> {
    Router::new()
        .route("/", post(register_account))
        .route("/:discord_id", get(get_account_by_id).patch(patch_account))
        .route("/:discord_id/tokens", get(get_tokens_for_account_by_id))
        .route("/:discord_id/tokens", post(post_new_token))
        .route(
            "/:discord_id/tokens/:token_name",
            delete(|| async { "TODO: Delete Token" }),
        )
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct AccountMeta {
    discord_id: String,
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct RegisterAccountBody {
    discord_id: i64,
}

async fn register_account(
    creator_token: Option<TokenData>,
    State(state): State<AppState>,
    extract::Json(body): extract::Json<RegisterAccountBody>,
) -> Response {
    // First check, if an account already exists and return a 409 on Conflict
    match get_account_info_by_discord_id(body.discord_id, &state.db).await {
        Ok(entry) => {
            if entry.is_some() {
                return (
                    StatusCode::CONFLICT,
                    Json::from(GenericMessage::new(
                        "An Account with the provided Discord ID already exists!".to_string(),
                    )),
                )
                    .into_response();
            }
        }
        Err(err) => {
            error!("Failed to get Account Info from DB: {}", err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to get Account Info from DB".to_string(),
                )),
            )
                .into_response();
        }
    }

    match create_account_by_discord_id(body.discord_id, &state.db).await {
        Err(err) => {
            error!("Failed to insert Account from DB: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to insert Account Info into DB".to_string(),
                )),
            )
                .into_response()
        }
        Ok(data) => {
            info!(
                "Account {} registered user for Account {}",
                creator_token.unwrap().owner_discord_id,
                body.discord_id
            );

            (StatusCode::OK, Json::from(data)).into_response()
        }
    }
}

#[derive(Serialize, Deserialize, Debug, JsonSchema)]
struct PatchBody {
    is_banned: Option<bool>,
    spoof_location_duration_minutes: Option<u16>,
}

impl PatchBody {
    fn has_any_changes(&self) -> bool {
        self.is_banned.is_some() || self.spoof_location_duration_minutes.is_some()
    }
}

async fn patch_account(
    creator_token: Option<TokenData>,
    State(state): State<AppState>,
    Path(discord_id): Path<i64>,
    extract::Json(body): extract::Json<PatchBody>,
) -> Response {
    if !body.has_any_changes() {
        return (
            StatusCode::BAD_REQUEST,
            Json::from(GenericMessage::new(
                "Received an Empty Patch. Ignoring".to_string(),
            )),
        )
            .into_response();
    }

    if let Some(banned) = body.is_banned {
        info!(
            "User {} set banned={} for User {}",
            creator_token.as_ref().unwrap().owner_discord_id,
            banned,
            discord_id
        );
    }

    if let Some(duration) = body.spoof_location_duration_minutes {
        if duration > 60 * 24 {
            return (
                StatusCode::BAD_REQUEST,
                Json::from(GenericMessage::new(
                    "The maximum spoofing duration is 24 hours".to_string(),
                )),
            )
                .into_response();
        }
        info!(
            "User {} set spoofing_duration={} for User {}",
            creator_token.as_ref().unwrap().owner_discord_id,
            duration,
            discord_id
        );
    }

    match patch_account_by_discord_id(
        discord_id,
        body.is_banned,
        body.spoof_location_duration_minutes,
        &state.db,
    )
    .await
    {
        Ok(response) => match response {
            Some(data) => (StatusCode::OK, Json::from(data)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json::from(GenericMessage::new(
                    "No Account found with provided Discord ID.".to_string(),
                )),
            )
                .into_response(),
        },
        Err(err) => {
            error!("DB error while trying to patch user: {}", err);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "DB error while trying to patch user".to_string(),
                )),
            )
                .into_response()
        }
    }
}

async fn get_account_by_id(State(state): State<AppState>, Path(discord_id): Path<i64>) -> Response {
    match get_account_info_by_discord_id(discord_id, &state.db).await {
        Ok(data_opt) => match data_opt {
            Some(payload) => (StatusCode::OK, Json::from(payload)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json::from(GenericMessage::new(
                    "No Account found with provided ID".to_string(),
                )),
            )
                .into_response(),
        },
        Err(err) => {
            error!("Failed to get Account by ID. DB Error: {}", err);

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Couldn't get account data because of a DB Error".to_string(),
                )),
            )
                .into_response()
        }
    }
}

async fn get_tokens_for_account_by_id(
    Path(discord_id): Path<String>,
    State(state): State<AppState>,
) -> Result<(StatusCode, Json<Vec<TokenData>>), (StatusCode, Json<GenericMessage>)> {
    let discord_id_number = match (discord_id).parse::<i64>() {
        Ok(val) => Ok(val),
        Err(_) => Err((
            StatusCode::BAD_REQUEST,
            Json::from(GenericMessage::new(
                "Failed to parse Discord ID as i64".to_string(),
            )),
        )),
    }?;
    let tokens = get_all_tokens_for_user(discord_id_number, &state.db)
        .await
        .map_err(|_| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new("DB Error".to_string())),
            )
        })?;

    // Admin also gets the revoked tokens.
    // Self does not provive that info
    Ok((StatusCode::OK, Json::from(tokens)))
}

async fn post_new_token(
    State(state): State<AppState>,
    Path(discord_id): Path<i64>,
    Json(mut token_data): Json<TokenData>,
) -> Response {
    token_data.hashed_token = None;
    token_data.owner_discord_id = discord_id;
    if token_data.permissions.admin {
        return (
            StatusCode::BAD_REQUEST,
            Json::from(GenericMessage::new(
                "Admins cannot create new Admin Tokens for other users.".to_string(),
            )),
        )
            .into_response();
    }

    // First get account info and make sure the user isnt banned and does indeed exist
    let account_info = match get_account_info_by_discord_id(discord_id, &state.db).await {
        Ok(x) => match x {
            Some(x) => x,
            None => {
                return (
                    StatusCode::NOT_FOUND,
                    Json::from(GenericMessage::new(
                        "No Account found with provided ID".to_string(),
                    )),
                )
                    .into_response()
            }
        },
        Err(e) => {
            error!(
                "Failed to fetch info for user {}: {}",
                token_data.owner_discord_id, e
            );

            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to create new Token for User because of a DB Issue".to_string(),
                )),
            )
                .into_response();
        }
    };

    if account_info.is_banned {
        return (
            StatusCode::FORBIDDEN,
            Json::from(GenericMessage::new(
                "The account for which the Token is getting issued is banned".to_string(),
            )),
        )
            .into_response();
    }

    match create_new_token_for_user(&token_data, &state.db).await {
        Ok(data) => (StatusCode::OK, Json::from(data)).into_response(),
        Err(err) => {
            error!(
                "Failed to create new Token for User {}: {}",
                token_data.owner_discord_id, err
            );

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json::from(GenericMessage::new(
                    "Failed to create new Token for User because of a DB Issue".to_string(),
                )),
            )
                .into_response()
        }
    }
}

#[cfg(debug_assertions)]
pub(super) fn get_schemas() -> (String, Vec<schemars::schema::RootSchema>) {
    (
        String::from("admin_users"),
        vec![
            schema_for!(PatchBody),
            schema_for!(AccountMeta),
            schema_for!(RegisterAccountBody),
        ],
    )
}
