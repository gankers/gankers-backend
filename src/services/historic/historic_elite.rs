use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use chrono::TimeDelta;
use itertools::Itertools;
use std::{
    collections::{HashMap, VecDeque},
    time::Duration,
};

use tracing::{error, warn};
use uuid::Uuid;

use crate::{
    db::{
        elite_historic_kills::{
            get_all_entries_containing_interaction_indices_in_timeframe, handle_diff_entries,
        },
        elite_kills::is_update_needed,
    },
    services::{
        elite::{
            types::{InteractionKey, KillEventWithDiff},
            KillEvent,
        },
        types::HistoricQueueTask,
    },
    util::generic_error::GenericMessage,
    AppState,
};

use super::historic_loop::emit_enqueued_dm;

pub(super) async fn run_elite_historic_procedure(
    state: AppState,
    token_hash: String,
    events: Vec<KillEvent>,
) -> Result<Vec<KillEventWithDiff>, sqlx::Error> {
    // First, get all interactions

    let mut valid_events = VecDeque::new();
    let mut invalid_events = VecDeque::new();

    for event in events.into_iter() {
        match event.validate() {
            Ok(_) => valid_events.push_back(event),
            Err(err) => invalid_events.push_back((event, err)),
        }
    }

    if !invalid_events.is_empty() {
        let ts_list_string = invalid_events
            .iter()
            .map(|(x, _)| x.kill_timestamp.to_rfc2822())
            .join(", ");
        warn!("Ignored {} events because of bad structure from historic aggregate with the following Timestamps: {}", invalid_events.len(), ts_list_string);
    }

    let mut all_interactions: Vec<_> = valid_events
        .iter()
        .map(|x| InteractionKey::from(x).0)
        .unique()
        .collect();
    all_interactions.sort();

    let time_bounds = match valid_events.iter().map(|x| x.kill_timestamp).minmax() {
        itertools::MinMaxResult::NoElements => None,
        itertools::MinMaxResult::OneElement(x) => Some((x, x)),
        itertools::MinMaxResult::MinMax(min, max) => Some((min, max)),
    }
    .unwrap(); // unwrap is fine. We will *never* have NoElements because we return early at the start of the function.

    // Extend time bounds. Game is not really precise with the log times.
    // In case an already present event was added for example 5s later, it will be detected because of this expansion.

    let time_bounds = (
        time_bounds.0 - Duration::from_secs(5),
        time_bounds.1 + Duration::from_secs(5),
    );

    let data = get_all_entries_containing_interaction_indices_in_timeframe(
        &all_interactions,
        time_bounds.0,
        time_bounds.1,
        &state.db,
    )
    .await
    .map_err(|x| {
        error!("DB Interaction failed: {}", x);
        x
    })?;

    let db_state = data
        .into_iter()
        .map(|x| (x.kill_id.unwrap(), x))
        .collect_vec();
    let diff_actions = compare_state_between_db_and_input(db_state, valid_events.into());

    // Entries to Diff has been discovered. Hand over to DB Module to insert for us
    handle_diff_entries(diff_actions, &state.db, &token_hash)
        .await
        .map_err(|x| {
            error!("DB Interaction failed: {}", x);
            x
        })

    // TODO: Somehow write the result to a SSE so the Discord Bot can push the result via DMs to the sender
}

///
/// Contains util / businiess logic tied to handling aggregate events
///
pub(super) fn compare_state_between_db_and_input(
    db_state: Vec<(i32, KillEvent)>,
    received_state: Vec<KillEvent>,
) -> Vec<KillEventWithDiff> {
    let lookup = create_interaction_indexed_map(&db_state);

    // For each event, add data about whether an entry is to be added, updated or be ignored

    received_state
        .into_iter()
        .map(|kill_event| {
            let interaction = InteractionKey::from(&kill_event).0;

            match lookup.get(&interaction) {
                None => {
                    // No interactions found, at all.
                    // -> No such event exists yet. Create one
                    KillEventWithDiff::new_create(kill_event)
                }
                Some(data) => {
                    // First get a subslice that only contains time-entries within a +/- 5s margin to the current event
                    let mut relevant_entries = data
                        .iter()
                        .filter(|&list_event| {
                            let timestamp_from_list = list_event.1.kill_timestamp;
                            let own_timestamp = kill_event.kill_timestamp;

                            // This basically contains a "rolling window" of allowance
                            (timestamp_from_list - own_timestamp).abs() < TimeDelta::seconds(5)

                        })
                        .collect_vec();

                    if relevant_entries.len() > 1 {
                        warn!("More than 1 entry was found for an Event. Tried looking for Events with Interaction {} around Time {} and found {:?}",
                        &interaction, kill_event.kill_timestamp, &relevant_entries);
                    }

                    match relevant_entries.pop() {
                        Some(data) => {

                            // Determine if the new event contains any useful info missing in the current
                            match is_update_needed(data.1, &kill_event) {
                                true => KillEventWithDiff::new_modify(kill_event, data.0),
                                false => KillEventWithDiff::new_ignore(kill_event),
                            }
                        },
                        None => {
                            KillEventWithDiff::new_create(kill_event)
                        },
                    }
                }
            }
        })
        .collect_vec()
}

fn create_interaction_indexed_map(
    db_state: &Vec<(i32, KillEvent)>,
) -> HashMap<String, Vec<(i32, &KillEvent)>> {
    let mut lookup: HashMap<String, Vec<(i32, &KillEvent)>> =
        HashMap::with_capacity(db_state.len());

    for (kill_id, entry) in db_state {
        let interaction = InteractionKey::from(entry).0;
        let interaction_str = interaction.as_str();

        match lookup.get_mut(interaction_str) {
            Some(list) => {
                list.push((*kill_id, entry));
            }
            None => {
                lookup.insert(interaction, vec![(*kill_id, entry)]);
            }
        };
    }

    lookup
}

pub(crate) fn append_kill_events_to_job_queue(
    token_hash: String,
    events: Vec<KillEvent>,
    state: AppState,
) -> Result<Response, Response> {
    let identifier = Uuid::new_v4().to_string();
    let task = HistoricQueueTask {
        identifier: identifier.clone(),
        task: crate::services::types::HistoricQueueTaskData::EliteHistoricQueueTask {
            token_submitter: token_hash.clone(),
            payload: events,
        },
    };
    let position_of_job = match state.historic_queue.lock() {
        Err(x) => {
            error!("Failed to lock mutex because it is poisoned! {}", x);
            return Err((StatusCode::INTERNAL_SERVER_ERROR, "Mutex poisoned").into_response());
        }
        Ok(mut e) => {
            let len = (*e).len();
            if len > 20 {
                Err(len)
            } else {
                (*e).push_front(task);
                Ok(len + 1)
            }
        }
    };
    let status = match position_of_job.is_ok() {
        true => StatusCode::OK,
        false => StatusCode::SERVICE_UNAVAILABLE,
    };

    let queue_position_opt = match position_of_job {
        Ok(val) => Some(val),
        Err(_) => None,
    };
    let state_clone = state.clone();
    // Spawn a tokio task that will get the Discord ID from the Token, then try to send a Discord DM in the form of an emitted SSE Event
    tokio::spawn(emit_enqueued_dm(
        state_clone,
        token_hash,
        queue_position_opt,
        identifier.clone(),
    ));

    let message = match position_of_job {
        Ok(queue_position) => format!(
            "Your job (id: {}) is in queue #{}",
            identifier, queue_position
        ),
        Err(_) => "The queue is currently full. Please try again later. If this issue persists, send a Message to WDX on Discord.".to_string(),
    };
    Ok((status, Json::from(GenericMessage::new(message.to_string()))).into_response())
}

#[cfg(test)]
mod test {
    use chrono::{DateTime, Utc};
    use pretty_assertions::assert_eq;
    use serde::{Deserialize, Serialize};
    use sqlx::PgPool;
    use std::{
        collections::VecDeque,
        sync::{Arc, Mutex, RwLock},
    };

    use crate::{
        auth::claim::RawToken, db::elite_cmdrs::get_kill_history_for_cmdr,
        services::types::SseBroadcaster,
    };

    use super::*;

    fn util_get_example_kill_event(
        killer_name: &str,
        victim_name: &str,
        ts: DateTime<Utc>,
    ) -> KillEvent {
        KillEvent {
            kill_id: None,
            system: Some("ExampleSystem".to_string()),
            killer: killer_name.to_string(),
            killer_rank: 1,
            victim: victim_name.to_string(),
            victim_rank: 2,
            killer_ship: Some("python_nx".to_string()),
            victim_ship: None,
            killer_squadron: None,
            victim_squadron: None,
            kill_timestamp: ts,
            killer_power: None,
            victim_power: None,
        }
    }

    #[test]
    fn create_interaction_indexed_map_test() {
        // Arrange
        let ts_0: DateTime<Utc> = DateTime::parse_from_rfc3339("2024-07-15T18:00:00Z")
            .unwrap()
            .into();
        let ev1 = util_get_example_kill_event(
            "ExampleKiller",
            "ExampleVictim",
            ts_0 + Duration::from_secs(30),
        );
        let ev2 = util_get_example_kill_event(
            "ExampleKiller2",
            "ExampleVictim2",
            ts_0 + Duration::from_secs(0),
        );
        let ev3 = util_get_example_kill_event(
            "ExampleKiller",
            "ExampleVictim",
            ts_0 + Duration::from_secs(0),
        );
        let ev4 = util_get_example_kill_event(
            "ExampleKiller",
            "ExampleVictim2",
            ts_0 + Duration::from_secs(60),
        );

        let input: Vec<(i32, KillEvent)> = vec![(1, ev1), (2, ev2), (3, ev3), (4, ev4)];

        // Act
        let result = create_interaction_indexed_map(&input);

        // Assert
        assert_eq!(3, result.len());
        assert_eq!(2, result.get("examplevictim:examplekiller").unwrap().len());
        assert_eq!(
            1,
            result.get("examplevictim2:examplekiller2").unwrap().len()
        );
        assert_eq!(1, result.get("examplevictim2:examplekiller").unwrap().len());
    }

    #[derive(Serialize, Deserialize)]
    struct TestInputCase {
        name: String,
        event: Vec<KillEvent>,
    }

    #[derive(Serialize, Deserialize)]
    struct TestInput {
        events: Vec<TestInputCase>,
    }

    #[sqlx::test]
    async fn historic_insert_and_update_intergration_test(db: PgPool) {
        //// Arrange
        let test_inputs_str = include_str!("./test/historic-test-case.yml");
        let test_inputs = serde_yaml::from_str::<TestInput>(test_inputs_str).unwrap();

        let queue_mutex: Mutex<VecDeque<HistoricQueueTask>> =
            Mutex::new(VecDeque::with_capacity(64));
        let queue_arc = Arc::new(queue_mutex);

        let state_db = db.clone();
        let app_state = AppState {
            db: state_db,
            historic_queue: queue_arc,
            live_kills_channel: SseBroadcaster::new(),
            historic_updates_channel: SseBroadcaster::new(),
            emit_weekly_summary_not_before: Arc::new(RwLock::new(Utc::now())),
        };

        let token = RawToken {
            token: String::from("TEST"),
        };

        //// Act
        // Iterate over all events
        let mut results: Vec<Vec<KillEventWithDiff>> = Vec::with_capacity(test_inputs.events.len());
        for element in test_inputs.events {
            let result =
                run_elite_historic_procedure(app_state.clone(), token.hash(), element.event)
                    .await
                    .unwrap();
            results.push(result);
        }

        //// Get all results

        // 2 events were sent, but should have been merged into 1
        {
            let all_events_for_test_killer = get_kill_history_for_cmdr(
                "Test Killer2",
                1,
                10,
                crate::util::sized_pagination::KillFilter::All,
                &db,
            )
            .await
            .unwrap()
            .unwrap();
            assert_eq!(all_events_for_test_killer.len(), 1);
            let event = all_events_for_test_killer.first().unwrap();
            assert_eq!(event.system_name, Some(String::from("TestSystem")));
            assert_eq!(event.victim.ship_id, Some(String::from("python_nx")));
            assert_eq!(event.victim.squadron, Some(String::from("TEST")))
        }
        // 2 events were sent, but the second one should be ignored at it contains conflicting data
        {
            let all_events_for_test_killer = get_kill_history_for_cmdr(
                "Test Killer",
                1,
                10,
                crate::util::sized_pagination::KillFilter::All,
                &db,
            )
            .await
            .unwrap()
            .unwrap();
            assert_eq!(all_events_for_test_killer.len(), 1);
            let event = all_events_for_test_killer.first().unwrap();
            assert_eq!(event.system_name, Some(String::from("Test")));
            assert_eq!(event.victim.ship_id, Some(String::from("empire_trader")));
            assert_eq!(event.killer.squadron, Some(String::from("TEST")))
        }
    }
}
