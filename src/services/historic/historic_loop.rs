use std::time::Duration;

use chrono::Utc;
use serde::{Deserialize, Serialize};
use sqlx::{Pool, Postgres};
use tokio::time::sleep;
use tracing::{debug, error, info, warn};

use crate::{
    db::{
        tokens::get_discord_account_based_on_hashed_token, webhooks::get_and_delete_expired_jobs,
    },
    services::{historic::historic_elite::run_elite_historic_procedure, types::SseBroadcaster},
    util::weekly_summary::{get_next_weekly_emission_time, run_weekly_summary},
    AppState,
};

#[derive(Serialize, Deserialize)]
struct SseEnqueuedPayload {
    queue_position: usize,
}

#[derive(Serialize, Deserialize)]
struct SseFinishedPayload {
    created: u32,
    ignored: u32,
    updated: u32,
}

#[derive(Serialize, Deserialize)]
enum SsePayload {
    EnqueueRejected,
    Enqueued(SseEnqueuedPayload),
    Started,
    Finished(SseFinishedPayload),
}

#[derive(Serialize, Deserialize)]
struct SseEvent {
    discord_id: i64,
    job_id: String,
    payload: SsePayload,
}

pub(super) async fn historic_thread_loop(app_state: AppState) {
    loop {
        let next_weekly_summary_time = match app_state.emit_weekly_summary_not_before.read() {
            Ok(val) => Some(*val),
            Err(err) => {
                error!("Failed to acquire next weekly summary time: {}", err);
                None
            }
        };

        if let Some(time) = next_weekly_summary_time {
            if time < Utc::now() {
                let _ = run_weekly_summary(&app_state.db, Utc::now()).await;

                match app_state.emit_weekly_summary_not_before.try_write() {
                    Ok(mut w) => *w = get_next_weekly_emission_time(Utc::now()),
                    Err(err) => {
                        warn!("Failed to acquire Lock to Weekly Summary Time. Skipping, but nothing should have a lock on the time: {}", err)
                    }
                }
            }
        }

        let _ = run_webhook_patches(&app_state.db).await;

        let task = match app_state.historic_queue.lock() {
            Ok(mut val) => (*val).pop_back(),
            Err(err) => {
                error!(
                    "Historic Loop cannot lock mutex, because it is Poisoned! {}",
                    err
                );
                None
            }
        };
        match task {
            None => {
                debug!("Historic Queue does not contain elements. Waiting for 1m");
                sleep(Duration::from_secs(60)).await;
                continue;
            }
            Some(task) => {
                info!("Starting Historic Task for Job ID {}", task.identifier);
                match task.task {
                    crate::services::types::HistoricQueueTaskData::EliteHistoricQueueTask {
                        token_submitter,
                        payload,
                    } => {
                        let job_triggerer = get_discord_account_based_on_hashed_token(
                            &token_submitter,
                            &app_state.db,
                        )
                        .await;
                        let discord_id = match job_triggerer {
                            Ok(triggerer) => match triggerer {
                                Some(discord_id) => Some(discord_id),
                                None => {
                                    warn!("Missing Job Triggerer, which may indicate misuse of an Admin Server Token. No Discord DM can be sent in this case!");
                                    None
                                }
                            },
                            Err(err) => {
                                error!("Failed to get Job triggerer: {}", err);
                                None
                            }
                        };

                        if let Some(discord_id) = discord_id {
                            let job_started_message = SseEvent {
                                discord_id,
                                job_id: task.identifier.clone(),
                                payload: SsePayload::Started,
                            };
                            emit_sse_update(
                                &app_state.historic_updates_channel,
                                &job_started_message,
                            )
                        }

                        let task_result = run_elite_historic_procedure(
                            app_state.clone(),
                            token_submitter,
                            payload,
                        )
                        .await;
                        if let Some(discord_id) = discord_id {
                            match task_result {
                                Ok(data) => {
                                    info!(
                                        "Historic Queue {} succeeded handling {} events",
                                        task.identifier,
                                        data.len()
                                    );

                                    let mut created = 0;
                                    let mut ignored = 0;
                                    let mut updated = 0;

                                    for entry in data {
                                        match entry.insertion_state {
                                            crate::services::elite::types::InsertionState::Create |
                                            crate::services::elite::types::InsertionState::PostCreate(_) => created += 1,
                                            crate::services::elite::types::InsertionState::Update(_) => updated += 1,
                                            crate::services::elite::types::InsertionState::Ignore => ignored += 1,
                                        }
                                    }

                                    let payload = SseEvent {
                                        payload: SsePayload::Finished(SseFinishedPayload {
                                            created,
                                            ignored,
                                            updated,
                                        }),
                                        discord_id,
                                        job_id: task.identifier,
                                    };
                                    emit_sse_update(&app_state.historic_updates_channel, &payload)
                                }
                                Err(error) => {
                                    error!("Historic Queue {} failed: {}", task.identifier, error)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

pub(super) async fn emit_enqueued_dm(
    app_state: AppState,
    hashed_token: String,
    queue_position: Option<usize>,
    identifier: String,
) {
    let job_triggerer =
        get_discord_account_based_on_hashed_token(&hashed_token, &app_state.db).await;
    let discord_id = match job_triggerer {
        Ok(triggerer) => match triggerer {
            Some(discord_id) => discord_id,
            None => {
                warn!("Missing Job Triggerer, which may indicate misuse of an Admin Server Token. No Discord DM can be sent in this case!");
                return;
            }
        },
        Err(err) => {
            error!("Failed to get Job triggerer: {}", err);
            return;
        }
    };

    let payload = match queue_position {
        Some(queue_position) => SsePayload::Enqueued(SseEnqueuedPayload { queue_position }),
        None => SsePayload::EnqueueRejected,
    };

    let entire_message = SseEvent {
        discord_id,
        job_id: identifier,
        payload,
    };

    emit_sse_update(&app_state.historic_updates_channel, &entire_message)
}

fn emit_sse_update(broadcaster: &SseBroadcaster, payload: &SseEvent) {
    match serde_json::to_string(payload) {
        Ok(sse_payload) => {
            broadcaster.broadcast(&sse_payload);
            debug!("Emitted Kill Event to SSE Listener Channel")
        }
        Err(err) => {
            error!("Failed to TX Kill Events to SSE Channel: {}", err)
        }
    }
}

async fn patch_hook(hook_url_with_id: String, payload_raw: String) -> Result<(), String> {
    let client = reqwest::Client::new();
    let _ = client
        .patch(&hook_url_with_id)
        .header("Content-Type", "application/json")
        .body(payload_raw.clone())
        .send()
        .await;
    Ok(())
}

async fn run_webhook_patches(pool: &Pool<Postgres>) -> Result<(), sqlx::Error> {
    let mut tx = pool.begin().await?;
    // The correct way would be to retry here. But that also seems wrong. Dont want every minute to get spammed with faulty hooks.

    for (url, body) in get_and_delete_expired_jobs(&mut tx).await? {
        if let Err(err) = patch_hook(url, body).await {
            error!("Failed to patch message from webhook: {}", err);
        }
    }

    if let Err(err) = tx.commit().await {
        error!("Failed to commit transaction: {}", err);
        Err(err)
    } else {
        Ok(())
    }
}
