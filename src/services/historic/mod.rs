use crate::AppState;

pub(crate) mod historic_elite;
pub(crate) mod historic_loop;

pub(crate) async fn init_historic_handler(app_state: AppState) {
    historic_loop::historic_thread_loop(app_state).await
}
