use axum::{routing::get, Router};
use once_cell::sync::Lazy;
use rand::seq::SliceRandom;

use crate::{util::schema::get_all_schemas, AppState};

use self::{admin::create_admin_router, elite::create_elite_router, users::create_users_router};
use tower_http::trace::TraceLayer;
mod admin;
pub(crate) mod elite;
pub(crate) mod historic;
pub(crate) mod types;
mod users;

pub fn get_router(state: AppState) -> Router {
    Router::new()
        .route("/", get(index_route))
        .nest("/users", create_users_router(&state.clone()))
        .nest("/admin", create_admin_router(&state.clone()))
        .nest("/elite", create_elite_router(&state.clone()))
        .route("/schema", get(get_all_schemas))
        .layer(TraceLayer::new_for_http())
        .with_state(state)
}

#[cfg(debug_assertions)]
pub(crate) fn get_schemas() -> Vec<(String, Vec<schemars::schema::RootSchema>)> {
    let mut all_schemas = Vec::new();
    all_schemas.extend(admin::get_schemas());
    all_schemas.extend(elite::get_schemas());
    all_schemas.extend(users::get_schemas());

    all_schemas
}

pub(crate) static EASTER_EGG_PAYLOADS: Lazy<Vec<String>> = Lazy::new(|| {
    vec![
        include_str!("../easter_eggs/bun.txt").to_string(),
        include_str!("../easter_eggs/burger.txt").to_string(),
        include_str!("../easter_eggs/ggi.txt").to_string(),
        include_str!("../easter_eggs/monkey.txt").to_string(),
        include_str!("../easter_eggs/stare.txt").to_string(),
        include_str!("../easter_eggs/weaponized_mutton.txt").to_string(),
        include_str!("../easter_eggs/trashcan.txt").to_string(),
    ]
});

async fn index_route() -> String {
    EASTER_EGG_PAYLOADS
        .choose(&mut rand::thread_rng())
        .unwrap()
        .clone()
}
