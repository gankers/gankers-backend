use once_cell::sync::Lazy;

pub(crate) struct Config {
    pub(crate) port: u32,
    pub(super) db_connect_string: String,
}

impl Config {
    fn init() -> Self {
        Config {
            port: parse_port(),
            db_connect_string: parse_db(),
        }
    }
}

pub(crate) static CONFIG: Lazy<Config> = Lazy::new(Config::init);

fn parse_port() -> u32 {
    match std::env::var_os("PORT") {
        Some(val) => (val.to_str().unwrap()).parse::<u32>().unwrap(),
        None => 8080u32,
    }
}

fn parse_db() -> String {
    match std::env::var_os("DATABASE_URL") {
        Some(val) => val.into_string().unwrap(),
        None => panic!("Must provide DATABASE_URL as env var or in .env file!"),
    }
}
