mod discord_webhook;
pub(crate) mod elite_kill_webhook;
pub(crate) mod generic_error;
pub(crate) mod powerplay;
pub(crate) mod rank_names;
pub(crate) mod schema;
pub(crate) mod ship_names;
pub(crate) mod sized_pagination;
pub(crate) mod weekly_summary;
