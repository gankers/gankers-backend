use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub(crate) struct GenericMessage {
    pub(crate) message: String,
}

impl GenericMessage {
    pub fn new(message: String) -> GenericMessage {
        GenericMessage { message }
    }
}
