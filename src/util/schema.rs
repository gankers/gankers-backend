use std::collections::HashMap;

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use schemars::schema::RootSchema;

#[cfg(debug_assertions)]
pub(crate) async fn get_all_schemas() -> Response {
    use crate::services::get_schemas;
    let schemas = get_schemas();

    let schemas_as_map: HashMap<String, Vec<RootSchema>> = schemas.into_iter().collect();

    (StatusCode::OK, Json::from(schemas_as_map)).into_response()
}

#[cfg(not(debug_assertions))]
pub(crate) async fn get_all_schemas() -> Response {
    use super::generic_error::GenericMessage;

    (
        StatusCode::IM_A_TEAPOT,
        Json::from(GenericMessage::new(
            "This endpoint is only active in Dev-Mode.".to_string(),
        )),
    )
        .into_response()
}
