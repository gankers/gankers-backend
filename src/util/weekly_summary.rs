use std::{cmp::Ordering, collections::HashMap};

use chrono::{DateTime, Datelike, Duration, Timelike, Utc};
use itertools::enumerate;
use rand::seq::SliceRandom;
use sqlx::PgPool;
use tracing::error;

use crate::db::{
    elite_leaderboard::{get_killboard_entries_for_timespan, KillboardEntry},
    webhooks::get_all_elite_weekly_webhooks,
};

use super::discord_webhook::{
    push_hook_and_return_id, DiscordEmbedAuthor, DiscordEmbedEntry, DiscordEmbedField,
    DiscordEmbedFooter, DiscordWebhookPayload,
};

pub(crate) fn get_next_weekly_emission_time(now: DateTime<Utc>) -> DateTime<Utc> {
    let days_until_next_thursday = match now.weekday() {
        chrono::Weekday::Thu => match now.hour() >= 8 {
            true => 7,
            false => 0,
        },
        other => {
            let mut diff = chrono::Weekday::Thu.num_days_from_monday() as i64
                - other.num_days_from_monday() as i64;

            if diff < 0 {
                diff += 7;
            }

            diff
        }
    };

    (now + Duration::days(days_until_next_thursday))
        .with_hour(8)
        .unwrap()
        .with_minute(0)
        .unwrap()
        .with_second(0)
        .unwrap()
}

pub(crate) fn get_current_weekly_period_from_timestamp(
    time: DateTime<Utc>,
) -> (DateTime<Utc>, DateTime<Utc>) {
    // Get the "next" thursday
    let upper = get_next_weekly_emission_time(time);
    let lower = upper - Duration::days(7);

    let delta = time - lower;

    if delta <= Duration::minutes(5) {
        return (lower - Duration::days(7), upper - Duration::days(7));
    }

    (lower, upper)
}

pub(crate) async fn run_weekly_summary(
    pool: &PgPool,
    now: DateTime<Utc>,
) -> Result<(), sqlx::Error> {
    let (from, to) = get_current_weekly_period_from_timestamp(now);
    let hooks = get_all_elite_weekly_webhooks(pool).await?;

    let all_relevant_kills = get_killboard_entries_for_timespan(pool, from, to).await?;
    // This should be cleaned up at some point
    let summary = build_summary_payload(&all_relevant_kills, from, to);

    for hook in hooks {
        if hook.push_elite_weekly {
            // Push Weekly Info to all hooks that have that flag set
            let _ = push_hook_and_return_id(&hook.webhook_url, &summary).await;
        }
        if hook.push_elite_weekly_deep_dive {
            // TODO: Push Deep Dive Info
        }
    }

    Ok(())
}

/// For each week, a random entry from here is chosen.
const WEEKLY_SUMMARY_TEXTS: &[&str] = &[
    "Another week, another weekly leaderboard summary.",
    "Oh look. Another week of ganks.",
    "To gank, or not to gank… is a stupid fucking question. Of course you gank.",
    "Your ad here!",
    "Haha ship go boom!",
    "Loads of rebuys have been blessed.",
    "Down here, salt is a way of life. Obviously the environment down here is all salt. The ceiling is salt, the floor is salt, the walls are salt, and to an extent the air is salt. And you breathe that in, and you constantly taste the salt",
    "Funny number must go up."
];

fn build_summary_payload(
    all_kills: &[KillboardEntry],
    from: DateTime<Utc>,
    to: DateTime<Utc>,
) -> DiscordWebhookPayload {
    // First count kills
    let mut counter: HashMap<&str, i32> = HashMap::new();
    for kill in all_kills {
        *counter.entry(&kill.killer.name).or_insert(0) += 1;
    }
    let mut killer_and_count: Vec<(&str, i32)> = counter.into_iter().collect();
    // Basically sort by Kills DESC, followed by Names ASC
    killer_and_count.sort_by(|a, b| {
        let kill_count_ord = b.1.cmp(&a.1);
        match kill_count_ord {
            Ordering::Equal => a.0.cmp(b.0),
            _ => kill_count_ord,
        }
    });
    // We only care about the top 20 entries
    killer_and_count.truncate(20);

    if killer_and_count.is_empty() {
        return DiscordWebhookPayload {
            embeds: vec![DiscordEmbedEntry {
                title: "Weekly Leaderboard Summary".to_string(),
                description: ":skull: No kills?".to_string(),
                color: 0xff00ff,
                footer: DiscordEmbedFooter {
                    text: format!(
                        "Summary for {} until {} 8AM UTC",
                        from.format("%Y-%m-%d"),
                        to.format("%Y-%m-%d")
                    ),
                    icon_url: Some(String::from("https://gankers.org/og.png")),
                },
                author: DiscordEmbedAuthor {
                    name: "".to_string(),
                },
                fields: vec![],
            }],
        };
    }

    let mut left_row = Vec::with_capacity(killer_and_count.len());
    let mut center_row = Vec::with_capacity(killer_and_count.len());
    let mut right_row = Vec::with_capacity(killer_and_count.len());

    // if multiple CMDRs have the same amount, they should have the same Position in the board.
    let mut last_kill_count = 0;
    let mut last_position = 0;
    for (idx, entry) in enumerate(killer_and_count) {
        let position_to_display = {
            if last_kill_count == entry.1 {
                last_position
            } else {
                last_kill_count = entry.1;
                last_position = (idx as i32) + 1;
                last_position
            }
        };
        let escaped_cmdr = entry.0.replace('\\', "\\\\");

        left_row.push(format!("{}", position_to_display));
        center_row.push(escaped_cmdr);
        right_row.push(format!(
            "[{}](https://gankers.org/elite/cmdrs/{})",
            entry.1,
            urlencoding::encode(entry.0)
        ));
    }

    let mut right_row = right_row.join("\n");
    if right_row.len() > 1000 {
        right_row.replace_range(1000.., "\n...");
    }
    let mut center_row = center_row.join("\n");
    if center_row.len() > 1000 {
        center_row.replace_range(1000.., "\n...");
    }

    let mut rng = rand::thread_rng();
    let description = match WEEKLY_SUMMARY_TEXTS.choose(&mut rng) {
        Some(val) => String::from(*val),
        None => {
            error!("Failed to choose a random string. Should never happen as always some values are present");
            String::from("")
        }
    };
    DiscordWebhookPayload {
        embeds: vec![DiscordEmbedEntry {
            title: format!("Weekly Leaderboard Summary :dagger: × {}", all_kills.len()),
            description,
            color: 0xff00ff,
            footer: DiscordEmbedFooter {
                text: format!(
                    "Summary for {} until {} 8AM UTC",
                    from.format("%Y-%m-%d"),
                    to.format("%Y-%m-%d")
                ),
                icon_url: Some(String::from("https://gankers.org/og.png")),
            },
            author: DiscordEmbedAuthor {
                name: "".to_string(),
            },
            fields: vec![
                DiscordEmbedField {
                    name: ":trophy:".to_string(),
                    value: left_row.join("\n"),
                    inline: true,
                },
                DiscordEmbedField {
                    name: ":busts_in_silhouette:".to_string(),
                    value: center_row,
                    inline: true,
                },
                DiscordEmbedField {
                    name: "×:dagger:".to_string(),
                    value: right_row,
                    inline: true,
                },
            ],
        }],
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use itertools::Itertools;
    use pretty_assertions::assert_eq;
    use serde::{Deserialize, Serialize};

    #[test]
    fn assert_next_thursday_is_selected() {
        let inputs = vec![
            "2024-08-29T14:30:00Z",
            "2024-08-29T07:30:00Z",
            "2024-08-28T14:30:00Z",
            "2024-08-29T08:00:00Z",
            "2024-08-29T08:01:00Z",
        ];

        let input_times = inputs
            .into_iter()
            .map(|x| DateTime::parse_from_rfc3339(x).unwrap().with_timezone(&Utc))
            .collect_vec();

        let expected = vec![
            "2024-09-05T08:00:00Z",
            "2024-08-29T08:00:00Z",
            "2024-08-29T08:00:00Z",
            "2024-09-05T08:00:00Z",
            "2024-09-05T08:00:00Z",
        ];
        let extected_times = expected
            .into_iter()
            .map(|x| DateTime::parse_from_rfc3339(x).unwrap().with_timezone(&Utc))
            .collect_vec();

        let actual = input_times
            .into_iter()
            .map(get_next_weekly_emission_time)
            .collect_vec();

        assert_eq!(
            serde_yaml::to_string(&extected_times).unwrap(),
            serde_yaml::to_string(&actual).unwrap()
        )
    }

    #[test]
    fn test_get_current_weekly_period_from_timestamp() {
        #[derive(Serialize, Deserialize)]
        struct TestInputEntry {
            input: DateTime<Utc>,
            lower: DateTime<Utc>,
            upper: DateTime<Utc>,
        }

        impl TestInputEntry {
            fn new(input: &str, lower: &str, upper: &str) -> Self {
                Self {
                    input: DateTime::parse_from_rfc3339(input)
                        .unwrap()
                        .with_timezone(&Utc),
                    lower: DateTime::parse_from_rfc3339(lower)
                        .unwrap()
                        .with_timezone(&Utc),
                    upper: DateTime::parse_from_rfc3339(upper)
                        .unwrap()
                        .with_timezone(&Utc),
                }
            }
        }

        let test_cases = vec![
            // After 8AM+5m, should be 29->05
            TestInputEntry::new(
                "2024-08-29T14:30:00Z",
                "2024-08-29T08:00:00Z",
                "2024-09-05T08:00:00Z",
            ),
            TestInputEntry::new(
                "2024-08-29T08:10:00Z",
                "2024-08-29T08:00:00Z",
                "2024-09-05T08:00:00Z",
            ),
            TestInputEntry::new(
                "2024-08-29T08:00:00Z",
                "2024-08-22T08:00:00Z",
                "2024-08-29T08:00:00Z",
            ),
            TestInputEntry::new(
                "2024-08-29T07:59:00Z",
                "2024-08-22T08:00:00Z",
                "2024-08-29T08:00:00Z",
            ),
            TestInputEntry::new(
                "2024-08-29T08:04:00Z",
                "2024-08-22T08:00:00Z",
                "2024-08-29T08:00:00Z",
            ),
        ];

        let actual = test_cases
            .iter()
            .map(|x| {
                let result = get_current_weekly_period_from_timestamp(x.input);
                TestInputEntry {
                    input: x.input,
                    lower: result.0,
                    upper: result.1,
                }
            })
            .collect_vec();

        assert_eq!(
            serde_yaml::to_string(&test_cases).unwrap(),
            serde_yaml::to_string(&actual).unwrap()
        )
    }
}
