use chrono::{DateTime, Utc};
use sqlx::Postgres;
use tracing::error;

use crate::{
    db::webhooks::enqueue_webhook_patch_job,
    services::elite::KillEvent,
    util::{
        discord_webhook::{
            DiscordEmbedAuthor, DiscordEmbedEntry, DiscordEmbedField, DiscordEmbedFooter,
        },
        ship_names::get_pretty_names_for_ship_ids,
    },
};

use super::discord_webhook::{push_hook_and_return_id, DiscordWebhookPayload};

fn build_webhook_payload(kill_event: &KillEvent) -> DiscordWebhookPayload {
    fn escape_and_wrap_cmdr_in_link(cmdr: &str) -> String {
        let escaped_cmdr = cmdr.replace('\\', "\\\\");

        format!(
            "[{}](https://gankers.org/elite/cmdrs/{})",
            escaped_cmdr,
            urlencoding::encode(cmdr)
        )
    }

    let mut killer_rows = Vec::with_capacity(2);
    let mut victim_rows = Vec::with_capacity(2);
    killer_rows.push(match &kill_event.killer_squadron {
        Some(squad) => format!(
            "**[{}]** {}",
            squad,
            escape_and_wrap_cmdr_in_link(&kill_event.killer),
        ),
        None => escape_and_wrap_cmdr_in_link(&kill_event.killer),
    });
    victim_rows.push(match &kill_event.victim_squadron {
        Some(squad) => format!(
            "**[{}]** {}",
            squad,
            escape_and_wrap_cmdr_in_link(&kill_event.victim),
        ),
        None => escape_and_wrap_cmdr_in_link(&kill_event.victim),
    });

    if let Some(ship) = &kill_event.killer_ship {
        killer_rows.push(get_pretty_names_for_ship_ids(ship, false).unwrap_or(ship.clone()))
    }
    if let Some(ship) = &kill_event.victim_ship {
        victim_rows.push(get_pretty_names_for_ship_ids(ship, false).unwrap_or(ship.clone()))
    }

    if let Some(power) = &kill_event.killer_power {
        killer_rows.push(format!("‹{power}›"));
    }
    if let Some(power) = &kill_event.victim_power {
        victim_rows.push(format!("‹{power}›"));
    }

    let system_row = match &kill_event.system {
        Some(val) => {
            format!(
                "[{}](https://gankers.org/elite/systems/{})",
                val,
                urlencoding::encode(val)
            )
        }
        None => "-".to_string(),
    };

    let date_string = kill_event
        .kill_timestamp
        .format("%Y-%m-%d %H:%M")
        .to_string();

    DiscordWebhookPayload {
        embeds: vec![DiscordEmbedEntry {
            title: String::new(),
            description: String::new(),
            color: 16711680,
            footer: DiscordEmbedFooter {
                text: date_string,
                icon_url: Some(String::from("https://gankers.org/og.png")),
            },
            author: DiscordEmbedAuthor {
                name: String::new(),
            },
            fields: vec![
                DiscordEmbedField::new(String::from("🗡️ Attacker 🗡️"), killer_rows.join("\n")),
                DiscordEmbedField::new(String::from("💀 Victim 💀"), victim_rows.join("\n")),
                DiscordEmbedField::new(String::from("🪐 System 🪐"), system_row),
            ],
        }],
    }
}

pub(crate) async fn send_killboard_webhook(
    webhooks: Vec<String>,
    killevent: &KillEvent,
    correction_killevent: Option<&KillEvent>,
    correction_after: DateTime<Utc>,
    pool: &sqlx::Pool<Postgres>,
) {
    let payload = build_webhook_payload(killevent);
    let correction_payload = correction_killevent.map(build_webhook_payload);
    let mut error_count = 0;
    for webhook in &webhooks {
        match push_hook_and_return_id(webhook, &payload).await {
            Ok(id) => {
                let patch_webhook = format!("{}/messages/{}", webhook, id);
                if let Some(correction_payload) = &correction_payload {
                    let payload_raw = match serde_json::to_string(correction_payload) {
                        Ok(data) => data,
                        Err(err) => {
                            error!("Failed to build Webhook Payload. Err: {}", err);
                            error_count += 1;
                            continue;
                        }
                    };

                    if let Err(err) = enqueue_webhook_patch_job(
                        pool,
                        patch_webhook,
                        payload_raw,
                        correction_after,
                    )
                    .await
                    {
                        error!("Failed to enqueue correction Payload. Err: {}", err);
                        error_count += 1;
                        continue;
                    }
                }
            }
            Err(_) => {
                error_count += 1;
            }
        }
    }
    if error_count > 0 {
        error!("{} Webhooks failed for Killboard Send", error_count);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::DateTime;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_build_payload() {
        let datetime = DateTime::parse_from_rfc3339("2024-07-15T18:00:00Z")
            .unwrap()
            .into();
        let event = KillEvent {
            kill_id: None,
            system: Some("TEST_SYSTEM".to_string()),
            killer: "TEST_KILLER".to_string(),
            killer_rank: 1,
            victim: "TEST_VICTIM".to_string(),
            victim_rank: 1,
            killer_ship: Some("python_nx".to_string()),
            victim_ship: Some("python_nx".to_string()),
            killer_squadron: None,
            victim_squadron: Some(String::from("TEST")),
            kill_timestamp: datetime,
            killer_power: None,
            victim_power: None,
        };

        let result = build_webhook_payload(&event);

        let expected = DiscordWebhookPayload {
            embeds: vec![DiscordEmbedEntry {
                title: String::new(),
                description: String::new(),
                color: 16711680,
                footer: DiscordEmbedFooter {
                    text: String::from("2024-07-15 18:00"),
                    icon_url: Some(String::from("https://gankers.org/og.png")),
                },
                author: DiscordEmbedAuthor {
                    name: String::new(),
                },
                fields: vec![
                    DiscordEmbedField::new(
                        String::from("🗡️ Attacker 🗡️"),
                        String::from("[TEST_KILLER](https://gankers.org/elite/cmdrs/TEST_KILLER)\nPython MkII"),
                    ),
                    DiscordEmbedField::new(
                        String::from("💀 Victim 💀"),
                        String::from(
                            "**[TEST]** [TEST_VICTIM](https://gankers.org/elite/cmdrs/TEST_VICTIM)\nPython MkII",
                        ),
                    ),
                    DiscordEmbedField::new(
                        String::from("🪐 System 🪐"),
                        String::from(
                            "[TEST_SYSTEM](https://gankers.org/elite/systems/TEST_SYSTEM)",
                        ),
                    ),
                ],
            }],
        };

        assert_eq!(
            serde_yaml::to_string(&expected).unwrap(),
            serde_yaml::to_string(&result).unwrap()
        )
    }
}
