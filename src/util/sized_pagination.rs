use serde::{Deserialize, Serialize};

///
/// Struct to have a unified Struct for passing
/// Pagination Info
///
#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct SizedPaginationQuery {
    #[serde(default = "default_page")]
    pub(crate) page: u16,

    #[serde(default = "default_page_size")]
    pub(crate) page_size: u8,
}

fn default_page() -> u16 {
    1
}

fn default_page_size() -> u8 {
    50
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) enum KillFilter {
    All,
    Kills,
    Deaths,
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct SizedPaginationQueryWithKillFilter {
    #[serde(default = "default_page")]
    pub(crate) page: u16,

    #[serde(default = "default_page_size")]
    pub(crate) page_size: u8,

    #[serde(default = "default_filter")]
    pub(crate) filter: KillFilter,
}

fn default_filter() -> KillFilter {
    KillFilter::default()
}

impl Default for KillFilter {
    fn default() -> Self {
        return KillFilter::All;
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct PowerplayAllowUnpledged {
    #[serde(default = "default_with_unpledged")]
    pub(crate) with_unpledged: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct PowerplaySizedPaginationWithAllowUnpledged {
    #[serde(default = "default_with_unpledged")]
    pub(crate) with_unpledged: bool,
    #[serde(default = "default_page")]
    pub(crate) page: u16,

    #[serde(default = "default_page_size")]
    pub(crate) page_size: u8,
}

fn default_with_unpledged() -> bool {
    return false;
}
