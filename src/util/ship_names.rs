use itertools::Itertools;
use once_cell::sync::Lazy;

static ON_FOOT_IDS: Lazy<Vec<String>> = Lazy::new(|| {
    vec![
        "utilitysuit_class1",
        "utilitysuit_class2",
        "utilitysuit_class3",
        "utilitysuit_class4",
        "utilitysuit_class5",
        "tacticalsuit_class1",
        "tacticalsuit_class2",
        "tacticalsuit_class3",
        "tacticalsuit_class4",
        "tacticalsuit_class5",
        "explorationsuit_class1",
        "explorationsuit_class2",
        "explorationsuit_class3",
        "explorationsuit_class4",
        "explorationsuit_class5",
        "on_foot", // added by us
    ]
    .into_iter()
    .map(|x| x.to_string())
    .collect_vec()
});

pub(crate) fn is_on_foot(ship_id: &str) -> bool {
    ON_FOOT_IDS.contains(&ship_id.to_string())
}

pub(crate) fn get_pretty_names_for_ship_ids(ship_id: &str, reduce_onfoot: bool) -> Option<String> {
    if reduce_onfoot && is_on_foot(ship_id) {
        return Some("On-foot".to_string());
    }

    let ship_name = match ship_id.to_ascii_lowercase().as_str() {
        "sidewinder" => "Sidewinder",
        "eagle" => "Eagle",
        "hauler" => "Hauler",
        "adder" => "Adder",
        "viper" => "Viper MkIII",
        "cobramkiii" => "Cobra MkIII",
        "cobramkv" => "Cobra MkV",
        "type6" => "Type-6 Transporter",
        "dolphin" => "Dolphin",
        "type7" => "Type-7 Transporter",
        "type8" => "Type-8 Transporter",
        "asp" => "Asp Explorer",
        "vulture" => "Vulture",
        "empire_trader" => "Imperial Clipper",
        "federation_dropship" => "Federal Dropship",
        "orca" => "Orca",
        "type9" => "Type-9 Heavy",
        "python" => "Python",
        "belugaliner" => "Beluga Liner",
        "ferdelance" => "Fer-de-Lance",
        "anaconda" => "Anaconda",
        "federation_corvette" => "Federal Corvette",
        "cutter" => "Imperial Cutter",
        "diamondback" => "Diamondback Scout",
        "empire_courier" => "Imperial Courier",
        "diamondbackxl" => "Diamondback Explorer",
        "empire_eagle" => "Imperial Eagle",
        "federation_dropship_mkii" => "Federal Assault Ship",
        "federation_gunship" => "Federal Gunship",
        "viper_mkiv" => "Viper MkIV",
        "cobramkiv" => "Cobra MkIV",
        "independant_trader" => "Keelback",
        "asp_scout" => "Asp Scout",
        "type9_military" => "Type-10 Defender",
        "krait_mkii" => "Krait MkII",
        "typex" => "Alliance Chieftain",
        "typex_2" => "Alliance Crewsader",
        "typex_3" => "Alliance Challenger",
        "krait_light" => "Krait Phantom",
        "mandalay" => "Mandalay",
        "mamba" => "Mamba",
        "mamba_light" => "Graybox Gamer",
        "panther_lx" => "Graybox Gamer",
        "python_nx" => "Python MkII",
        // On-foot Suits (why FDEV, why are these per Grade?)
        "utilitysuit_class1" => "On-foot (Maverick G1)",
        "utilitysuit_class2" => "On-foot (Maverick G2)",
        "utilitysuit_class3" => "On-foot (Maverick G3)",
        "utilitysuit_class4" => "On-foot (Maverick G4)",
        "utilitysuit_class5" => "On-foot (Maverick G5)",
        "tacticalsuit_class1" => "On-foot (Dominator G1)",
        "tacticalsuit_class2" => "On-foot (Dominator G2)",
        "tacticalsuit_class3" => "On-foot (Dominator G3)",
        "tacticalsuit_class4" => "On-foot (Dominator G4)",
        "tacticalsuit_class5" => "On-foot (Dominator G5)",
        "explorationsuit_class1" => "On-foot (Artemis G1)",
        "explorationsuit_class2" => "On-foot (Artemis G2)",
        "explorationsuit_class3" => "On-foot (Artemis G3)",
        "explorationsuit_class4" => "On-foot (Artemis G4)",
        "explorationsuit_class5" => "On-foot (Artemis G5)",
        // SRVs (Drive Assist On, … fuck it, Drive Assist Off)
        "testbuggy" => "SRV Scarab",
        "combat_srv_multicrew_01" => "SRV Scorpion",
        _ => return None,
    };

    Some(ship_name.to_string())
}

pub(crate) fn get_colours_for_ship_ids(ship_id: &str) -> String {
    if is_on_foot(ship_id) {
        return "#555555".to_string();
    }

    let colour = match ship_id.to_ascii_lowercase().as_str() {
        "sidewinder" => "8f00ff",
        "eagle" => "7000ff",
        "hauler" => "5000fe",
        "adder" => "3100fe",
        "viper" => "1200fe",
        "cobramkiii" => "000efd",
        "type6" => "002dfd",
        "dolphin" => "004cfd",
        "type7" => "006bfc",
        "type8" => "008afc",
        "asp" => "01a8fc",
        "vulture" => "01c7fb",
        "empire_trader" => "01e6fb",
        "federation_dropship" => "01fbf1",
        "orca" => "01fad2",
        "type9" => "01fab3",
        "python" => "01fa95",
        "belugaliner" => "01f976",
        "ferdelance" => "01f957",
        "anaconda" => "01f939",
        "federation_corvette" => "01f81a",
        "cutter" => "06f801",
        "diamondback" => "25f801",
        "empire_courier" => "43f701",
        "diamondbackxl" => "61f701",
        "empire_eagle" => "7ff701",
        "federation_dropship_mkii" => "9df601",
        "federation_gunship" => "bbf601",
        "viper_mkiv" => "d9f601",
        "cobramkiv" => "f5f402",
        "cobramkv" => "9ff402",
        "independant_trader" => "f5d602",
        "asp_scout" => "f5b802",
        "type9_military" => "f49a02",
        "krait_mkii" => "f47c02",
        "typex" => "f45e02",
        "typex_2" => "f34002",
        "typex_3" => "f32302",
        "krait_light" => "f30502",
        "mandalay" => "39c900",
        "mamba" => "f2021c",
        "mamba_light" => "Graybox Gamer",
        "panther_lx" => "Graybox Gamer",
        "python_nx" => "f2023a",
        "testbuggy" => "009300",
        "combat_srv_multicrew_01" => "007393",
        _ => "000000",
    };

    "#".to_owned() + colour
}
