use itertools::Itertools;
use once_cell::sync::Lazy;

static RANK_NAMES: Lazy<Vec<String>> = Lazy::new(|| {
    vec![
        "Harmless",
        "Mostly Harmless",
        "Novice",
        "Competent",
        "Expert",
        "Master",
        "Dangerous",
        "Deadly",
        "Elite",
    ]
    .into_iter()
    .map(|x| x.to_string())
    .collect_vec()
});

static RANK_COLORS: Lazy<Vec<String>> = Lazy::new(|| {
    vec![
        "#ddd1ff", "#b9c9ff", "#a0e6ff", "#88ffe3", "#70fe95", "#7ffe58", "#cbfe3f", "#fd4f0f",
        "#f2023a",
    ]
    .into_iter()
    .map(|x| x.to_string())
    .collect_vec()
});

pub(crate) fn get_rank_name_and_colour(rank: u8) -> (String, String) {
    let rank = match rank >= RANK_NAMES.len() as u8 {
        true => RANK_NAMES.len() as u8 - 1,
        false => rank,
    };

    (
        RANK_NAMES[rank as usize].clone(),
        RANK_COLORS[rank as usize].clone(),
    )
}
