pub(crate) fn is_string_powerplay_name(input: &str) -> bool {
    let valid_options = vec![
        "A. Lavigny-Duval",
        "Aisling Duval",
        "Archon Delaine",
        "Denton Patreus",
        "Edmund Mahon",
        "Felicia Winters",
        "Jerome Archer",
        "Li Yong-Rui",
        "Nakato Kaine",
        "Pranav Antal",
        "Yuri Grom",
        "Zemina Torval",
        // Legacy:
        "Zachary Hudson",
    ];

    return valid_options.contains(&input);
}
