use serde::{Deserialize, Serialize};
use tracing::error;

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct DiscordWebhookPayload {
    pub(super) embeds: Vec<DiscordEmbedEntry>,
}

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct DiscordEmbedEntry {
    pub(super) title: String,
    pub(super) description: String,
    pub(super) color: usize,
    pub(super) footer: DiscordEmbedFooter,
    pub(super) author: DiscordEmbedAuthor,
    pub(super) fields: Vec<DiscordEmbedField>,
}

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct DiscordEmbedFooter {
    pub(super) text: String,
    pub(super) icon_url: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct DiscordEmbedAuthor {
    pub(super) name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub(super) struct DiscordEmbedField {
    pub(super) name: String,
    pub(super) value: String,
    pub(super) inline: bool,
}

impl DiscordEmbedField {
    pub(super) fn new(header: String, content: String) -> Self {
        Self {
            name: header,
            value: content,
            inline: true,
        }
    }
}

pub(crate) async fn push_hook_and_return_id(
    hook_url: &str,
    payload: &DiscordWebhookPayload,
) -> Result<String, String> {
    let payload_raw = match serde_json::to_string(payload) {
        Ok(data) => data,
        Err(err) => {
            error!("Failed to build Webhook Payload. Err: {}", err);
            return Err(err.to_string());
        }
    };

    let client = reqwest::Client::new();
    let request = client
        .post(hook_url)
        .query(&[("wait", "true")])
        .header("Content-Type", "application/json")
        .body(payload_raw.clone())
        .send()
        .await;

    match request {
        Err(err) => {
            error!("A Webhook has failed: {}", &err);
            Err(err.to_string())
        }
        Ok(response) => match response.json::<PostWebhookResponse>().await {
            Ok(val) => Ok(val.id),
            Err(err) => {
                error!("Emitted Webhook, but failed to parse JSON response. As such, no correction job was enqueued. Reason: {}", err);
                Err(err.to_string())
            }
        },
    }
}

#[derive(Serialize, Deserialize)]
struct PostWebhookResponse {
    id: String,
}
