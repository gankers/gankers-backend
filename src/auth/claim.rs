use axum::http::request::Parts;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::{async_trait, extract::FromRequestParts};
use axum::{Json, RequestPartsExt};

use axum_extra::headers::authorization::Bearer;
use axum_extra::headers::Authorization;
use axum_extra::TypedHeader;
use serde::Serialize;
use serde_json::json;
use sha256::digest;

use crate::db::tokens::TokenData;
use crate::{db::tokens::get_token_data, AppState};

pub(crate) struct RawToken {
    pub(crate) token: String,
}

impl RawToken {
    pub(crate) fn hash(&self) -> String {
        digest(&self.token)
    }
}

#[async_trait]
impl FromRequestParts<AppState> for Option<RawToken> {
    type Rejection = AuthError;

    async fn from_request_parts(
        parts: &mut Parts,
        _state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let result = match parts.extract::<TypedHeader<Authorization<Bearer>>>().await {
            Ok(val) => {
                let wrapper = val.0;
                let bearer = wrapper.0;
                let token = bearer.token();
                Ok(Some(String::from(token)))
            }
            Err(err) => match err.reason() {
                axum_extra::typed_header::TypedHeaderRejectionReason::Missing => Ok(None),
                _ => Err(AuthError::InvalidToken),
            },
        }?
        .map(|x| RawToken { token: x });

        Ok(result)
    }
}

#[async_trait]
impl FromRequestParts<AppState> for Option<TokenData> {
    type Rejection = AuthError;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let result = match parts.extract::<TypedHeader<Authorization<Bearer>>>().await {
            Ok(val) => {
                let wrapper = val.0;
                let bearer = wrapper.0;
                let token = bearer.token();
                Ok(Some(String::from(token)))
            }
            Err(err) => match err.reason() {
                axum_extra::typed_header::TypedHeaderRejectionReason::Missing => Ok(None),
                _ => Err(AuthError::InvalidToken),
            },
        }?;

        match result {
            None => Ok(None),
            Some(token) => match get_token_data(&token, &state.db).await {
                Ok(val) => match val {
                    Some(val) => Ok(Some(val)),
                    None => Err(AuthError::WrongCredentials),
                },
                Err(_) => Err(AuthError::DbError),
            },
        }
    }
}

impl IntoResponse for AuthError {
    fn into_response(self) -> axum::response::Response {
        let (status, error_message) = match self {
            AuthError::WrongCredentials => (StatusCode::UNAUTHORIZED, "Wrong credentials"),
            AuthError::MissingCredentials => (StatusCode::BAD_REQUEST, "Missing Bearer Token"),
            AuthError::InvalidToken => (StatusCode::BAD_REQUEST, "Invalid token"),
            AuthError::Forbidden => (
                StatusCode::FORBIDDEN,
                "Token valid, but does not have required Permission",
            ),
            AuthError::DbError => (StatusCode::INTERNAL_SERVER_ERROR, "DB Issues"),
            AuthError::RevokedToken => (StatusCode::FORBIDDEN, "This token has been revoked"),
        };
        let body = Json(json!({
            "error": error_message,
        }));
        (status, body).into_response()
    }
}

#[derive(Debug, Serialize, Copy, Clone)]
pub(crate) enum AuthError {
    WrongCredentials,
    MissingCredentials,
    InvalidToken,
    Forbidden,
    DbError,
    RevokedToken,
}
