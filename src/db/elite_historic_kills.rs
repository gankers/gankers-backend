use chrono::Utc;
use itertools::Itertools;
use sqlx::{Error, PgPool, Postgres, Transaction};

use crate::{
    db::elite_leaderboard::KillboardEntryDb,
    services::elite::{
        types::{InsertionState, InteractionKey, KillEventWithDiff},
        KillEvent,
    },
};

pub(crate) async fn get_all_entries_containing_interaction_indices_in_timeframe(
    interaction_indices: &[String],
    lower_bound: chrono::DateTime<Utc>,
    upper_bound: chrono::DateTime<Utc>,
    pool: &PgPool,
) -> Result<Vec<KillEvent>, Error> {
    let result: Vec<KillEvent> = sqlx::query_as!(
        KillboardEntryDb,
        r#"
        SELECT system_name, killer, killer_ship, victim, victim_ship, kill_timestamp, kill_id, victim_rank, killer_rank, victim_squadron, killer_squadron, interaction_index, victim_power, killer_power
        FROM elite_kills_patched
        WHERE kill_timestamp < $1 
            AND kill_timestamp > $2
            AND interaction_index = ANY($3)"#,
        upper_bound,
        lower_bound,
        &interaction_indices
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .filter_map(|x| x.into()).collect_vec();
    Ok(result)
}

struct CreateInsertResponse {
    kill_id: i32,
}

async fn insert_all_kills_to_create_with_tx(
    diff_actions: &mut Vec<KillEventWithDiff>,
    caller_hashed_token: &str,
    tx: &mut Transaction<'_, Postgres>,
) -> Result<(), Error> {
    for entry in diff_actions {
        match entry.insertion_state {
            InsertionState::Create => {}
            _ => continue,
        }

        // This might be a bit ugly, but should be fine for now.
        // Later this could be replaced by a batch insert.
        let interaction = InteractionKey::from(&entry.event).0;

        let inserted_entry_result: Result<CreateInsertResponse, Error> = sqlx::query_as!(
            CreateInsertResponse,
            r#"
            INSERT INTO "elite_kills"
            (interaction_index, killer, killer_squadron, killer_ship, killer_rank, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, hashed_token_creator, system_name)
            VALUES
            ($1               , $2    ,              $3,          $4,          $5,     $6,              $7,          $8,          $9,            $10,                  $11,         $12)
            RETURNING (kill_id)
            "#,
            interaction,
            entry.event.killer,
            entry.event.killer_squadron,
            entry.event.killer_ship,
            entry.event.killer_rank,
            entry.event.victim,
            entry.event.victim_squadron,
            entry.event.victim_ship,
            entry.event.victim_rank,
            entry.event.kill_timestamp,
            caller_hashed_token,
            entry.event.system
        ).fetch_one(&mut **tx).await;

        let response = inserted_entry_result?;

        // We got the ID back. Put it into the Entry
        entry.insertion_state = InsertionState::PostCreate(response.kill_id)
    }
    Ok(())
}

pub(crate) async fn handle_diff_entries(
    mut diff_actions: Vec<KillEventWithDiff>,
    pool: &PgPool,
    caller_hashed_token: &str,
) -> Result<Vec<KillEventWithDiff>, Error> {
    if diff_actions
        .iter()
        .all(|x| matches!(x.insertion_state, InsertionState::Ignore))
    {
        // Only ignored events. Don't care about those :)
        return Ok(diff_actions);
    }

    // We use a transaction. Either everything gets added, or nothing at all!
    let mut tx = pool.begin().await?;

    if let Err(err) =
        insert_all_kills_to_create_with_tx(&mut diff_actions, caller_hashed_token, &mut tx).await
    {
        let _ = tx.rollback().await;
        return Err(err);
    }

    if let Err(err) =
        insert_all_kills_to_update_with_tx(&diff_actions, caller_hashed_token, &mut tx).await
    {
        let _ = tx.rollback().await;
        return Err(err);
    }

    let all_affected_cmdrs = get_all_affected_cmdrs(&diff_actions);

    if let Err(err) = update_data_for_commanders(Some(all_affected_cmdrs), &mut tx).await {
        let _ = tx.rollback().await;
        return Err(err);
    }

    tx.commit().await?;

    Ok(diff_actions)
}

pub(crate) async fn update_data_for_commanders(
    all_affected_cmdrs: Option<Vec<String>>,
    tx: &mut Transaction<'_, Postgres>,
) -> Result<(), Error> {
    // I am so, so sorry
    // WARNING:
    // IF YOU CHANGE ONE OF THE MATCH ENTRIES, YOU MUST CHANGE ALL OF THEM.
    // BEST TO MODIFY THE FIRST QUERY AND THEN REMOVE THE WHERE CLAUSE IN THE SECOND
    match all_affected_cmdrs {
        Some(all_affected_cmdrs) => {
            sqlx::query!(
            r#"
            WITH recent_data AS (
                SELECT 
                  cmdr AS cmdr_name, 
                  cmdr_squadron AS last_squadron, 
                  cmdr_rank AS last_rank, 
                  cmdr_power as last_power,
                  kill_timestamp AS last_event_timestamp
                FROM (
                  SELECT 
                    row_number() OVER (
                      PARTITION BY cmdr 
                      ORDER BY 
                        kill_timestamp DESC
                    ) AS row_num, 
                    * 
                  FROM (
                    SELECT 
                      killer AS cmdr, 
                      killer_squadron AS cmdr_squadron, 
                      killer_rank AS cmdr_rank,
                      killer_power AS cmdr_power,
                      kill_timestamp 
                    FROM 
                      elite_kills ek 
                    WHERE 
                      ek.killer in (SELECT unnest($1::text[])) 
                    UNION ALL 
                    SELECT 
                      victim AS cmdr, 
                      victim_squadron AS cmdr_squadron, 
                      victim_rank AS cmdr_rank, 
                      victim_power AS cmdr_power,
                      kill_timestamp 
                    FROM 
                      elite_kills ek 
                    WHERE 
                      ek.victim in (SELECT unnest($1::text[])) 
                  ) AS subquery_1
                  ORDER BY 
                    kill_timestamp DESC
                ) AS subquery_2
                WHERE 
                  row_num = 1
              ),
              kill_data AS (
                SELECT 
                  COALESCE(victim, killer) AS cmdr_name, 
                  COALESCE(deaths, 0) AS deaths, 
                  COALESCE(kills, 0) AS kills 
                FROM (
                  SELECT 
                    victim, 
                    COUNT(victim) AS deaths 
                  FROM 
                    public.elite_kills 
                  WHERE 
                    victim in (SELECT unnest($1::text[])) 
                  GROUP BY 
                    victim
                ) AS victims
                FULL JOIN (
                  SELECT 
                    killer, 
                    COUNT(killer) AS kills 
                  FROM 
                    public.elite_kills 
                  WHERE 
                    killer in (SELECT unnest($1::text[])) 
                  GROUP BY 
                    killer
                ) AS killers ON victims.victim = killers.killer
              )
              INSERT INTO elite_commanders (cmdr_name, last_squadron, last_rank, last_power, last_event_timestamp, deaths, kills)
              SELECT 
                recent_data.cmdr_name, 
                recent_data.last_squadron, 
                recent_data.last_rank, 
                recent_data.last_power,
                recent_data.last_event_timestamp, 
                coalesce (kill_data.deaths, 0), 
                coalesce (kill_data.kills, 0)
              FROM 
                recent_data
              FULL OUTER JOIN 
                kill_data ON recent_data.cmdr_name = kill_data.cmdr_name
                ON CONFLICT (cmdr_name) DO UPDATE
                SET 
                    last_squadron = EXCLUDED.last_squadron,
                    last_power = EXCLUDED.last_power,
                    last_rank = EXCLUDED.last_rank,
                    last_event_timestamp = EXCLUDED.last_event_timestamp,
                    deaths = coalesce (EXCLUDED.deaths, elite_commanders.deaths),
                    kills = coalesce (EXCLUDED.kills, elite_commanders.kills)
             "#,
            &all_affected_cmdrs
        ).execute(&mut **tx).await?;
        }
        None => {
            sqlx::query!(
          r#"
          WITH recent_data AS (
              SELECT 
                cmdr AS cmdr_name, 
                cmdr_squadron AS last_squadron, 
                cmdr_rank AS last_rank, 
                cmdr_power as last_power,
                kill_timestamp AS last_event_timestamp
              FROM (
                SELECT 
                  row_number() OVER (
                    PARTITION BY cmdr 
                    ORDER BY 
                      kill_timestamp DESC
                  ) AS row_num, 
                  * 
                FROM (
                  SELECT 
                    killer AS cmdr, 
                    killer_squadron AS cmdr_squadron, 
                    killer_rank AS cmdr_rank,
                    killer_power AS cmdr_power,
                    kill_timestamp 
                  FROM 
                    elite_kills ek 
                  UNION ALL 
                  SELECT 
                    victim AS cmdr, 
                    victim_squadron AS cmdr_squadron, 
                    victim_rank AS cmdr_rank, 
                    victim_power AS cmdr_power,
                    kill_timestamp 
                  FROM 
                    elite_kills ek 
                ) AS subquery_1
                ORDER BY 
                  kill_timestamp DESC
              ) AS subquery_2
              WHERE 
                row_num = 1
            ),
            kill_data AS (
              SELECT 
                COALESCE(victim, killer) AS cmdr_name, 
                COALESCE(deaths, 0) AS deaths, 
                COALESCE(kills, 0) AS kills 
              FROM (
                SELECT 
                  victim, 
                  COUNT(victim) AS deaths 
                FROM 
                  public.elite_kills 
                GROUP BY 
                  victim
              ) AS victims
              FULL JOIN (
                SELECT 
                  killer, 
                  COUNT(killer) AS kills 
                FROM 
                  public.elite_kills 
                GROUP BY 
                  killer
              ) AS killers ON victims.victim = killers.killer
            )
            INSERT INTO elite_commanders (cmdr_name, last_squadron, last_rank, last_power, last_event_timestamp, deaths, kills)
            SELECT 
              recent_data.cmdr_name, 
              recent_data.last_squadron, 
              recent_data.last_rank, 
              recent_data.last_power,
              recent_data.last_event_timestamp, 
              coalesce (kill_data.deaths, 0), 
              coalesce (kill_data.kills, 0)
            FROM 
              recent_data
            FULL OUTER JOIN 
              kill_data ON recent_data.cmdr_name = kill_data.cmdr_name
              ON CONFLICT (cmdr_name) DO UPDATE
              SET 
                  last_squadron = EXCLUDED.last_squadron,
                  last_power = EXCLUDED.last_power,
                  last_rank = EXCLUDED.last_rank,
                  last_event_timestamp = EXCLUDED.last_event_timestamp,
                  deaths = coalesce (EXCLUDED.deaths, elite_commanders.deaths),
                  kills = coalesce (EXCLUDED.kills, elite_commanders.kills)
           "#,
      ).execute(&mut **tx).await?;
        }
    }

    Ok(())
}

fn get_all_affected_cmdrs(diff_actions: &[KillEventWithDiff]) -> Vec<String> {
    diff_actions
        .iter()
        .flat_map(|x| vec![x.event.killer.clone(), x.event.victim.clone()])
        .unique()
        .collect_vec()
}

async fn insert_all_kills_to_update_with_tx(
    diff_actions: &[KillEventWithDiff],
    caller_hashed_token: &str,
    tx: &mut Transaction<'_, Postgres>,
) -> Result<(), Error> {
    // TODO: Rewrite to Batch Entry
    for entry in diff_actions {
        let db_id = match entry.insertion_state {
            InsertionState::Update(id) => id,
            _ => continue,
        };

        sqlx::query!(
            r#"
            UPDATE elite_kills
            SET 
                system_name = COALESCE(system_name, $1), 
                killer_ship = COALESCE(killer_ship, $2), 
                victim_ship = COALESCE(victim_ship, $3),
                killer_squadron = COALESCE(killer_squadron, $4),
                victim_squadron = COALESCE(victim_squadron, $5),
                hashed_token_updater = COALESCE(hashed_token_updater, $6),
                killer_power = COALESCE(killer_power, $7),
                victim_power = COALESCE(victim_power, $8)
            WHERE 
                kill_id = $9"#,
            entry.event.system,
            entry.event.killer_ship,
            entry.event.victim_ship,
            entry.event.killer_squadron,
            entry.event.victim_squadron,
            caller_hashed_token,
            entry.event.killer_power,
            entry.event.victim_power,
            db_id
        )
        .execute(&mut **tx)
        .await?;
    }

    Ok(())
}
