use chrono::{DateTime, Utc};
use itertools::Itertools;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use crate::{db::elite_leaderboard::KillboardEntryDb, util::sized_pagination::KillFilter};

use super::elite_leaderboard::KillboardEntry;

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct DbCmdrAssociation {
    cmdr_name: String,
    is_public: bool,
}

pub(crate) async fn get_all_cmdrs_associated_with_token(
    pool: &PgPool,
    owner_id: i64,
) -> Result<Vec<DbCmdrAssociation>, sqlx::Error> {
    let result: Vec<DbCmdrAssociation> = sqlx::query_as!(
        DbCmdrAssociation,
        r#"
        SELECT cmdr_name, is_public
        FROM elite_commanders
        WHERE owner_discord_id = $1"#,
        owner_id
    )
    .fetch_all(pool)
    .await?;

    Ok(result)
}

#[allow(dead_code)] // constructed by SQLX Macro
struct DbFuzzyResult {
    cmdr_name: String,
}

pub(crate) async fn search_cmdrs_fuzzy(
    term: &str,
    pool: &PgPool,
) -> Result<Vec<String>, sqlx::Error> {
    let result: Vec<DbFuzzyResult> = sqlx::query_as!(
        DbFuzzyResult,
        r#"
        SELECT cmdr_name
        FROM elite_commanders
        ORDER BY SIMILARITY(cmdr_name, $1) DESC
        LIMIT 20"#,
        term
    )
    .fetch_all(pool)
    .await?;

    Ok(result.into_iter().map(|x| x.cmdr_name).collect_vec())
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct DbKillHistoryResult {
    killer: String,
    killer_squadron: Option<String>,
    killer_rank: i64,
    killer_ship: Option<String>,
    self_died: Option<bool>,
    victim: String,
    victim_squadron: Option<String>,
    victim_rank: i64,
    victim_ship: Option<String>,
    kill_timestamp: DateTime<Utc>,
    kill_id: i64,
    full_count: Option<i64>,
    system_name: Option<String>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct KillHistoryResultEntry {
    killer: String,
    killer_squadron: Option<String>,
    killer_rank: i64,
    killer_ship: Option<String>,
    self_died: bool,
    victim: String,
    victim_squadron: Option<String>,
    victim_rank: i64,
    victim_ship: Option<String>,
    kill_timestamp: DateTime<Utc>,
    kill_id: i64,
    system: Option<String>,
}

impl From<DbKillHistoryResult> for KillHistoryResultEntry {
    fn from(value: DbKillHistoryResult) -> Self {
        KillHistoryResultEntry {
            killer: value.killer,
            killer_squadron: value.killer_squadron,
            killer_rank: value.killer_rank,
            killer_ship: value.killer_ship,
            self_died: value.self_died.unwrap_or(false),
            victim: value.victim,
            victim_squadron: value.victim_squadron,
            victim_rank: value.victim_rank,
            victim_ship: value.victim_ship,
            kill_timestamp: value.kill_timestamp,
            kill_id: value.kill_id,
            system: value.system_name,
        }
    }
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct CmdrQueryResult {
    pub(crate) page: usize,
    pub(crate) total_pages: usize,
    pub(crate) page_data: Vec<KillboardEntry>,
    pub(crate) kills: usize,
    pub(crate) deaths: usize,
    pub(crate) cmdr_name: String,
    pub(crate) squadron: Option<String>,
    pub(crate) power: Option<String>,
    pub(crate) rank: Option<usize>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct CmdrMetadata {
    pub(crate) kills: i64,
    pub(crate) deaths: i64,
    pub(crate) cmdr_name: String,
    pub(crate) power: Option<String>,
    pub(crate) squadron: Option<String>,
    pub(crate) rank: Option<i32>,
}

pub(crate) async fn get_kills_and_death_count_for_cmdr(
    cmdr: &str,
    pool: &PgPool,
) -> Result<Option<CmdrMetadata>, sqlx::Error> {
    let result: Option<CmdrMetadata> = sqlx::query_as!(
        CmdrMetadata,
        r#"
        select kills, deaths, cmdr_name, last_squadron as squadron, last_rank as rank, last_power as power
        from elite_commanders where cmdr_name = $1"#,
        cmdr,
    )
    .fetch_optional(pool)
    .await?;

    Ok(result)
}

pub(crate) async fn get_kill_history_for_cmdr(
    cmdr: &str,
    page: usize,
    page_size: usize,
    filter: KillFilter,
    pool: &PgPool,
) -> Result<Option<Vec<KillboardEntry>>, sqlx::Error> {
    let result: Vec<KillboardEntryDb> = match filter {
        KillFilter::All => sqlx::query_as!(
            KillboardEntryDb,
            r#"
            SELECT 
                kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, victim_power, killer_power
            FROM elite_kills_patched
            WHERE LOWER($1) = LOWER(killer) OR LOWER($1) = LOWER(victim)
            ORDER BY kill_timestamp DESC
            OFFSET $2
            LIMIT $3"#,
            cmdr,
            ((page - 1) * page_size) as i64,
            page_size as i64
        )
        .fetch_all(pool)
        .await?,
        KillFilter::Kills => sqlx::query_as!(
            KillboardEntryDb,
            r#"
            SELECT 
                kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, victim_power, killer_power
            FROM elite_kills_patched
            WHERE LOWER($1) = LOWER(killer)
            ORDER BY kill_timestamp DESC
            OFFSET $2
            LIMIT $3"#,
            cmdr,
            ((page - 1) * page_size) as i64,
            page_size as i64
        )
        .fetch_all(pool)
        .await?,
        KillFilter::Deaths => sqlx::query_as!(
            KillboardEntryDb,
            r#"
            SELECT 
                kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, victim_power, killer_power
            FROM elite_kills_patched
            WHERE LOWER($1) = LOWER(victim)
            ORDER BY kill_timestamp DESC
            OFFSET $2
            LIMIT $3"#,
            cmdr,
            ((page - 1) * page_size) as i64,
            page_size as i64
        )
        .fetch_all(pool)
        .await?
    };

    if result.is_empty() {
        return Ok(None);
    }

    let page_data = result
        .into_iter()
        .filter_map(|x| match KillboardEntry::try_from(x) {
            Ok(x) => Some(x),
            Err(_) => None,
        })
        .collect_vec();

    Ok(Some(page_data))
}
