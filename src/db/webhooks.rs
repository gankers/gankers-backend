use chrono::{DateTime, Utc};
use itertools::Itertools;
use sqlx::{PgPool, Postgres, Transaction};


pub(crate) async fn get_all_elite_kill_webhooks(pool: &PgPool) -> Result<Vec<String>, sqlx::Error> {
    struct Result {
        webhook_url: String,
    }

    let result = sqlx::query_as!(
        Result,
        r#"
        SELECT webhook_url
        FROM webhooks
        WHERE push_elite_kills"#,
    )
    .fetch_all(pool)
    .await?;

    Ok(result.into_iter().map(|x| x.webhook_url).collect_vec())
}

#[allow(dead_code)]
pub(crate) struct WeeklyEmissionInfo {
    pub(crate) webhook_url: String,
    pub(crate) push_elite_weekly_deep_dive: bool,
    pub(crate) push_elite_weekly: bool,
}

pub(crate) async fn get_all_elite_weekly_webhooks(
    pool: &PgPool,
) -> Result<Vec<WeeklyEmissionInfo>, sqlx::Error> {
    let result = sqlx::query_as!(
        WeeklyEmissionInfo,
        r#"
    SELECT webhook_url, push_elite_weekly_deep_dive, push_elite_weekly
    FROM webhooks
    WHERE push_elite_weekly_deep_dive OR push_elite_weekly"#,
    )
    .fetch_all(pool)
    .await?;

    Ok(result)
}

pub(crate) async fn enqueue_webhook_patch_job(
    pool: &PgPool,
    webhook: String,
    payload: String,
    correct_after: DateTime<Utc>,
) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"
        INSERT INTO webhook_patches_queue (patch_webhook_url, patch_body, not_before)
        VALUES ($1, $2, $3)
        "#,
        webhook,
        payload,
        correct_after
    )
    .execute(pool)
    .await?;
    Ok(())
}

// This returns the jobs in a transaction. The calling context has to commit the transaction if all is well.
pub(crate) async fn get_and_delete_expired_jobs(
    tx: &mut Transaction<'_, Postgres>,
) -> Result<Vec<(String, String)>, sqlx::Error> {
    struct DbResponse {
        patch_webhook_url: String,
        patch_body: String,
    }

    let result = sqlx::query_as!(
        DbResponse,
        r#"
            WITH entries_to_return AS (
            SELECT *
            FROM webhook_patches_queue
            WHERE not_before < NOW()
        )
        DELETE FROM webhook_patches_queue
        WHERE id IN (SELECT id FROM entries_to_return)
        RETURNING patch_webhook_url, patch_body;
        "#,
    )
    .fetch_all(&mut **tx)
    .await?
    .into_iter()
    .map(|x| (x.patch_webhook_url, x.patch_body))
    .collect();
    Ok(result)
}
