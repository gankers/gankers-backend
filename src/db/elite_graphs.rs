///
/// This Module is used to return data for visualizations on the website
///
use chrono::{DateTime, Utc};
use itertools::Itertools;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use crate::util::{
    rank_names::get_rank_name_and_colour,
    ship_names::{get_colours_for_ship_ids, get_pretty_names_for_ship_ids, is_on_foot},
};

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedKillsEntry {
    time: DateTime<Utc>,
    kills: usize,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedKillsDeathsEntry {
    time: DateTime<Utc>,
    kills: usize,
    deaths: usize,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedCmdrKillsEntry {
    time: DateTime<Utc>,
    kills: usize,
    cmdr: String,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedShipKillsDeathsEntry {
    time: DateTime<Utc>,
    kills: usize,
    deaths: usize,
    ship_id: String,
    ship_name: String,
    ship_color: String,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedRankKillsDeathsEntry {
    time: DateTime<Utc>,
    kills: usize,
    deaths: usize,
    rank: u8,
    rank_color: String,
    rank_name: String,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct TimedSystemKillsEntry {
    time: DateTime<Utc>,
    kills: usize,
    system_name: String,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct WeekSummarySystemEntry {
    pub(super) system_name: String,
    pub(super) kills: usize,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct WeekSummaryShipEntry {
    ship_id: String,
    ship_name: String,
    ship_color: String,
    kills: usize,
    deaths: usize,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct WeekSummaryRankEntry {
    rank: u8,
    rank_name: String,
    rank_color: String,
    kills: usize,
    deaths: usize,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct WeekSummary {
    pub(super) systems: Vec<WeekSummarySystemEntry>,
    ships: Vec<WeekSummaryShipEntry>,
    ranks: Vec<WeekSummaryRankEntry>,
}

/*
*
*  select date_trunc('week', now() at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours'
*
*  This returns the start of the "Elite Week" of a given timestamp - being 8AM UTC on a Thursday
*  This basically takes a timestamp, moved it 3 days and 8 hours back, so that Thu 8AM is at Mo 0AM;
*  then truncates by week (time-stamp gets reduced to the last Mo 0AM)
*  finally, the Time is moved back by 3 days and 8 hours so our Mo 0AM is back at Thu 8AM
*/

/// This does not return timed metrics, but an aggregated view for the entire week - useful for Pie-Charts
pub(crate) async fn get_week_summary_metrics(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<WeekSummary, sqlx::Error> {
    struct DbResponseSystem {
        kills: Option<i64>,
        system_name: Option<String>,
    }

    let result_system = sqlx::query_as!(
        DbResponseSystem,
        r#"
        SELECT 
            COUNT(*) AS kills,
            ek.system_name as system_name
        FROM 
            elite_kills ek 
        WHERE 
            ek.kill_timestamp >= date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours'
            AND ek.kill_timestamp < date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '1 week 3 days 8 hours'
        GROUP BY 
            system_name
        ORDER BY 
            kills DESC
        "#,
        timestamp_in_week
    )
    .fetch_all(pool);

    struct DbResponseShips {
        kills: Option<i64>,
        deaths: Option<i64>,
        ship: Option<String>,
    }

    let result_ships = sqlx::query_as!(
        DbResponseShips,
        r#"
        WITH current_week_start AS (
            -- Calculate the start of the current custom week (Thursday 8 AM UTC)
            SELECT 
                date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours' AS week_start
        ),
        kill_data AS (
            -- Get kills by killer_ship
            SELECT 
                ek.killer_ship AS ship, 
                COUNT(*) AS kills
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'
            GROUP BY 
                ek.killer_ship
        ),
        death_data AS (
            -- Get deaths by victim_ship
            SELECT 
                ek.victim_ship AS ship, 
                COUNT(*) AS deaths
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'
            GROUP BY 
                ek.victim_ship
        )
        -- Perform a full join on the ship to get both kills and deaths
        SELECT 
            COALESCE(kd.ship, dd.ship) AS ship,
            COALESCE(kd.kills, 0) AS kills,
            COALESCE(dd.deaths, 0) AS deaths
        FROM 
            kill_data kd
        FULL OUTER JOIN 
            death_data dd 
            ON kd.ship = dd.ship
        ORDER BY 
            kills DESC;
        "#,
        timestamp_in_week
    )
    .fetch_all(pool);

    struct DbResponseRanks {
        kills: Option<i64>,
        deaths: Option<i64>,
        rank: Option<i16>,
    }

    let result_ranks = sqlx::query_as!(
        DbResponseRanks,
        r#"
        WITH current_week_start AS (
            -- Calculate the start of the current custom week (Thursday 8 AM UTC)
            SELECT 
                date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours' AS week_start
        ),
        kill_data AS (
            -- Get kills by killer_rank
            SELECT 
                ek.killer_rank AS rank, 
                COUNT(*) AS kills
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'
            GROUP BY 
                ek.killer_rank
        ),
        death_data AS (
            -- Get deaths by victim_rank
            SELECT 
                ek.victim_rank AS rank, 
                COUNT(*) AS deaths
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'
            GROUP BY 
                ek.victim_rank
        )
        -- Perform a full join on the rank to get both kills and deaths
        SELECT 
            COALESCE(kd.rank, dd.rank) AS rank,
            COALESCE(kd.kills, 0) AS kills,
            COALESCE(dd.deaths, 0) AS deaths
        FROM 
            kill_data kd
        FULL OUTER JOIN 
            death_data dd 
            ON kd.rank = dd.rank
        ORDER BY 
            rank asc;
        "#,
        timestamp_in_week
    )
    .fetch_all(pool);

    // All futures are running in parallel in the back. Wait for them to all finish by basically awaiting

    let result_system = result_system.await?.into_iter().filter_map(|x| {
        if x.kills.is_none() || x.system_name.is_none() {
            None
        } else {
            Some(WeekSummarySystemEntry {
                kills: x.kills.unwrap() as usize,
                system_name: x.system_name.unwrap(),
            })
        }
    });
    let result_ships = result_ships.await?.into_iter().filter_map(|x| {
        if x.ship.is_none() {
            None
        } else {
            let ship = x.ship.unwrap();

            Some(WeekSummaryShipEntry {
                kills: x.kills.unwrap_or(0) as usize,
                deaths: x.deaths.unwrap_or(0) as usize,
                ship_name: get_pretty_names_for_ship_ids(&ship, true).unwrap_or("N/A".to_string()),
                ship_color: get_colours_for_ship_ids(&ship),
                ship_id: match is_on_foot(&ship) {
                    true => "on_foot".to_string(),
                    false => ship,
                },
            })
        }
    });
    let result_ranks = result_ranks.await?.into_iter().filter_map(|x| {
        if x.rank.is_none() {
            None
        } else {
            let rank = x.rank.unwrap();
            let rank = if rank < 0 {
                0u8
            } else if rank > 0xFF {
                0xFFu8 // basically keep within u8 bounds
            } else {
                rank as u8
            };

            let (rank_name, rank_color) = get_rank_name_and_colour(rank);

            Some(WeekSummaryRankEntry {
                kills: x.kills.unwrap_or(0) as usize,
                deaths: x.deaths.unwrap_or(0) as usize,
                rank,
                rank_color,
                rank_name,
            })
        }
    });

    Ok(WeekSummary {
        systems: result_system.collect(),
        ships: result_ships.collect(),
        ranks: result_ranks.collect(),
    })
}

pub(crate) async fn get_hourly_ship_count_for_week(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<Vec<TimedShipKillsDeathsEntry>, sqlx::Error> {
    struct DbResponse {
        kill_hour: Option<DateTime<Utc>>,
        killer_ship: Option<String>,
        kills: Option<i64>,
        deaths: Option<i64>,
    }

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        WITH current_week_start AS (
            -- Calculate the start of the current custom week (Thursday 8 AM UTC)
            SELECT 
                date_trunc('week', $1 at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours' AS week_start
        ),
        kill_data AS (
            -- Aggregate kills by hour and killer_ship
            SELECT 
                date_trunc('hour', ek.kill_timestamp) AS kill_hour,
                ek.killer_ship,  
                COUNT(*) AS kills
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start 
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'  -- Only entries from the current custom week
            GROUP BY 
                kill_hour, 
                ek.killer_ship
        ),
        death_data AS (
            -- Aggregate deaths by hour and victim_ship
            SELECT 
                date_trunc('hour', ek.kill_timestamp) AS death_hour,
                ek.victim_ship AS killer_ship,  -- Treat the victim's ship as the "killer_ship" for deaths
                COUNT(*) AS deaths
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start 
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'  -- Only entries from the current custom week
            GROUP BY 
                death_hour, 
                ek.victim_ship
        )
        -- Join the kill and death data based on killer_ship and hour
        SELECT 
            k.kill_hour, 
            k.killer_ship, 
            k.kills, 
            COALESCE(d.deaths, 0) AS deaths
        FROM 
            kill_data k
        LEFT JOIN 
            death_data d 
            ON k.kill_hour = d.death_hour 
            AND k.killer_ship = d.killer_ship
        ORDER BY 
            k.kill_hour ASC, 
            k.kills DESC;
        "#,
        timestamp_in_week
    )
    .fetch_all(pool)
    .await?;

    let converted = result.into_iter().filter_map(|x| {
        if x.kill_hour.is_none() || x.killer_ship.is_none() {
            None
        } else {
            let ship = x.killer_ship.as_ref().unwrap();

            Some(TimedShipKillsDeathsEntry {
                time: x.kill_hour.unwrap(),
                kills: x.kills.unwrap_or(0) as usize,
                deaths: x.deaths.unwrap_or(0) as usize,
                ship_name: get_pretty_names_for_ship_ids(ship, true).unwrap_or("N/A".to_string()),
                ship_color: get_colours_for_ship_ids(ship),
                ship_id: match is_on_foot(ship) {
                    true => "on_foot".to_string(),
                    false => ship.to_string(),
                },
            })
        }
    });

    Ok(converted.collect())
}

pub(crate) async fn get_hourly_system_count_for_week(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<Vec<TimedSystemKillsEntry>, sqlx::Error> {
    struct DbResponse {
        kills: Option<i64>,
        system_name: Option<String>,
        kill_hour: Option<DateTime<Utc>>,
    }

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        SELECT 
            COUNT(*) AS kills,
            ek.system_name as system_name,
            date_trunc('hour', ek.kill_timestamp) as kill_hour
        FROM 
            elite_kills ek 
        WHERE 
            ek.kill_timestamp >= date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours'
            AND ek.kill_timestamp < date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '1 week 3 days 8 hours'
        GROUP BY 
            date_trunc('hour', ek.kill_timestamp), system_name
        ORDER BY 
            date_trunc('hour', ek.kill_timestamp) ASC, kills DESC
        "#,
        timestamp_in_week
    )
    .fetch_all(pool)
    .await?;

    let iter = result.into_iter().filter_map(|x| {
        if x.system_name.is_none() || x.kill_hour.is_none() {
            None
        } else {
            Some(TimedSystemKillsEntry {
                time: x.kill_hour.unwrap(),
                kills: x.kills.unwrap_or(0) as usize,
                system_name: x.system_name.unwrap(),
            })
        }
    });

    Ok(iter.collect())
}

pub(crate) async fn get_hourly_rank_count_for_week(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<Vec<TimedRankKillsDeathsEntry>, sqlx::Error> {
    struct DbResponse {
        kill_hour: Option<DateTime<Utc>>,
        killer_rank: Option<i16>,
        kills: Option<i64>,
        deaths: Option<i64>,
    }

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        WITH current_week_start AS (
            -- Calculate the start of the current custom week (Thursday 8 AM UTC)
            SELECT 
                date_trunc('week', $1 at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours' AS week_start
        ),
        kill_data AS (
            -- Aggregate kills by hour and killer_rank
            SELECT 
                date_trunc('hour', ek.kill_timestamp) AS kill_hour,
                ek.killer_rank,  
                COUNT(*) AS kills
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start 
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'  -- Only entries from the current custom week
            GROUP BY 
                kill_hour, 
                ek.killer_rank
        ),
        death_data AS (
            -- Aggregate deaths by hour and victim_rank
            SELECT 
                date_trunc('hour', ek.kill_timestamp) AS death_hour,
                ek.victim_rank AS killer_rank,  -- Treat the victim's rank as the "killer_rank" for deaths
                COUNT(*) AS deaths
            FROM 
                elite_kills ek,
                current_week_start cws
            WHERE 
                ek.kill_timestamp >= cws.week_start 
                AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'  -- Only entries from the current custom week
            GROUP BY 
                death_hour, 
                ek.victim_rank
        )
        -- Join the kill and death data based on killer_rank and hour
        SELECT 
            k.kill_hour, 
            k.killer_rank, 
            k.kills, 
            COALESCE(d.deaths, 0) AS deaths
        FROM 
            kill_data k
        LEFT JOIN 
            death_data d 
            ON k.kill_hour = d.death_hour 
            AND k.killer_rank = d.killer_rank
        ORDER BY 
            k.kill_hour ASC, 
            k.kills DESC;
        "#,
        timestamp_in_week
    )
    .fetch_all(pool)
    .await?;

    let converted = result.into_iter().filter_map(|x| {
        if x.kill_hour.is_none() || x.killer_rank.is_none() {
            None
        } else {
            let rank = x.killer_rank.unwrap();
            let rank = if rank < 0 {
                0u8
            } else if rank > 0xFF {
                0xFFu8 // basically keep within u8 bounds
            } else {
                rank as u8
            };

            let (rank_name, rank_color) = get_rank_name_and_colour(rank);

            Some(TimedRankKillsDeathsEntry {
                time: x.kill_hour.unwrap(),
                kills: x.kills.unwrap_or(0) as usize,
                deaths: x.deaths.unwrap_or(0) as usize,
                rank,
                rank_color,
                rank_name,
            })
        }
    });

    Ok(converted.collect())
}

pub(crate) async fn get_weekly_sum_series(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<Vec<TimedKillsEntry>, sqlx::Error> {
    struct DbResponse {
        count: Option<i64>,
        week_start: Option<DateTime<Utc>>,
    }

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        WITH last_20_weeks AS (
            -- Generate week start times for the last 20 weeks (Thursday 8 AM UTC)
            SELECT 
                generate_series(
                    date_trunc('week', $1 at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours' - INTERVAL '19 weeks', 
                    date_trunc('week', $1 at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours', 
                    '1 week'
                ) AT TIME ZONE 'UTC' AS week_start
        ),
        killer_stats AS (
            SELECT 
                coalesce(COUNT(*), 0) AS kills, 
                date_trunc('week', ek.kill_timestamp) + INTERVAL '3 days 8 hours' AS week_start
            FROM elite_kills_patched ek
            GROUP BY week_start
        )
        -- Perform LEFT JOIN with last 10 weeks series
        SELECT 
            lw.week_start,
            coalesce(ks.kills, 0) AS count
        FROM last_20_weeks lw
        LEFT JOIN killer_stats ks ON lw.week_start = ks.week_start
        ORDER BY lw.week_start DESC;
        "#,
        timestamp_in_week
    )
    .fetch_all(pool)
    .await?;

    Ok(result
        .iter()
        .map(|x| TimedKillsEntry {
            time: x.week_start.unwrap_or(Utc::now()),
            kills: x.count.unwrap_or(0) as usize,
        })
        .collect_vec())
}

async fn get_weekly_top_ten(
    pool: &PgPool,
    timestamp_in_week: DateTime<Utc>,
) -> Result<Vec<(String, usize)>, sqlx::Error> {
    struct DbResponse {
        kill_count: Option<i64>,
        killer: Option<String>,
    }
    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        SELECT 
            COUNT(*) AS kill_count, 
            ek.killer 
        FROM 
            elite_kills ek 
        WHERE 
            ek.kill_timestamp >= date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours'
            AND ek.kill_timestamp < date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '1 week 3 days 8 hours'
        GROUP BY 
            ek.killer 
        ORDER BY 
            kill_count DESC 
        LIMIT 10;
        "#,
        timestamp_in_week,
    )
    .fetch_all(pool)
    .await?;

    Ok(result
        .into_iter()
        .map(|x| (x.killer.unwrap(), x.kill_count.unwrap() as usize))
        .collect())
}

pub(crate) async fn get_kills_and_death_by_week_for_cmdr(
    pool: &PgPool,
    cmdr: String,
) -> Result<Vec<TimedKillsDeathsEntry>, sqlx::Error> {
    struct DbResponse {
        week_start: Option<DateTime<Utc>>,
        kills: Option<i64>,
        deaths: Option<i64>,
    }
    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        WITH last_20_weeks AS (
            -- Generate week start times for the last 20 weeks (Thursday 8 AM UTC)
            SELECT 
                generate_series(
                    date_trunc('week', now() at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours' - INTERVAL '19 weeks', 
                    date_trunc('week', now() at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours', 
                    '1 week'
                ) AT TIME ZONE 'UTC' AS week_start
        ),
        killer_stats AS (
            SELECT 
                coalesce(COUNT(*), 0) AS kills, 
                date_trunc('week', ek.kill_timestamp) + INTERVAL '3 days 8 hours' AS week_start
            FROM elite_kills_patched ek
            WHERE lower(ek.killer) = lower($1)
            GROUP BY week_start
        ),
        victim_stats AS (
            SELECT 
                coalesce(COUNT(*), 0) AS deaths, 
                date_trunc('week', ek.kill_timestamp) + INTERVAL '3 days 8 hours' AS week_start
            FROM elite_kills_patched ek
            WHERE lower(ek.victim) = lower($1)
            GROUP BY week_start
        )
        -- Perform LEFT JOIN with last 20 weeks series
        SELECT 
            lw.week_start,
            coalesce(ks.kills, 0) AS kills, 
            coalesce(vs.deaths, 0) AS deaths
        FROM last_20_weeks lw
        LEFT JOIN killer_stats ks ON lw.week_start = ks.week_start
        LEFT JOIN victim_stats vs ON lw.week_start = vs.week_start
        ORDER BY lw.week_start ASC;
        "#,
        cmdr,
    )
    .fetch_all(pool)
    .await?;

    let kd_series = result.iter().map(|x| TimedKillsDeathsEntry {
        kills: x.kills.unwrap() as usize,
        deaths: x.deaths.unwrap() as usize,
        time: x.week_start.unwrap(),
    });

    Ok(kd_series.collect())
}

pub(crate) async fn get_weekly_board_progress_top_ten(
    pool: &PgPool,
    // This is used to figure out the current week
    week_timestamp: DateTime<Utc>,
) -> Result<Vec<TimedCmdrKillsEntry>, sqlx::Error> {
    struct DbResponse {
        kill_hour: Option<DateTime<Utc>>,
        killer: Option<String>,
        kills: Option<i64>,
    }

    let top_ten_query = get_weekly_top_ten(pool, week_timestamp);

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        WITH current_week_start AS (
            -- Calculate the start of the current custom week (Thursday 8 AM UTC)
            SELECT 
                date_trunc('week', $1 at TIME zone 'UTC' - interval '3 days 8 hours' ) + INTERVAL '3 days 8 hours' AS week_start
        )
        SELECT 
            -- Group by hour and cmdr
            date_trunc('hour', ek.kill_timestamp) AS kill_hour,
            ek.killer,  
            COUNT(*) AS kills
        FROM 
            elite_kills ek,
            current_week_start cws
        WHERE 
            ek.kill_timestamp >= cws.week_start AND ek.kill_timestamp < cws.week_start + INTERVAL '1 week'  -- Only entries from the current custom week
        GROUP BY 
            kill_hour, 
            ek.killer 
        ORDER BY 
            kill_hour ASC, 
            kills DESC;"#,
        week_timestamp
    )
    .fetch_all(pool)
    .await?;

    let top_ten = top_ten_query.await?;

    // We only care about the top 10 entries
    Ok(result
        .into_iter()
        .filter_map(
            |x| match top_ten.iter().any(|y| &y.0 == x.killer.as_ref().unwrap()) {
                true => Some(TimedCmdrKillsEntry {
                    time: x.kill_hour.unwrap(),
                    kills: x.kills.unwrap() as usize,
                    cmdr: x.killer.unwrap(),
                }),
                false => None,
            },
        )
        .collect_vec())
}

pub(crate) async fn get_weekly_board_progress_all(
    pool: &PgPool,
    // This is used to figure out the current week
    week_timestamp: DateTime<Utc>,
) -> Result<Vec<TimedKillsEntry>, sqlx::Error> {
    struct DbResponse {
        kill_hour: Option<DateTime<Utc>>,
        kills: Option<i64>,
    }

    let result: Vec<DbResponse> = sqlx::query_as!(
        DbResponse,
        r#"
        SELECT 
            COUNT(*) AS kills, 
            date_trunc('hour', ek.kill_timestamp) as kill_hour
        FROM 
            elite_kills ek 
        WHERE 
            ek.kill_timestamp >= date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '3 days 8 hours'
            AND ek.kill_timestamp < date_trunc('week', $1 AT TIME ZONE 'UTC' - INTERVAL '3 days 8 hours') 
                + INTERVAL '1 week 3 days 8 hours'
        GROUP BY 
            date_trunc('hour', ek.kill_timestamp) 
        ORDER BY 
            date_trunc('hour', ek.kill_timestamp)  
        "#,
        week_timestamp,
    )
    .fetch_all(pool)
    .await?;

    Ok(result
        .into_iter()
        .map(|x| TimedKillsEntry {
            time: x.kill_hour.unwrap_or(Utc::now()),
            kills: x.kills.unwrap_or(0) as usize,
        })
        .collect())
}
