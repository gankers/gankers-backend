use std::time::Duration;

use chrono::{DateTime, TimeDelta, Utc};
use itertools::Itertools;
use rand::Rng;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{Error, PgPool};
use tracing::{error, warn};

use crate::{db::elite_leaderboard::KillboardEntryDb, services::elite::KillEvent};

pub(super) struct DbKillEvent {
    pub(super) kill_id: i32,
    pub(super) system_name: Option<String>,
    pub(super) killer: String,
    pub(super) victim: String,
    pub(super) victim_squadron: Option<String>,
    pub(super) killer_squadron: Option<String>,
    pub(super) victim_rank: i16,
    pub(super) killer_rank: i16,
    pub(super) killer_ship: Option<String>,
    pub(super) killer_power: Option<String>,
    pub(super) victim_ship: Option<String>,
    pub(super) victim_power: Option<String>,
    pub(super) kill_timestamp: DateTime<chrono::Utc>,
}

impl From<&DbKillEvent> for KillEvent {
    fn from(val: &DbKillEvent) -> Self {
        KillEvent {
            kill_id: Some(val.kill_id),
            system: val.system_name.clone(),
            killer: val.killer.clone(),
            victim: val.victim.clone(),
            victim_rank: val.victim_rank,
            killer_rank: val.killer_rank,
            killer_ship: val.killer_ship.clone(),
            victim_ship: val.victim_ship.clone(),
            kill_timestamp: val.kill_timestamp,
            killer_squadron: val.killer_squadron.clone(),
            victim_squadron: val.victim_squadron.clone(),
            killer_power: val.killer_power.clone(),
            victim_power: val.victim_power.clone(),
        }
    }
}

pub(crate) async fn insert_spoof_overlay(
    pool: &PgPool,
    spoof_until: DateTime<Utc>,
    spoof_system: Option<String>,
    spoof_victim_ship: Option<String>,
    spoof_id: i32,
) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"
        INSERT INTO elite_kills_overlay (kill_id, system_name, victim_ship, valid_until)
        VALUES ($1, $2, $3, $4)
        "#,
        spoof_id,
        spoof_system,
        spoof_victim_ship,
        spoof_until
    )
    .execute(pool)
    .await?;
    Ok(())
}

pub(crate) async fn determine_poison_system(
    pool: &PgPool,
    current_system: Option<String>,
    cmdr_name: String,
) -> Option<String> {
    struct SpoofSystemsResult {
        system_name: String,
    }

    struct SystemAndCount {
        system_name: Option<String>,
        count: Option<i64>,
    }

    // Check if there is an entry already and return immediately if it's a candidate
    match sqlx::query_as!(
        SpoofSystemsResult,
        "SELECT system_name FROM spoof_systems WHERE cmdr_name = $1 AND valid_until >= NOW()",
        &cmdr_name
    )
    .fetch_optional(pool)
    .await
    {
        Ok(x) => {
            if let Some(x) = x {
                let is_current_system = match &current_system {
                    Some(current) => x.system_name == *current,
                    None => false,
                };

                if !is_current_system {
                    return Some(x.system_name);
                }
            }
        }
        Err(_) => return None,
    };

    // Else determine a (weighted) random system
    let weighted_systems: Vec<(String, usize)> = match sqlx::query_as!(
        SystemAndCount,
        r#"
            WITH systems AS (
                SELECT system_name
                FROM elite_kills_patched ek
                WHERE LOWER(ek.killer) = LOWER($1) 
                AND system_name IS NOT NULL
                ORDER BY ek.kill_timestamp DESC
                LIMIT 100
            )
            SELECT system_name, COUNT(*) 
            FROM systems
            GROUP BY system_name
            ORDER BY COUNT(*) DESC;"#,
        cmdr_name,
    )
    .fetch_all(pool)
    .await
    {
        Ok(x) => x,
        Err(_) => return None,
    }
    .into_iter()
    .filter_map(|x| {
        if x.count.is_none()
            || x.system_name.as_deref() == Some("HIP 97950")
            || x.system_name.is_none()
        {
            None
        } else {
            let result = (x.system_name.unwrap(), x.count.unwrap() as usize);
            if current_system.is_some() && result.0 == current_system.clone().unwrap() {
                None
            } else {
                Some(result)
            }
        }
    })
    .collect_vec();

    if weighted_systems.is_empty() {
        return None;
    }

    fn determine_system(weighted_systems: Vec<(String, usize)>) -> Option<String> {
        let total_weight: usize = weighted_systems.iter().map(|x| x.1).sum();

        let mut rng = rand::thread_rng();
        let pick = rng.gen_range(0..total_weight);
        let mut cumulative = 0usize;
        for (name, weight) in weighted_systems {
            if weight + cumulative > pick {
                return Some(name);
            }
            cumulative += weight;
        }
        None
    }

    let system = match determine_system(weighted_systems) {
        Some(x) => x,
        None => {
            // The for-Loop should be exhaustive
            // so should never get to here
            warn!("Escaped poison loop. - should only happen if no-one submitted kills last week.");
            return None;
        }
    };

    // We have a new system, commit it to DB
    match sqlx::query!(
        "
            INSERT INTO spoof_systems (cmdr_name, system_name, valid_until)
            VALUES ($1, $2, $3) 
            ON CONFLICT (cmdr_name)
            DO UPDATE SET
                system_name = $2,
                valid_until = $3;
            ",
        cmdr_name,
        &system,
        Utc::now() + TimeDelta::hours(1)
    )
    .execute(pool)
    .await
    {
        Ok(_) => {}
        Err(err) => {
            error!("Failed to insert spoof system. Reason: {}", err)
        }
    }

    Some(system)
}

pub(crate) async fn determine_poison_ship_name(
    pool: &PgPool,
    system_name: String,
) -> Option<String> {
    struct DbEntry {
        kill_count: Option<i64>,
        victim_ship: Option<String>,
    }

    let result: Vec<(String, usize)> = match sqlx::query_as!(
        DbEntry,
        r#"
            SELECT COUNT(*) AS kill_count, 
                victim_ship 
            FROM elite_kills ek 
            WHERE system_name = $1 
            AND victim_ship IS NOT NULL
            GROUP BY victim_ship 
            ORDER BY kill_count DESC;
            "#,
        system_name,
    )
    .fetch_all(pool)
    .await
    {
        Ok(x) => x,
        Err(e) => {
            error!("Error while trying to get victim count for system: {}", e);
            return None;
        }
    }
    .into_iter()
    .filter_map(|x| {
        if x.kill_count.is_some() && x.victim_ship.is_some() {
            Some((x.victim_ship.unwrap(), x.kill_count.unwrap() as usize))
        } else {
            None
        }
    })
    .collect_vec();

    if result.is_empty() {
        return None;
    }

    let total_weight = result.iter().map(|x| x.1).sum();

    let mut rng = rand::thread_rng();
    let pick = rng.gen_range(0..total_weight);

    let mut cumulative = 0usize;
    for (name, weight) in result {
        if weight + cumulative > pick {
            return Some(name);
        }
        cumulative += weight;
    }
    // The for-Loop should be exhaustive
    // so should never get to here
    warn!("Escaped poison loop. - should only happen if no-one submitted kills last week.");
    None
}

pub(crate) async fn get_kill_from_interaction_index_and_timestamp_if_exists(
    interaction: &str,
    timestamp: chrono::DateTime<chrono::Utc>,
    pool: &PgPool,
) -> Result<Option<(i32, KillEvent)>, Error> {
    let lower_bound = timestamp - Duration::from_secs(10);
    let upper_bound = timestamp + Duration::from_secs(10);

    let result: Option<_> = sqlx::query_as!(
        KillboardEntryDb,
        r#"
        SELECT system_name, killer, killer_ship, victim, victim_ship, kill_timestamp, kill_id, victim_rank, killer_rank, victim_squadron, killer_squadron, interaction_index, killer_power, victim_power
        FROM elite_kills_patched
        WHERE kill_timestamp < $1 
            AND kill_timestamp > $2
            AND interaction_index = $3"#,
        upper_bound,
        lower_bound,
        interaction
    )
    .fetch_optional(pool)
    .await?;

    Ok(match result {
        None => None,
        Some(x) => {
            let killevent: Option<KillEvent> = x.into();
            match killevent {
                None => None,
                Some(event) => Some((event.kill_id.unwrap_or(0), event)),
            }
        }
    })
}

#[derive(Debug, Serialize, Deserialize, JsonSchema, PartialEq, PartialOrd)]
pub(crate) enum InsertionResult {
    Created,
    Updated,
    Ignored,
}

impl ToString for InsertionResult {
    fn to_string(&self) -> String {
        (match &self {
            InsertionResult::Created => "Created",
            InsertionResult::Updated => "Updated",
            InsertionResult::Ignored => "Ignored",
        })
        .to_string()
    }
}

pub(crate) async fn insert_new_live_event(
    interaction: &str,
    event: &KillEvent,
    pool: &PgPool,
    caller_hashed_token: &str,
) -> Result<(i32, InsertionResult), Error> {
    // First check, if an event already exists
    let result = get_kill_from_interaction_index_and_timestamp_if_exists(
        interaction,
        event.kill_timestamp,
        pool,
    )
    .await?;

    match result {
        Some((val_idx, val)) => {
            // if here: An entry was already present!
            match is_update_needed(&val, event) {
                true => {
                    // We can update an existing entry!
                    update_db_entry(val_idx, event, pool)
                        .await
                        .map(|_| (val_idx, InsertionResult::Updated))
                }
                false => {
                    // No update. Just log the event in the replay-Table and be done with it
                    Ok((val_idx, InsertionResult::Ignored))
                }
            }
        }
        None => {
            // No entry yet. Create a new one!
            match create_new_pvpkill(event, interaction, caller_hashed_token, pool).await {
                Ok(val_idx) => Ok((val_idx, InsertionResult::Created)),
                Err(err) => Err(err),
            }
        }
    }
}

/// private function, does only update cells that are NULL
/// please only call this if an update is actually needed
/// does NOT write to replay log
async fn update_db_entry(
    db_id: i32,
    event: &KillEvent,
    pool: &sqlx::Pool<sqlx::Postgres>,
) -> Result<(), Error> {
    sqlx::query!(
        r#"
        UPDATE elite_kills
        SET 
            system_name = COALESCE(system_name, $1), 
            killer_ship = COALESCE(killer_ship, $2), 
            victim_ship = COALESCE(victim_ship, $3),
            victim_squadron = COALESCE(victim_squadron, $4),
            killer_squadron = COALESCE(killer_squadron, $5),
            victim_power = COALESCE(victim_power, $6),
            killer_power = COALESCE(killer_power, $7)
        WHERE 
            kill_id = $8"#,
        event.system,
        event.killer_ship,
        event.victim_ship,
        event.victim_squadron,
        event.killer_squadron,
        event.victim_power,
        event.killer_power,
        db_id
    )
    .execute(pool)
    .await?;

    Ok(())
}

/// Checks if an event has new data that was missing before, and as such
/// needs an update
pub(crate) fn is_update_needed(db_entry: &KillEvent, received_entry: &KillEvent) -> bool {
    let has_killer_ship_update =
        db_entry.killer_ship.is_none() && received_entry.killer_ship.is_some();
    let has_victim_ship_update =
        db_entry.victim_ship.is_none() && received_entry.victim_ship.is_some();
    let has_system_update = db_entry.system.is_none() && received_entry.system.is_some();
    let has_any_squadron_update = (db_entry.victim_squadron.is_none()
        && received_entry.victim_squadron.is_some())
        || (db_entry.killer_squadron.is_none() && received_entry.killer_squadron.is_some());
    let has_any_power_update = (db_entry.killer_power.is_none()
        && received_entry.killer_power.is_some())
        || (db_entry.victim_power.is_none() && received_entry.victim_power.is_some());
    has_killer_ship_update
        || has_victim_ship_update
        || has_system_update
        || has_any_squadron_update
        || has_any_power_update
}

async fn create_new_pvpkill(
    event: &KillEvent,
    interaction: &str,
    hashed_token: &str,
    pool: &PgPool,
) -> Result<i32, Error> {
    struct CreateInsertResponse {
        kill_id: i32,
    }

    let inserted_entry_result: CreateInsertResponse = sqlx::query_as!(
        CreateInsertResponse,
        r#"
        INSERT INTO "elite_kills"
        (interaction_index, killer, killer_squadron, killer_ship, killer_rank, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, hashed_token_creator, system_name, killer_power, victim_power)
        VALUES
        ($1               , $2    ,              $3,          $4,          $5,     $6,              $7,          $8,          $9,            $10,                  $11,         $12,          $13,          $14)
        RETURNING (kill_id)
        "#,
        interaction,
        event.killer,
        event.killer_squadron,
        event.killer_ship,
        event.killer_rank,
        event.victim,
        event.victim_squadron,
        event.victim_ship,
        event.victim_rank,
        event.kill_timestamp,
        hashed_token,
        event.system,
        event.killer_power,
        event.victim_power,
    ).fetch_one(pool).await?;

    Ok(inserted_entry_result.kill_id)
}

#[cfg(test)]
mod tests {
    use chrono::Utc;

    use crate::services::elite::types::InteractionKey;

    use super::*;

    fn new_event() -> KillEvent {
        KillEvent {
            kill_id: None,
            system: Some("TEST_SYSTEM".to_string()),
            killer: "TEST_KILLER".to_string(),
            killer_rank: 1,
            victim: "TEST_VICTIM".to_string(),
            victim_rank: 1,
            killer_ship: Some("python_nx".to_string()),
            victim_ship: Some("python_nx".to_string()),
            killer_squadron: None,
            victim_squadron: None,
            kill_timestamp: Utc::now(),
            killer_power: None,
            victim_power: None,
        }
    }

    /// This test creates two nearly identical events. They have
    /// the same payload, but differ a little bit in their time
    /// This is a common occurence as the log is generated locally
    /// It is expected that only events is actually generated
    #[sqlx::test]
    async fn test_merge_no_duplicates_near_ts(pool: PgPool) {
        let mut event1 = new_event();
        event1.kill_timestamp = DateTime::parse_from_rfc3339("2024-07-15T18:00:00Z")
            .unwrap()
            .into();
        let mut event2 = event1.clone();
        event2.kill_timestamp += Duration::from_secs(2);

        let interaction_index = InteractionKey::from(&event1);

        let (id, result) =
            insert_new_live_event(&interaction_index.0, &event1, &pool, "TEST_TOKEN")
                .await
                .unwrap();

        let (id2, result2) =
            insert_new_live_event(&interaction_index.0, &event2, &pool, "TEST_TOKEN")
                .await
                .unwrap();

        assert_eq!(id, id2);
        assert_eq!(result, InsertionResult::Created);
        assert_eq!(result2, InsertionResult::Ignored);
    }

    /// This test makes sure that missing data is properly inserted
    /// E.g. if Event 1 is missing the System, Enemy Ship and Own Squad, but is present in Event 2, then the Event should get updated
    /// In total, both events should get merged into one
    #[sqlx::test]
    async fn test_merge_and_update_events(pool: PgPool) {
        let mut event1 = new_event();
        event1.kill_timestamp = DateTime::parse_from_rfc3339("2024-07-15T18:00:00Z")
            .unwrap()
            .into();
        event1.victim_ship = None;
        event1.killer_squadron = Some("KILL".to_string());

        let mut event2 = event1.clone();
        event2.kill_timestamp += Duration::from_secs(2);
        event2.killer_ship = None;
        event2.victim_squadron = Some("VICT".to_string());

        let interaction_index = InteractionKey::from(&event1);

        let (id, result) =
            insert_new_live_event(&interaction_index.0, &event1, &pool, "TEST_TOKEN")
                .await
                .unwrap();

        let (id2, result2) =
            insert_new_live_event(&interaction_index.0, &event2, &pool, "TEST_TOKEN")
                .await
                .unwrap();

        assert_eq!(id, id2);
        assert_eq!(result, InsertionResult::Created);
        assert_eq!(result2, InsertionResult::Updated);

        let merged = get_kill_from_interaction_index_and_timestamp_if_exists(
            &interaction_index.0,
            event1.kill_timestamp + Duration::from_secs(1),
            &pool,
        )
        .await
        .unwrap()
        .unwrap();

        assert_eq!(merged.0, id);
    }

    #[sqlx::test]
    async fn test_poison_system_reproducibility(pool: PgPool) {
        sqlx::query!("
            INSERT INTO public.elite_kills (interaction_index,killer,killer_squadron,killer_rank,killer_ship,victim,victim_squadron,victim_rank,victim_ship,kill_timestamp,hashed_token_creator,hashed_token_updater,system_name) VALUES
                ('azorbid:st4r f0x','ST4R F0X','NATO',6,'empire_trader','azorbid',NULL,1,'type9','2018-09-20 21:58:40.000','000000000','000000000','Eravate'),
                ('st3n:st4r f0x','ST4R F0X','NATO',6,'empire_trader','St3n',NULL,0,'sidewinder','2018-09-20 22:00:09.000','000000000','000000000','Eravate'),
                ('st4r f0x:amen vendein','ST4R F0X','NATO',6,'empire_trader','Amen Vendein',NULL,0,'type6','2018-09-20 22:02:14.000','000000000','000000000','Eravate'),
                ('st4r f0x:dennishd','ST4R F0X','NATO',6,'empire_trader','DennisHD',NULL,0,'type7','2018-09-20 22:03:54.000','000000000','000000000','Eravate'),
                ('st4r f0x:katanavolker','ST4R F0X','NATO',6,'empire_trader','KatanaVolker',NULL,8,'federation_corvette','2018-09-20 22:07:18.000','000000000','000000000','Eravate'),
                ('st4r f0x:gardemarin2018','ST4R F0X','NATO',6,'empire_trader','Gardemarin2018',NULL,0,'sidewinder','2018-09-20 22:16:06.000','000000000','000000000','Eravate'),
                ('sampico:st4r f0x','ST4R F0X','NATO',6,'Empire_Trader','sampico',NULL,0,'asp','2018-09-20 23:13:09.000','000000000','000000000','Eravate'),
                ('st4r f0x:borrel','ST4R F0X','NATO',6,'Empire_Trader','Borrel',NULL,2,'type7','2018-09-20 23:14:42.000','000000000','000000000','Eravate'),
                ('st4r f0x:leestar','ST4R F0X','NATO',6,'Empire_Trader','Leestar',NULL,8,'krait_mkii','2018-09-21 23:21:45.000','000000000','000000000','V886 Centauri'),
                ('wdx:st4r f0x','ST4R F0X','NATO',6,'ferdelance','WDX','G0NK',8,'Federation_Corvette','2018-09-22 20:35:24.000','000000000','000000000','Shinrarta Dezhra');
                ").execute(&pool).await.unwrap();
        // A set of systems is given. A new kill event from Eravate is made. The system must be either Shin or V886
        let system =
            determine_poison_system(&pool, Some("Eravate".to_string()), "ST4R F0X".to_string())
                .await
                .unwrap();
        if &system != "V886 Centauri" && system != "Shinrarta Dezhra" {
            panic!("unexpected system {}", system);
        }

        // Check that the entry was put into the DB
        struct DbResult {
            system_name: String,
        }
        let fetched_system = sqlx::query_as!(
            DbResult,
            "SELECT system_name FROM spoof_systems WHERE cmdr_name = $1",
            "ST4R F0X"
        )
        .fetch_one(&pool)
        .await
        .unwrap();
        assert_eq!(system, fetched_system.system_name);
        // Make sure that subsequent calls all use that system
        for _ in 0..5 {
            let s =
                determine_poison_system(&pool, Some("Eravate".to_string()), "ST4R F0X".to_string())
                    .await
                    .unwrap();
            assert_eq!(system, s);
        }
    }
}
