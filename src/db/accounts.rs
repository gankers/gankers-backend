use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use tracing::error;

#[derive(Deserialize, Serialize)]
pub(crate) struct AccountData {
    pub(crate) discord_id: i64,
    pub(crate) is_admin: bool,
    pub(crate) is_banned: bool,
    pub(crate) registered_timestamp: DateTime<Utc>,
    pub(crate) spoof_location_duration_minutes: i16,
}

pub(crate) async fn get_account_info_by_discord_id(
    discord_id: i64,
    pool: &PgPool,
) -> Result<Option<AccountData>, sqlx::Error> {
    let result: Option<AccountData> = sqlx::query_as!(
        AccountData,
        r#"
        SELECT owner_discord_id as discord_id, is_admin, is_banned, registered_timestamp, spoof_location_duration_minutes
        FROM accounts
        WHERE owner_discord_id = $1"#,
        discord_id
    )
    .fetch_optional(pool)
    .await?;
    Ok(result)
}

pub(crate) async fn create_account_by_discord_id(
    discord_id: i64,
    pool: &PgPool,
) -> Result<AccountData, sqlx::Error> {
    let result: AccountData = sqlx::query_as!(
        AccountData,
        r#"
        INSERT INTO accounts
         (owner_discord_id)
        VALUES ($1)
        RETURNING 
        owner_discord_id as discord_id, is_admin, is_banned, registered_timestamp, spoof_location_duration_minutes"#,
        discord_id
    )
    .fetch_one(pool)
    .await?;

    Ok(result)
}

pub(crate) async fn patch_account_by_discord_id(
    discord_id: i64,
    banned: Option<bool>,
    spoof_duration: Option<u16>,
    pool: &PgPool,
) -> Result<Option<AccountData>, sqlx::Error> {
    let spoof_duration = spoof_duration.map(|x| x as i16);
    let result: Option<AccountData> = sqlx::query_as!(
        AccountData,
        r#"
        UPDATE accounts
         SET is_banned = COALESCE($1, is_banned),
             spoof_location_duration_minutes = COALESCE($3, spoof_location_duration_minutes)
         WHERE owner_discord_id = $2 
        RETURNING 
        owner_discord_id as discord_id, spoof_location_duration_minutes, is_admin, is_banned, registered_timestamp"#,
        banned,
        discord_id,
        spoof_duration
    )
    .fetch_optional(pool)
    .await?;

    Ok(result)
}

pub(crate) async fn get_spoof_duration_from_token_minutes(
    hashed_token: &str,
    pool: &PgPool,
) -> Option<usize> {
    struct DbResponse {
        spoof_location_duration_minutes: i64,
    }

    match sqlx::query_as!(
        DbResponse,
        r#"
            SELECT a.spoof_location_duration_minutes
            FROM accounts a
            JOIN tokens t ON a.owner_discord_id = t.owner_discord_id 
            WHERE t.hashed_token = $1;
            "#,
        hashed_token
    )
    .fetch_optional(pool)
    .await
    {
        Ok(val) => match val {
            Some(e) => Some(e.spoof_location_duration_minutes as usize),
            None => {
                error!("Dangling Token? Token {} has no owner!", hashed_token);
                None
            }
        },
        Err(err) => {
            error!(
                "Failed to get Spoodinf Preferences from Token {}. Reason: {}",
                hashed_token, err
            );
            None
        }
    }
}
