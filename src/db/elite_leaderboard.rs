use chrono::{DateTime, Utc};
use itertools::Itertools;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use crate::{services::elite::KillEvent, util::ship_names::get_pretty_names_for_ship_ids};

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub(crate) struct LeaderboardEntry {
    cmdr_name: String,
    kills: i64,
    deaths: i64,
}

pub(crate) async fn get_leaderboard_entries(
    page: i64,
    pool: &PgPool,
) -> Result<Vec<LeaderboardEntry>, sqlx::Error> {
    let limit = 50i64;
    let offset = limit * (page - 1);

    let result: Vec<LeaderboardEntry> = sqlx::query_as!(
        LeaderboardEntry,
        r#"
        SELECT cmdr_name, kills, deaths
        FROM elite_commanders
        ORDER BY kills DESC, deaths ASC
        LIMIT $1
        OFFSET $2"#,
        limit,
        offset
    )
    .fetch_all(pool)
    .await?;

    Ok(result)
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub(crate) struct WeeklyLeaderboardEntry {
    cmdr_name: String,
    kills: Option<i64>,
}
///
/// Does something similar to get_leaderboard_entries, except that we must operate on the kills table
/// This is more expensive :/
///
pub(crate) async fn get_leaderboard_entries_in_range(
    page: i64,
    page_size: i64,
    pool: &PgPool,
    range: (DateTime<Utc>, DateTime<Utc>),
) -> Result<Vec<WeeklyLeaderboardEntry>, sqlx::Error> {
    let mut page_size = page_size;
    if page_size == 0 {
        page_size = 10;
    }

    if page_size > 50 {
        // TODO: Err handling
        page_size = 50;
    } else if page_size < 0 {
        page_size = 10;
    }

    let offset = page_size * (page - 1);
    // Not really happy w/ using naive here, but oh well
    let lower = range.0.naive_utc();
    let upper = range.1.naive_utc();

    struct DbWeeklyLeaderboardEntry {
        cmdr_name: Option<String>,
        kills: Option<i64>,
    }

    let result: Vec<WeeklyLeaderboardEntry> = sqlx::query_as!(
        DbWeeklyLeaderboardEntry,
        r#"
        SELECT COALESCE(count(*), 0) as kills, killer as cmdr_name
        FROM elite_kills_patched ek
        WHERE ek.kill_timestamp AT TIME ZONE 'UTC' BETWEEN $1 AND $2
        GROUP BY killer ORDER BY count(*) DESC
        LIMIT $3
        OFFSET $4"#,
        lower,
        upper,
        page_size,
        offset
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .filter_map(|x| match x.cmdr_name {
        Some(name) => Some(WeeklyLeaderboardEntry {
            cmdr_name: name,
            kills: x.kills,
        }),
        None => None,
    })
    .collect_vec();

    Ok(result)
}

pub(crate) async fn get_total_kill_event_count(pool: &PgPool) -> Result<usize, sqlx::Error> {
    struct ResultContainer {
        count: Option<i64>,
    }

    let result: ResultContainer = sqlx::query_as!(
        ResultContainer,
        r#"
        SELECT COUNT(*) AS count FROM elite_kills
        "#
    )
    .fetch_one(pool)
    .await?;

    Ok(result.count.unwrap_or(0) as usize)
}

#[derive(Serialize, Deserialize)]
pub(super) struct KillboardEntryDb {
    pub(super) kill_id: Option<i32>,
    pub(super) interaction_index: Option<String>,
    pub(super) killer: Option<String>,
    pub(super) killer_squadron: Option<String>,
    pub(super) killer_rank: Option<i16>,
    pub(super) killer_ship: Option<String>,
    pub(super) victim: Option<String>,
    pub(super) victim_squadron: Option<String>,
    pub(super) victim_rank: Option<i16>,
    pub(super) victim_ship: Option<String>,
    pub(super) kill_timestamp: Option<DateTime<Utc>>,
    pub(super) system_name: Option<String>,
    pub(super) killer_power: Option<String>,
    pub(super) victim_power: Option<String>,
}

impl From<KillboardEntryDb> for Option<KillEvent> {
    fn from(x: KillboardEntryDb) -> Self {
        Some(KillEvent {
            kill_id: x.kill_id,
            system: x.system_name,
            killer: match x.killer {
                Some(x) => x,
                None => return None,
            },
            killer_rank: match x.killer_rank {
                Some(x) => x,
                None => return None,
            },
            victim: match x.victim {
                Some(x) => x,
                None => return None,
            },
            victim_rank: match x.victim_rank {
                Some(x) => x,
                None => return None,
            },
            killer_ship: x.killer_ship,
            victim_ship: x.victim_ship,
            killer_squadron: x.killer_squadron,
            victim_squadron: x.victim_squadron,
            kill_timestamp: match x.kill_timestamp {
                Some(x) => x,
                None => return None,
            },
            killer_power: x.killer_power,
            victim_power: x.victim_power,
        })
    }
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct KillboardCmdrEntry {
    pub(crate) name: String,
    pub(crate) squadron: Option<String>,
    pub(crate) rank_id: i64,
    pub(crate) ship_id: Option<String>,
    pub(crate) ship_name: Option<String>,
    pub(crate) power: Option<String>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct KillboardEntry {
    pub(crate) kill_id: i64,
    pub(crate) interaction_index: String,
    pub(crate) kill_timestamp: DateTime<Utc>,
    pub(crate) system_name: Option<String>,
    pub(crate) killer: KillboardCmdrEntry,
    pub(crate) victim: KillboardCmdrEntry,
}

impl TryFrom<KillboardEntryDb> for KillboardEntry {
    type Error = ();

    fn try_from(value: KillboardEntryDb) -> Result<Self, Self::Error> {
        let victim_ship_name = {
            match value.victim_ship.clone() {
                Some(val) => get_pretty_names_for_ship_ids(&val, false),
                None => None,
            }
        };

        let victim_name = match value.victim {
            Some(x) => x,
            None => {
                return Err(());
            }
        };

        let victim_rank = match value.victim_rank {
            Some(x) => x,
            None => {
                return Err(());
            }
        };

        let victim = KillboardCmdrEntry {
            name: victim_name,
            squadron: value.victim_squadron,
            rank_id: victim_rank as i64,
            ship_id: value.victim_ship,
            ship_name: victim_ship_name,
            power: value.victim_power,
        };

        let killer_ship_name = {
            match value.killer_ship.clone() {
                Some(val) => get_pretty_names_for_ship_ids(&val, false),
                None => None,
            }
        };

        let killer_name = match value.killer {
            Some(x) => x,
            None => {
                return Err(());
            }
        };

        let killer_rank = match value.killer_rank {
            Some(x) => x,
            None => {
                return Err(());
            }
        };

        let killer = KillboardCmdrEntry {
            name: killer_name,
            squadron: value.killer_squadron,
            rank_id: killer_rank as i64,
            ship_name: killer_ship_name,
            ship_id: value.killer_ship,
            power: value.killer_power,
        };

        Ok(KillboardEntry {
            kill_id: match value.kill_id {
                Some(x) => x.into(),
                None => return Err(()),
            },
            interaction_index: match value.interaction_index {
                Some(x) => x,
                None => return Err(()),
            },
            kill_timestamp: match value.kill_timestamp {
                Some(x) => x,
                None => return Err(()),
            },
            system_name: value.system_name,
            killer,
            victim,
        })
    }
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub(crate) struct SystemMetadata {
    pub(crate) system_name: String,
    pub(crate) kill_count: i64,
}

pub(crate) async fn get_system_metadata(
    system_name: String,
    pool: &PgPool,
) -> Result<Option<SystemMetadata>, sqlx::Error> {
    struct SystemMetadataLocal {
        system_name: Option<String>,
        kill_count: Option<i64>,
    }

    let result: SystemMetadataLocal= sqlx::query_as!(
        SystemMetadataLocal,
        r#"
        select coalesce(count(*), 0) as kill_count, coalesce(max(ek.system_name), '') as system_name from elite_kills_patched ek where LOWER(ek.system_name) = LOWER($1);
        "#,
        system_name
    )
    .fetch_one(pool)
    .await?;

    let result = SystemMetadata {
        system_name: result.system_name.unwrap_or("".to_string()),
        kill_count: result.kill_count.unwrap_or(0),
    };

    if result.kill_count == 0 {
        return Ok(None);
    }
    Ok(Some(result))
}

pub(crate) async fn get_recent_killboard_entries_for_system(
    systen_name: String,
    page_size: i64,
    page: i64,
    pool: &PgPool,
) -> Result<Vec<KillboardEntry>, sqlx::Error> {
    let mut page_size = page_size;
    if page_size == 0 {
        page_size = 10;
    }

    if page_size > 50 {
        // TODO: Err handling
        page_size = 50;
    } else if page_size < 0 {
        page_size = 10;
    }

    let offset = (page - 1) * page_size;
    let result: Vec<KillboardEntryDb> = sqlx::query_as!(
        KillboardEntryDb,
        r#"
        SELECT
            kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, killer_power, victim_power
        FROM elite_kills_patched ek
        WHERE system_name = $3
        ORDER BY ek.kill_timestamp DESC
        LIMIT $1
        OFFSET $2"#,
        page_size,
        offset,
        systen_name
    )
    .fetch_all(pool)
    .await?;

    let structured_result = result
        .into_iter()
        .filter_map(|x| match KillboardEntry::try_from(x) {
            Ok(x) => Some(x),
            Err(_) => None,
        })
        .collect_vec();
    Ok(structured_result)
}

pub(crate) async fn get_killboard_entries_for_timespan(
    pool: &PgPool,
    from: DateTime<Utc>,
    to: DateTime<Utc>,
) -> Result<Vec<KillboardEntry>, sqlx::Error> {
    let result: Vec<KillboardEntryDb> = sqlx::query_as!(
        KillboardEntryDb,
        r#"
        SELECT
            kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, killer_power, victim_power
        FROM elite_kills_patched ek
        WHERE kill_timestamp > $1 AND kill_timestamp < $2
        ORDER BY ek.kill_timestamp ASC"#,
        from,
        to
    )
    .fetch_all(pool)
    .await?;

    let structured_result = result
        .into_iter()
        .filter_map(|x| match KillboardEntry::try_from(x) {
            Ok(x) => Some(x),
            Err(_) => None,
        })
        .collect_vec();
    Ok(structured_result)
}

pub(crate) async fn get_recent_killboard_entries(
    page_size: i64,
    page: i64,
    pool: &PgPool,
) -> Result<Vec<KillboardEntry>, sqlx::Error> {
    let mut page_size = page_size;
    if page_size == 0 {
        page_size = 10;
    }

    if page_size > 50 {
        // TODO: Err handling
        page_size = 50;
    } else if page_size < 0 {
        page_size = 10;
    }

    let offset = (page - 1) * page_size;

    let result: Vec<KillboardEntryDb> = sqlx::query_as!(
        KillboardEntryDb,
        r#"
        SELECT
            kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, killer_power, victim_power
        FROM elite_kills_patched ek
        ORDER BY ek.kill_timestamp DESC
        LIMIT $1
        OFFSET $2"#,
        page_size,
        offset
    )
    .fetch_all(pool)
    .await?;

    let structured_result = result
        .into_iter()
        .filter_map(|x| match KillboardEntry::try_from(x) {
            Ok(x) => Some(x),
            Err(_) => None,
        })
        .collect_vec();
    Ok(structured_result)
}
