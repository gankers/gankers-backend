//
// This module is used do CRUD operations against the DB for the tokens table
//

use rand::{distributions::Alphanumeric, Rng};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sha256::digest;
use sqlx::{Error, PgPool};

#[derive(Debug, Deserialize, Serialize)]
struct DbToken {
    owner_discord_id: i64,
    revoked: bool,
    has_admin: bool,
    has_self: bool,
    has_elite_submit: bool,
    revoke_on_new_token: bool,
    hashed_token: String,
}

#[derive(Debug, Deserialize, Serialize, JsonSchema)]
pub(crate) struct Permissions {
    pub(crate) admin: bool,
    pub(crate) self_api: bool,
    pub(crate) elite_submit: bool,
}

#[derive(Debug, Deserialize, Serialize, JsonSchema)]
pub(crate) struct TokenData {
    pub(crate) owner_discord_id: i64,
    pub(crate) revoke_on_new_token: bool,
    pub(crate) revoked: bool,
    pub(crate) permissions: Permissions,
    pub(crate) hashed_token: Option<String>,
}

/// Contains the "cleartext" token to be returned,
/// aswell as permissions and "data" belonging to this token
/// Cleartext Token shall NOT be stored. It is simply for returning
/// a newly created token!
#[derive(Debug, Deserialize, Serialize, JsonSchema)]
pub(crate) struct TokenWithData {
    pub(crate) token: String,
    pub(crate) data: TokenData,
}

impl From<&DbToken> for TokenData {
    fn from(value: &DbToken) -> Self {
        let perms = Permissions {
            admin: value.has_admin,
            self_api: value.has_self,
            elite_submit: value.has_elite_submit,
        };

        TokenData {
            owner_discord_id: value.owner_discord_id,
            revoke_on_new_token: value.revoke_on_new_token,
            permissions: perms,
            revoked: value.revoked,
            hashed_token: Some(value.hashed_token.clone()),
        }
    }
}

pub(crate) async fn get_token_data(token: &str, pool: &PgPool) -> Result<Option<TokenData>, Error> {
    let hashed_token = digest(token);
    let token: Option<DbToken> = sqlx::query_as!(
        DbToken,
        r#"
        SELECT owner_discord_id, revoked, has_admin, has_self, has_elite_submit, revoke_on_new_token, hashed_token
        FROM tokens 
        WHERE hashed_token = $1"#,
        hashed_token
    )
    .fetch_optional(pool)
    .await?;
    match token {
        Some(val) => Ok(Some(TokenData::from(&val))),
        None => Ok(None),
    }
}

pub(crate) async fn get_discord_account_based_on_hashed_token(
    hashed_token: &str,
    pool: &PgPool,
) -> Result<Option<i64>, Error> {
    struct Response {
        owner_discord_id: i64,
    }
    let response: Option<Response> = sqlx::query_as!(
        Response,
        r#"
        SELECT owner_discord_id FROM tokens WHERE hashed_token = $1
        "#,
        hashed_token
    )
    .fetch_optional(pool)
    .await?;

    Ok(response.map(|x| x.owner_discord_id))
}

pub(crate) async fn _revoke_token(token: &str, pool: &PgPool) -> Result<(), Error> {
    let hashed_token = digest(token);
    sqlx::query!(
        r#"
        UPDATE tokens 
        SET revoked = false
        WHERE hashed_token = $1"#,
        hashed_token
    )
    .execute(pool)
    .await?;
    Ok(())
}

pub(crate) async fn get_all_tokens_for_user(
    discord_id: i64,
    pool: &PgPool,
) -> Result<Vec<TokenData>, Error> {
    let tokens = sqlx::query_as!(
        DbToken,
        r#"
        SELECT owner_discord_id, revoked, has_admin, has_self, has_elite_submit, revoke_on_new_token, hashed_token
        FROM tokens 
        WHERE owner_discord_id = $1"#,
        discord_id
    )
    .fetch_all(pool)
    .await?;

    let data: Vec<_> = tokens.iter().map(TokenData::from).collect();

    Ok(data)
}

pub(crate) async fn create_new_token_for_user(
    token_data: &TokenData,
    pool: &PgPool,
) -> Result<TokenWithData, Error> {
    create_new_token_for_user_with_revocation(token_data, pool, true).await
}

pub(crate) async fn create_new_token_for_user_with_revocation(
    token_data: &TokenData,
    pool: &PgPool,
    revoke_tokens: bool,
) -> Result<TokenWithData, Error> {
    // Technically, this *could* cause collisions
    // But quite frankly… |A-Za-z0-9|^32
    // = 62^32 = 2.2726578845e+57… yeah..
    let new_token: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(32)
        .map(char::from)
        .collect();

    let hashed_token = digest(&new_token);

    if revoke_tokens {
        revoke_all_autorevokable_tokens_for_account(token_data.owner_discord_id, pool).await?;
    }

    sqlx::query!(
        r#"
        INSERT INTO tokens 
        (owner_discord_id, hashed_token, revoked, revoke_on_new_token, has_admin, has_self, has_elite_submit)
        VALUES ($1,        $2,           $3,      $4,                  $5,        $6,       $7)
        "#,
        token_data.owner_discord_id,
        hashed_token,
        token_data.revoked,
        token_data.revoke_on_new_token,
        token_data.permissions.admin,
        token_data.permissions.self_api,
        token_data.permissions.elite_submit
    )
    .execute(pool)
    .await?;

    let data = get_token_data(&new_token, pool).await?;

    match data {
        None => Err(Error::RowNotFound),
        Some(data) => Ok(TokenWithData {
            token: new_token,
            data,
        }),
    }
}

pub(crate) async fn revoke_all_autorevokable_tokens_for_account(
    owner_discord_id: i64,
    pool: &PgPool,
) -> Result<(), Error> {
    sqlx::query!(
        r#"
        UPDATE tokens 
        SET revoked = True
        WHERE revoke_on_new_token = True AND owner_discord_id = $1
        "#,
        owner_discord_id,
    )
    .execute(pool)
    .await?;
    Ok(())
}
