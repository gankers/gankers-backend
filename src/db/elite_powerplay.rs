use chrono::{DateTime, Utc};
use itertools::Itertools;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

use super::elite_leaderboard::{KillboardEntry, KillboardEntryDb};

#[derive(Debug, Serialize, Deserialize, JsonSchema, Clone)]
pub(crate) struct PowerplayInfo {
    pub(crate) power: String,
    pub(crate) kills: i64,
    pub(crate) deaths: i64,
    /// Agents are only returned when timespan is set to EliteWeek
    pub(crate) agents: Option<i64>,
}

async fn get_agent_count_for_timespan(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: (DateTime<Utc>, DateTime<Utc>),
    include_unpledged_and_ff: bool,
) -> Result<Vec<(String, i64)>, sqlx::Error> {
    struct AgentCountResult {
        power: Option<String>,
        agent_count: Option<i64>,
    }
    let mut result: Vec<AgentCountResult> = match include_unpledged_and_ff {
        false => {
            sqlx::query_as!(
                AgentCountResult,
                r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND (killer_power IS NOT NULL AND victim_power IS NOT NULL) AND killer_power != victim_power
            ),
            unwrapped AS (
                SELECT 
                    killer AS person, 
                    killer_power AS power
                FROM tmp
                UNION ALL
                SELECT 
                    victim AS person, 
                    victim_power AS power
                FROM tmp
            )
            SELECT 
                power, 
                COUNT(DISTINCT person) AS agent_count
            FROM unwrapped
            where power is not NULL
            GROUP BY power
            ORDER BY power;
            "#,
                bounds.0,
                bounds.1
            )
            .fetch_all(pool)
            .await?
        }
        true => {
            sqlx::query_as!(
                AgentCountResult,
                r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE ek.kill_timestamp BETWEEN $1 AND $2
            ),
            unwrapped AS (
                SELECT 
                    killer AS person, 
                    killer_power AS power
                FROM tmp
                UNION ALL
                SELECT 
                    victim AS person, 
                    victim_power AS power
                FROM tmp
            )
            SELECT 
                power, 
                COUNT(DISTINCT person) AS agent_count
            FROM unwrapped
            where power is not NULL
            GROUP BY power
            ORDER BY power;
            "#,
                bounds.0,
                bounds.1
            )
            .fetch_all(pool)
            .await?
        }
    };
    return Ok(result
        .iter_mut()
        .filter_map(|x| {
            if x.agent_count.is_none() || x.power.is_none() {
                return None;
            }
            Some((x.power.clone().unwrap(), x.agent_count.unwrap()))
        })
        .collect_vec());
}

async fn get_agent_count_for_timespan_for_power(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: (DateTime<Utc>, DateTime<Utc>),
    power: &str,
    include_unpledged_and_ff: bool,
) -> Result<Option<i64>, sqlx::Error> {
    struct AgentCount {
        agent_count: Option<i64>,
    }
    let result: Option<AgentCount> = match include_unpledged_and_ff {
        true => sqlx::query_as!(
            AgentCount,
            r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND (killer_power = $3 OR victim_power = $3) AND killer_power != victim_power
            ),
            unwrapped AS (
                SELECT 
                    killer AS person 
                FROM tmp
                WHERE killer_power = $3
                UNION ALL
                SELECT 
                    victim AS person
                FROM tmp
                WHERE victim_power = $3
            )
            SELECT  
                COUNT(DISTINCT(person)) AS agent_count
            FROM unwrapped
            "#,
            bounds.0,
            bounds.1,
            power
        )
        .fetch_optional(pool)
        .await?,
        false => sqlx::query_as!(
            AgentCount,
            r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND (killer_power = $3 OR victim_power = $3) AND (killer_power IS NOT NULL AND victim_power IS NOT NULL)
            ),
            unwrapped AS (
                SELECT 
                    killer AS person 
                FROM tmp
                WHERE killer_power = $3
                UNION ALL
                SELECT 
                    victim AS person
                FROM tmp
                WHERE victim_power = $3
            )
            SELECT  
                COUNT(DISTINCT(person)) AS agent_count
            FROM unwrapped
            "#,
            bounds.0,
            bounds.1,
            power
        )
        .fetch_optional(pool)
        .await?,
    };

    match result {
        Some(val) => Ok(Some(val.agent_count.unwrap_or(0))),
        None => Ok(None),
    }
}

async fn get_kills_and_deaths_for_all_powers(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: Option<(DateTime<Utc>, DateTime<Utc>)>,
    include_unpledged_and_ff: bool,
) -> Result<Vec<(String, i64, i64)>, sqlx::Error> {
    struct DbResult {
        power: Option<String>,
        kills: Option<i64>,
        deaths: Option<i64>,
    }

    let permutation = (bounds, include_unpledged_and_ff);

    let result: Vec<DbResult> = match permutation {
        (None, true) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek 
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
            )
            .fetch_all(pool)
            .await?
        }
        (None, false) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek WHERE killer_power IS NOT NULL AND victim_power IS NOT NULL AND victim_power != killer_power
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
            )
            .fetch_all(pool)
            .await?
        }
        (Some((lower, upper)), true) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek 
                    WHERE ek.kill_timestamp BETWEEN $1 AND $2
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                lower,
                upper
            )
            .fetch_all(pool)
            .await?
        }
        (Some((lower, upper)), false) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek 
                    WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND killer_power IS NOT NULL AND victim_power IS NOT NULL AND victim_power != killer_power
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                lower,
                upper
            )
            .fetch_all(pool)
            .await?
        },
    };

    return Ok(result
        .iter()
        .filter_map(|x| {
            let name = match x.power.clone() {
                None => return None,
                Some(name) => name,
            };

            return Some((name, x.kills.unwrap_or(0), x.deaths.unwrap_or(0)));
        })
        .collect_vec());
}

async fn get_kills_and_deaths_for_power(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: Option<(DateTime<Utc>, DateTime<Utc>)>,
    power: &str,
    include_unpledged_and_ff: bool,
) -> Result<Option<(i64, i64)>, sqlx::Error> {
    struct DbResult {
        power: Option<String>,
        kills: Option<i64>,
        deaths: Option<i64>,
    }

    let permutation = (bounds, include_unpledged_and_ff);

    let result = match permutation {
        (None, true) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek WHERE  (killer_power = $1 OR victim_power = $1)
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                power
            )
            .fetch_all(pool)
            .await?
        },
        (None, false) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek WHERE  (killer_power = $1 OR victim_power = $1) AND killer_power IS NOT NULL AND victim_power IS NOT NULL AND victim_power != killer_power
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                power
            )
            .fetch_all(pool)
            .await?
        },
        (Some((lower, upper)), true) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek 
                    WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND (killer_power = $3 OR victim_power = $3)
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                lower,
                upper,
                power
            )
            .fetch_all(pool)
            .await?
        }
        (Some((lower, upper)), false) => {
            sqlx::query_as!(
                DbResult,
                r#"
                WITH tmp AS (
                    SELECT * 
                    FROM elite_kills ek 
                    WHERE (ek.kill_timestamp BETWEEN $1 AND $2) AND (killer_power = $3 OR victim_power = $3) AND (killer_power IS NOT NULL AND victim_power IS NOT NULL) AND victim_power != killer_power
                ),
                kills AS (
                    SELECT 
                        killer_power AS power, 
                        COUNT(*) AS kills
                    FROM tmp
                    GROUP BY killer_power
                ),
                deaths AS (
                    SELECT 
                        victim_power AS power, 
                        COUNT(*) AS deaths
                    FROM tmp
                    GROUP BY victim_power
                )
                SELECT 
                    COALESCE(k.power, d.power) AS power, 
                    COALESCE(k.kills, 0) AS kills, 
                    COALESCE(d.deaths, 0) AS deaths
                FROM kills k
                FULL OUTER JOIN deaths d
                ON k.power = d.power
                WHERE COALESCE(k.power, d.power) IS NOT NULL
                ORDER BY power;
                "#,
                lower,
                upper,
                power
            )
            .fetch_all(pool)
            .await?
        },
    };

    for entry in result {
        match entry.power {
            None => continue,
            Some(x) => {
                if x != power {
                    continue;
                }
            }
        }
        // if here: correct entry
        return Ok(Some((entry.kills.unwrap_or(0), entry.deaths.unwrap_or(0))));
    }

    Ok(None)
}

pub(crate) async fn get_all_powers_summary(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: Option<(DateTime<Utc>, DateTime<Utc>)>,
    include_unpledged_and_ff: bool,
) -> Result<Vec<PowerplayInfo>, sqlx::Error> {
    let agents_fut = async {
        match bounds {
            Some(bounds) => {
                match get_agent_count_for_timespan(pool, bounds, include_unpledged_and_ff).await {
                    Ok(val) => Some(val),
                    Err(_) => None,
                }
            }
            None => None,
        }
    };

    let kills_and_deaths_for_powers_fut =
        get_kills_and_deaths_for_all_powers(pool, bounds, include_unpledged_and_ff);

    let (agents, kills_deaths) = tokio::join!(agents_fut, kills_and_deaths_for_powers_fut);

    let response = kills_deaths?
        .into_iter()
        .map(|(power, kills, deaths)| {
            let mut agents_count = None;
            match &agents {
                Some(agents) => {
                    for (agent_power, count) in agents {
                        if &power != agent_power {
                            continue;
                        }
                        agents_count = Some(*count);
                        break;
                    }
                }
                None => {}
            }

            PowerplayInfo {
                power,
                kills,
                deaths,
                agents: agents_count,
            }
        })
        .collect_vec();

    Ok(response)
}

pub(crate) async fn get_power_detail_info(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: Option<(DateTime<Utc>, DateTime<Utc>)>,
    power: &str,
    include_unpledged: bool,
) -> Result<Option<PowerplayInfo>, sqlx::Error> {
    let agents_fut = async {
        match bounds {
            Some(bounds) => {
                match get_agent_count_for_timespan_for_power(pool, bounds, power, include_unpledged)
                    .await
                {
                    Ok(val) => val,
                    Err(_) => None,
                }
            }
            None => None,
        }
    };

    let kills_and_deaths_for_power_fut =
        get_kills_and_deaths_for_power(pool, bounds, &power, include_unpledged);

    let (agents, kills_deaths) = tokio::join!(agents_fut, kills_and_deaths_for_power_fut);

    let result = match kills_deaths? {
        None => return Ok(None),
        Some((kills, deaths)) => PowerplayInfo {
            power: power.to_string(),
            kills,
            deaths,
            agents,
        },
    };

    Ok(Some(result))
}

pub(crate) async fn get_agent_leaderboard_for_power_and_timerange(
    pool: &sqlx::Pool<sqlx::Postgres>,
    bounds: (DateTime<Utc>, DateTime<Utc>),
    power: &str,
    offset: i64,
    limit: i64,
    include_unpledged_and_ff: bool,
) -> Result<Vec<(String, i64, i64)>, sqlx::Error> {
    struct DbResult {
        person: Option<String>,
        kill_count: Option<i64>,
        death_count: Option<i64>,
    }

    let result: Vec<DbResult> = match include_unpledged_and_ff {
        true => {
            sqlx::query_as!(
                DbResult,
                r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE (ek.kill_timestamp BETWEEN $1 AND $2) 
                AND (killer_power = $3 OR victim_power = $3) 
            ),
            unwrapped AS (
                SELECT 
                    killer AS person,
                    'kill' AS action
                FROM tmp
                WHERE killer_power = $3
                UNION ALL
                SELECT 
                    victim AS person,
                    'death' AS action
                FROM tmp
                WHERE victim_power = $3
            )
            SELECT  
                person,
                COUNT(CASE WHEN action = 'kill' THEN 1 END) AS kill_count,
                COUNT(CASE WHEN action = 'death' THEN 1 END) AS death_count
            FROM unwrapped
            GROUP BY person
            ORDER BY kill_count DESC, death_count ASC
            OFFSET $4
            LIMIT $5
            "#,
                bounds.0,
                bounds.1,
                power,
                offset,
                limit
            )
            .fetch_all(pool)
            .await?
        }
        false => {
            sqlx::query_as!(
                DbResult,
                r#"
            WITH tmp AS (
                SELECT * 
                FROM elite_kills ek 
                WHERE (ek.kill_timestamp BETWEEN $1 AND $2) 
                AND (killer_power = $3 OR victim_power = $3) 
                AND (killer_power IS NOT NULL AND victim_power IS NOT NULL)
                AND killer_power != victim_power
            ),
            unwrapped AS (
                SELECT 
                    killer AS person,
                    'kill' AS action
                FROM tmp
                WHERE killer_power = $3
                UNION ALL
                SELECT 
                    victim AS person,
                    'death' AS action
                FROM tmp
                WHERE victim_power = $3
            )
            SELECT  
                person,
                COUNT(CASE WHEN action = 'kill' THEN 1 END) AS kill_count,
                COUNT(CASE WHEN action = 'death' THEN 1 END) AS death_count
            FROM unwrapped
            GROUP BY person
            ORDER BY kill_count DESC, death_count ASC
            OFFSET $4
            LIMIT $5
            "#,
                bounds.0,
                bounds.1,
                power,
                offset,
                limit
            )
            .fetch_all(pool)
            .await?
        }
    };

    let response = result
        .into_iter()
        .filter_map(|x| {
            let name = match x.person.clone() {
                Some(x) => x,
                None => return None,
            };
            Some((name, x.kill_count.unwrap_or(0), x.death_count.unwrap_or(0)))
        })
        .collect_vec();

    return Ok(response);
}

pub(crate) async fn get_recent_killboard_entries_involving_power(
    page_size: i64,
    page: i64,
    pool: &PgPool,
    power: &str,
    include_unpledged_and_ff: bool,
) -> Result<Vec<KillboardEntry>, sqlx::Error> {
    let mut page_size = page_size;
    if page_size == 0 {
        page_size = 10;
    }

    if page_size > 50 {
        // TODO: Err handling
        page_size = 50;
    } else if page_size < 0 {
        page_size = 10;
    }

    let offset = (page - 1) * page_size;

    let result: Vec<KillboardEntryDb> = match include_unpledged_and_ff {
        true => sqlx::query_as!(
            KillboardEntryDb,
            r#"
            SELECT
                kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, killer_power, victim_power
            FROM elite_kills_patched ek
            WHERE killer_power = $3 OR victim_power = $3
            ORDER BY ek.kill_timestamp DESC
            LIMIT $1
            OFFSET $2"#,
            page_size,
            offset,
            power
        )
        .fetch_all(pool)
        .await?,
        false => sqlx::query_as!(
            KillboardEntryDb,
            r#"
            SELECT
                kill_id, interaction_index, killer, killer_squadron, killer_rank, killer_ship, victim, victim_squadron, victim_ship, victim_rank, kill_timestamp, system_name, killer_power, victim_power
            FROM elite_kills_patched ek
            WHERE (killer_power = $3 OR victim_power = $3) AND (killer_power IS NOT NULL AND victim_power IS NOT NULL) AND killer_power != victim_power
            ORDER BY ek.kill_timestamp DESC
            LIMIT $1
            OFFSET $2"#,
            page_size,
            offset,
            power
        )
        .fetch_all(pool)
        .await?
    };

    let structured_result = result
        .into_iter()
        .filter_map(|x| match KillboardEntry::try_from(x) {
            Ok(x) => Some(x),
            Err(_) => None,
        })
        .collect_vec();
    Ok(structured_result)
}
