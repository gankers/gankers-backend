// Run this with vite-node: `cat myStupidCsv.csv | npx vite-node migrate_prepare.ts`

import { readFileSync } from "node:fs";
import { stdout } from "node:process";

const stdinText = readFileSync("./kills.csv", "utf-8"); //readFileSync(0).toString(); // 0 = Stdin

// Table Format:
// (`id`, `timestamp`, `killer_name`, `killer_rank`, `killer_ship`, `victim_name`, `victim_rank`, `victim_ship`, `created_at`, `updated_at`, `location`, `killer_discord_id`)

interface Entry {
  id: number;
  timestamp: string;
  killer_name: string;
  killer_rank: number;
  killer_ship: string | undefined;
  victim_name: string;
  victim_rank: number;
  victim_ship: string | undefined;
  created_at: string | undefined;
  updated_at: string | undefined;
  location: string | undefined;
  killer_discord_id: BigInt | undefined;
}

interface ErrorContainer {
  message: string;
  row: number;
}

let errors: ErrorContainer[] = [];

function quoteUnwrap(input: string) {
  input = input.trim();
  if (input.startsWith('"') && input.endsWith('"')) {
    return input.substring(1, input.length - 1);
  } else if (input == "\\N") {
    return undefined;
  } else {
    return input;
  }
}

function toIntIfNotUndefined(input: string | undefined) {
  if (!input) {
    return input;
  }

  const asInt = Number.parseInt(input);

  if (Number.isNaN(asInt)) {
    throw new Error("Failed to parse " + input + " as int.");
  }
  return asInt;
}

function undefineUnknown(input: string | undefined) {
  if (input && input.toLocaleLowerCase() === "unknown") {
    return undefined;
  }
  return input;
}

const jsonEntries = stdinText
  .split("\n")
  .filter((e) => e.trim().length > 0)
  .map((e, row) => {
    // No idea if this is right, but according to ChatGPT this will split on commas that do not have a \\ in front of them :P
    const cols = e.split(/(?<!\\),/);

    if (cols.length != 12) {
      errors.push({
        row,
        message: "Expected Row to contain 12 cols, but found " + cols.length,
      });
      return undefined;
    }

    let [
      raw_id,
      raw_timestamp,
      raw_killer_name,
      raw_killer_rank,
      raw_killer_ship,
      raw_victim_name,
      raw_victim_rank,
      raw_victim_ship,
      raw_created_at,
      raw_updated_at,
      raw_location,
      raw_killer_discord_id,
    ] = cols;

    try {
      const id = Number.parseInt(raw_id);
      const timestamp =
        quoteUnwrap(raw_timestamp) !== undefined
          ? quoteUnwrap(raw_timestamp) + "Z"
          : undefined;
      const killer_name = quoteUnwrap(raw_killer_name);
      const killer_rank = toIntIfNotUndefined(quoteUnwrap(raw_killer_rank));
      const killer_ship = undefineUnknown(quoteUnwrap(raw_killer_ship));
      const victim_name = quoteUnwrap(raw_victim_name);
      const victim_rank = toIntIfNotUndefined(quoteUnwrap(raw_victim_rank));
      const victim_ship = undefineUnknown(quoteUnwrap(raw_victim_ship));
      const created_at = quoteUnwrap(raw_created_at);
      const updated_at = quoteUnwrap(raw_updated_at);
      const location = undefineUnknown(quoteUnwrap(raw_location));
      const killer_discord_id = toIntIfNotUndefined(
        undefineUnknown(quoteUnwrap(raw_killer_discord_id))
      );

      return {
        id,
        timestamp,
        killer_name,
        killer_rank,
        killer_ship,
        victim_name,
        victim_rank,
        victim_ship,
        created_at,
        updated_at,
        location,
        killer_discord_id,
      } as Entry;
    } catch (err) {
      const errContainer: ErrorContainer = {
        row,
        message: String(err),
      };
      console.error(errContainer);
      errors.push(errContainer);
    }
  })
  .filter(Boolean);

stdout.write(JSON.stringify(jsonEntries, undefined, 2));

console.error("count: ", jsonEntries.length);
